cd /Users/avilches/Work/Proy/Local/mamespin
gradlew clean assemble

# deploy fat jar
scp /Users/avilches/Work/Proy/Local/mamespin/build/libs/mamespin-* avilches@hyperspin-online.com:/srv/mamespin/bin
scp /Users/avilches/Work/Proy/Local/mamespin/op/service/mamespin.* avilches@hyperspin-online.com:/srv/mamespin/bin

# deploy config y html
scp /srv/mamespin/config.pro/* avilches@hyperspin-online.com:/srv/mamespin/config
scp /Users/avilches/Work/Proy/Local/mamespin/html/* avilches@hyperspin-online.com:/srv/mamespin/html

# nginx config
scp /Users/avilches/Work/Proy/Local/mamespin/op/nginx/* root@hyperspin-online.com:/etc/nginx/sites-enabled


