/*
* bq.com
*
* @author Alberto Vilches (alberto.vilches@bq.com)
* @date 25/3/15
* Copyright. All Rights Reserved.
*/
package tools;

import crypto.PersistenceContextService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class BoundedPersistenceExecutor {
    private ThreadPoolExecutor executor;
    private Semaphore semaphore;
    private PersistenceContextService persistenceContextService;
    private Callable<ThreadPoolExecutor> executorFactory;
    private int bound = 20; // Default value, overwrite it in the resources.groovy
    List<Future> tasks;

    public ThreadPoolExecutor getExecutor() {
        return executor;
    }

    public void setPersistenceContextService(PersistenceContextService persistenceContextService) {
        this.persistenceContextService = persistenceContextService;
    }

    public void setBound(int bound) {
        this.bound = bound;
    }

    public int getBound() {
        return bound;
    }

    public void setExecutorFactory(Callable<ThreadPoolExecutor> executorFactory) {
        this.executorFactory = executorFactory;
    }

    public void init() throws Exception {
        destroy();
        semaphore = new Semaphore(bound);
        executor = executorFactory.call();
        tasks = new ArrayList<>();
    }

    /**
     * This implementation is taken from Java Concurrency in Practice as the ideal solution to create a Executor that blocks if it's saturated. Don't change it
     */
    public Future submit(final Runnable command)
            throws InterruptedException, RejectedExecutionException {
        semaphore.acquire();
        try {
            Runnable runnable = new Runnable() {
                public void run() {
                    try {
                        persistenceContextService.run(new Callable() {
                            public Object call() throws Exception {
                                command.run();
                                return null;
                            }
                        });
                    } finally {
                        semaphore.release();
                    }
                }
            };
            Future future = executor.submit(runnable);
            tasks.add(future);
            return future;
        } catch (RejectedExecutionException e) {
            semaphore.release();
            throw e;
        }
    }

    public void waitToFinish() {
        if (tasks != null) {
            for (Future future : tasks) {
                try {
                    future.get();
                } catch (InterruptedException e) {
                    // e.printStackTrace();
                } catch (ExecutionException e) {
                    // e.printStackTrace();
                }
            }
            tasks.clear();
        }
    }

    public void destroy() {
        waitToFinish();
        if (executor != null) {
            executor.shutdown();
            try {
                if (!executor.awaitTermination(2, TimeUnit.SECONDS)) {
                    executor.shutdownNow();
                }
            } catch (InterruptedException e) {
                ; // Nothing
            } finally {
                executor.shutdownNow();
            }
        }
    }


}

