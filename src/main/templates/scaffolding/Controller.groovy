<%=packageName ? "package ${packageName}" : ''%>


import mamespin.User

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ${className}Controller {

    static namespace = "admin"

    static allowedMethods = [save: "POST", update: "POST"]

    def index(Integer max, String search) {
        params.max = Math.min(max ?: 50, 100)
        def list =  ${className}.createCriteria().list(params) {
            or {
                if (search) {
                    // like("", "%\${search.toLowerCase()}%")
                }
            }
        }
        [${propertyName}List: list, ${propertyName}: list.totalCount, search: search]
    }

    def show(${className} ${propertyName}) {
        if (${propertyName} == null) {
            notFound()
            return
        }
        [${propertyName}:${propertyName}]
    }

    def create() {
        [${propertyName}:new ${className}(params)]
    }

    @Transactional
    def save(${className} ${propertyName}) {
        if (${propertyName} == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (${propertyName}.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render model:[${propertyName}:${propertyName}], view:'edit'
            return
        }

        ${propertyName}.save flush:true

//        flash.message = message(code: 'default.created.message', args: [message(code: '${propertyName}.label', default: '${className}'), ${propertyName}.id])
        redirect action: "show", params: [id:${propertyName}.id]
    }

    def edit(${className} ${propertyName}) {
        [${propertyName}:${propertyName}]
    }

    @Transactional
    def update(${className} ${propertyName}) {
        if (${propertyName} == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (${propertyName}.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render model:[${propertyName}:${propertyName}], view:'edit'
            return
        }

        ${propertyName}.save flush:true

//        flash.message = message(code: 'default.updated.message', args: [message(code: '${propertyName}.label', default: '${className}'), ${propertyName}.id])
        redirect action: "show", params: [id:${propertyName}.id]
    }

    @Transactional
    def delete(${className} ${propertyName}) {

        if (${propertyName} == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        ${propertyName}.delete flush:true

//        flash.message = message(code: 'default.deleted.message', args: [message(code: '${propertyName}.label', default: '${className}'), ${propertyName}.id])
        redirect action: "index"
    }

    protected void notFound() {
        flash.message = message(code: 'default.not.found.message', args: [message(code: '${propertyName}.label', default: '${className}'), params.id])
        redirect action: "index"
    }
}
