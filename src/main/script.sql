CREATE DATABASE crypto CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

alter table currency add index rank(fiat, rank);

alter table trade add index date(user_id, trade_date);