;(function ($, window, document, undefined) {

    var configure = function () {
        var s = document.body || document.documentElement, s = s.style, prefixAnimation = '', prefixTransition = '';

        if (s.WebkitAnimation == '') prefixAnimation = '-webkit-';
        if (s.MozAnimation == '') prefixAnimation = '-moz-';
        if (s.OAnimation == '') prefixAnimation = '-o-';

        if (s.WebkitTransition == '') prefixTransition = '-webkit-';
        if (s.MozTransition == '') prefixTransition = '-moz-';
        if (s.OTransition == '') prefixTransition = '-o-';

        $.fn.extend({
            onCSSAnimationEnd: function (callback) {
                var $this = $(this).eq(0);
                $this.one('webkitAnimationEnd mozAnimationEnd oAnimationEnd oanimationend animationend', callback);
                if (( prefixAnimation == '' && !( 'animation' in s ) ) || $this.css(prefixAnimation + 'animation-duration') == '0s') callback();
                return this;
            },
            onCSSTransitionEnd: function (callback) {
                var $this = $(this).eq(0);
                $this.one('webkitTransitionEnd mozTransitionEnd oTransitionEnd otransitionend transitionend', callback);
                if (( prefixTransition == '' && !( 'transition' in s ) ) || $this.css(prefixTransition + 'transition-duration') == '0s') callback();
                return this;
            }
        });
    }

    var createDom = function (pair) {
        var wrapper = document.getElementById("content");
        var div = document.createElement("div");
        var html = '<div class="wrapper">';
        html += '<h1><span id="fsym_' + pair + '"></span> - <span id="tsym_' + pair + '"></span>   <strong><span class="price" id="price_' + pair + '"></span></strong></h1>';
        html += '<div class="label">24h Change: <span class="value" id="change_' + pair + '"></span> (<span class="value" id="changepct_' + pair + '"></span>)</div>';
        html += '<div class="label">Last Market: <span class="market" id="market_' + pair + '"></span></div>';
        html += '<div class="label">Last Trade Id: <span class="value" id="tradeid_' + pair + '"></span></div>';
        html += '<div class="label">Last Trade Volume: <span class="value" id="volume_' + pair + '"></span></div>';
        html += '<div class="label">Last Trade VolumeTo: <span class="value" id="volumeto_' + pair + '"></span></div>';
        html += '<div class="label">24h Volume: <span class="value" id="24volume_' + pair + '"></span></div>';
        html += '<div class="label">24h VolumeTo: <span class="value" id="24volumeto_' + pair + '"></span></div>';
        html += '<div class="source"> Source: <a href="http://www.cryptocompare.com">CryptoCompare</a></div>';
        html += '</div>';
        div.innerHTML = html;
        wrapper.appendChild(div);
    };

    var displayQuote = function (_quote) {
        var fsym = CCC.STATIC.CURRENCY.SYMBOL[_quote.FROMSYMBOL];
        var tsym = CCC.STATIC.CURRENCY.SYMBOL[_quote.TOSYMBOL];
        var pair = _quote.FROMSYMBOL + _quote.TOSYMBOL;
        console.log(pair, _quote);
        document.getElementById("market_" + pair).innerHTML = _quote.LASTMARKET;
        document.getElementById("fsym_" + pair).innerHTML = _quote.FROMSYMBOL;
        document.getElementById("tsym_" + pair).innerHTML = _quote.TOSYMBOL;
        document.getElementById("price_" + pair).innerHTML = _quote.PRICE;
        document.getElementById("volume_" + pair).innerHTML = CCC.convertValueToDisplay(fsym, _quote.LASTVOLUME);
        document.getElementById("volumeto_" + pair).innerHTML = CCC.convertValueToDisplay(tsym, _quote.LASTVOLUMETO);
        document.getElementById("24volume_" + pair).innerHTML = CCC.convertValueToDisplay(fsym, _quote.VOLUME24HOUR);
        document.getElementById("24volumeto_" + pair).innerHTML = CCC.convertValueToDisplay(tsym, _quote.VOLUME24HOURTO);
        document.getElementById("tradeid_" + pair).innerHTML = _quote.LASTTRADEID.toFixed(0);
        document.getElementById("tradeid_" + pair).innerHTML = _quote.LASTTRADEID.toFixed(0);
        document.getElementById("change_" + pair).innerHTML = CCC.convertValueToDisplay(tsym, _quote.CHANGE24H);
        document.getElementById("changepct_" + pair).innerHTML = _quote.CHANGEPCT24H.toFixed(2) + "%";

        if (_quote.FLAGS === "1") {
            document.getElementById("price_" + pair).className = "up";
        }
        else if (_quote.FLAGS === "2") {
            document.getElementById("price_" + pair).className = "down";
        }
        else if (_quote.FLAGS === "4") {
            document.getElementById("price_" + pair).className = "";
        }
    }

    var quote = {};
    var socket

    window.CCStream = {
        onUpdate: null,
        start: function (holdings, fiat) {
            configure()
            socket = io.connect('https://streamer.cryptocompare.com/');
            socket.on("m", function (message) {
                var messageType = message.substring(0, message.indexOf("~"));
                var res = {};
                if (messageType === CCC.STATIC.TYPE.CURRENTAGG) {
                    res = CCC.CURRENT.unpack(message);
                    // console.log(res);
                    var _quote = CCStream.updateQuote(res);

                    // displayQuote(_quote);
                    if (_quote) {
                        CCStream.onUpdate(_quote);
                    }

                }
            });
        },
        subscribeHoldingsToFiat: function (holdings, fiat, options) {
            //Format: {SubscriptionId}~{ExchangeName}~{FromSymbol}~{ToSymbol}
            //Use SubscriptionId 0 for TRADE, 2 for CURRENT and 5 for CURRENTAGG
            //For aggregate quote updates use CCCAGG as market
            var subscription = []; // '5~CCCAGG~BTC~EUR','5~CCCAGG~ETH~EUR'];
            var holdingCurrencies = Object.keys(holdings);
            subscription.push("5~CCCAGG~ETH~" + fiat);
            subscription.push("5~CCCAGG~BTC~" + fiat);
            for (var i = 0, len = holdingCurrencies.length; i < len; i++) {
                var coin = holdingCurrencies[i];
                subscription.push("5~CCCAGG~" + coin + "~" + fiat);
                subscription.push("5~CCCAGG~" + coin + "~BTC");
                subscription.push("5~CCCAGG~" + coin + "~ETH");
            }
            socket.emit('SubAdd', {subs: Array.from(new Set(subscription))});

            CCStream.onUpdate = function (_quote) {
                updateHoldingsFiat(holdings, fiat, options, _quote);
            }
        },
        updateQuote: function (_quote) {
            if (isNaN(_quote.PRICE)) {
                return;
            }
            var keys = Object.keys(_quote);
            var pair = _quote.FROMSYMBOL + _quote.TOSYMBOL;
            if (!quote.hasOwnProperty(pair)) {
                quote[pair] = {}
                // createDom(pair);
            }

            for (var i = 0; i < keys.length; ++i) {
                quote[pair][keys[i]] = _quote[keys[i]];
            }
            quote[pair]["CHANGE24H"] = quote[pair]["PRICE"] - quote[pair]["OPEN24HOUR"];
            quote[pair]["CHANGEPCT24H"] = quote[pair]["CHANGE24H"] / quote[pair]["OPEN24HOUR"] * 100;
            // console.log(quote);
            return quote[pair];
        }
    }

    var updateHoldingsFiat = function (holdings, fiat, options, _quote) {
        var from = _quote.FROMSYMBOL;
        var to = _quote.TOSYMBOL;

        if (to != fiat) {
            if (quote.hasOwnProperty(from + fiat)) {
                // Me llega una cotizacion contra ETH o BTC y ya la tengo en FIAT, la borro
                socket.emit('SubRemove', {subs: ['5~CCCAGG~' + from + '~' + to]});
                delete quote[from + to];
                return;
            } else if (to == "ETH" && quote.hasOwnProperty(from + "BTC")) {
                // Me llega una cotizacion contra ETH y ya la tengo en BTC, tambien la borro
                socket.emit('SubRemove', {subs: ['5~CCCAGG~' + from + '~' + to]});
                delete quote[from + to];
                return;
            }
        }

        var price = _quote.PRICE;
        if (to != fiat) {
            if (!quote.hasOwnProperty(to + fiat)) {
                console.log("No hay conversion intermedia todavia");
                return;
            }
            // El precio me esta viniendo en BTC o ETH
            price = price * quote[to + fiat].PRICE;
            if (isNaN(price)) {
                console.log("oh no");
                return;
            }
        }

        var currency = from;
        if (!holdings[currency]) {
            return
        }

        var var24h = _quote.CHANGEPCT24H.toFixed(2);
        var amount = holdings[currency].amount;
        var unitCost = holdings[currency].unitCost;
        var cost = amount * unitCost;
        var value = amount * price;
        var profit = value - cost;
        var varProfit = (cost ? (profit / cost) * 100 : 0).toFixed(2)
        var volume24h = _quote.VOLUME24HOURTO.toFixed(2)

        var colorUp = options && options.classes && options.classes.up || "text-success";
        var colorDown = options && options.classes && options.classes.down || "text-danger";

        var greenUp = '<span class="caret caret-up '+colorUp+'"></span>'
        var redDown = '<span class="caret '+colorDown+'"></span>'


        var valueUnitUpDown = "";
        var $valueUnit = $(".valueUnit_" + currency);
        if (_quote.FLAGS === "1") { // Sube
            if (options && options.valueUnit == "color") {
                $valueUnit.removeClass(colorUp+" "+colorDown).addClass(colorUp);
            } else if (options && options.valueUnit == "caret") {
                valueUnitUpDown = greenUp + " ";
            }
        } else if (_quote.FLAGS === "2") { // Baja
            if (options && options.valueUnit == "color") {
                $valueUnit.removeClass(colorUp+" "+colorDown).addClass(colorDown);
            } else if (options && options.valueUnit == "caret") {
                valueUnitUpDown = redDown + " ";
            }
        } else if (_quote.FLAGS === "4") { // Baja

        }
        $valueUnit.html(valueUnitUpDown + price.toFixed(2));

        var $var24h = $(".var24h_" + currency);
        $var24h.html(var24h)
        if (options && options.var24h == "color") {
            $var24h.removeClass(colorUp+" "+colorDown);
            if (var24h > 0) {
                $var24h.addClass(colorUp);
            } else if (var24h < 0) {
                $var24h.addClass(colorDown);
            }
        }


        var $value = $(".value_" + currency);
        if (options && options.valueFormat == "K") {
            $value.html(nFormatter(value));
        } else {
            $value.html(value.toFixed(2));
        }
        if (options && options.value == "color") {
            $value.removeClass(colorUp+" "+colorDown);
            if (profit > 0) {
                $value.addClass(colorUp);
            } else if (profit < 0) {
                $value.addClass(colorDown);
            }
        }

        var $valueProfit = $(".valueProfit_" + currency);
        $valueProfit.html(profit.toFixed(2));
        if (options && options.valueProfit == "color") {
            $valueProfit.removeClass(colorUp+" "+colorDown);
            if (profit > 0) {
                $valueProfit.addClass(colorUp);
            } else if (profit < 0) {
                $valueProfit.addClass(colorDown);
            }
        }

        var $varProfit = $(".varProfit_" + currency)
        if (options && options.varProfit == "color") {
            $varProfit.removeClass(colorUp+" "+colorDown);
            if (profit > 0) {
                $varProfit.addClass(colorUp);
            } else if (profit < 0) {
                $varProfit.addClass(colorDown);
            }
        }
        var varProfitWithCaret = ""
        if (options && options.varProfit == "caret") {
            if (profit > 0) {
                varProfitWithCaret = greenUp
            } else if (profit < 0) {
                varProfitWithCaret = redDown
            }
        }
        $varProfit.html(varProfit+" %"+varProfitWithCaret);



        $(".volume24hFiat_" + currency).html(volume24h)

        var tds = $(".holding_" + currency).find('td');

        for (var i = 0, len = tds.length; i < len; i++) {
            $(tds[i]).addClass("flash").onCSSAnimationEnd(function () {
                $(this).removeClass("flash");
            });
        }

        if (options && options.onUpdate) {
            options.onUpdate({
                currency: currency,
                var24h: var24h,
                cost: cost.toFixed(2),
                value: value.toFixed(2),
                profit: profit.toFixed(2),
                varProfit: varProfit
            }, _quote);
        }
    }
})(jQuery, window, document);


