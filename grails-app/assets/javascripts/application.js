// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require lib/tether/tether.min.js
//= require lib/bootstrap/bootstrap.min.js
//= require plugins.js
//= require_self


function nFormatter(num, digits) {
  digits = digits || 1
  var si = [
    { value: 1E18, symbol: "E" },
    { value: 1E15, symbol: "P" },
    { value: 1E12, symbol: "T" },
    { value: 1E9,  symbol: "G" },
    { value: 1E6,  symbol: "M" },
    { value: 1E3,  symbol: "k" }
  ], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;
  for (i = 0; i < si.length; i++) {
    if (num >= si[i].value) {
      return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }
  }
  return num.toFixed(digits).replace(rx, "$1");
}

function emptyIfNull(value) {
    return value != null ? value : ""
}

function dateFormatter(data, rowData, index) {
    if (!data) return ""
    var date = moment(new Date(data)).format('DD/MMM/YYYY')
    if (date == moment().format('DD/MMM/YYYY')) {
        date = "Today"
    }
    return date;
}

function dateTimeFormatter(data, rowData, index) {
    if (!data) return ""
    var mome = moment(new Date(data))
    var date = mome.format('DD/MMM/YYYY');
    if (date == moment().format('DD/MMM/YYYY')) {
        date = "Today"
    }
    return date + " <span class='time small'>" + mome.format("HH:mm") + "</span>";
}

function booleanFormatter(data, rowData, index) {
    if (data == "true" || data == true) {
        return "<i class=\"font-icon font-icon-check-bird color-green\"></i>"
    } else {
        return "<i class=\"font-icon font-icon-close-2 color-red\"></i>"
    }

}
function amountFormatter(data, rowData, index) {
    if (!data) return ""
    var parts = data.split("|")
    return parts[0] + " <span class='currency'>" + parts[1] + "</span>"
}

function amountCellStyle(value, row, index, field) {
    return {
        css: {
            "color": "#000"
            // "font-weight": "bold"
        }
    };
}

function numberZeroEmptyFormatter(value, row, index, field) {
    if (value == 0 || value == null || value == undefined) return ""
    return value
}

function currencyCellStyle(value, row, index, field) {
    return {
        // css: {"border-left": "0",
        //       "padding-left": "0"}
    };
}

function createDetailFormatter(field) {
    return function(index, row, $detail) {
        $detail.html(row[field]);
    }
}

function trimFormatter(data, row, index) {
    if (data && data.length > 40) {
        return data.substring(0, 40)+ "..."
    }
    return data ? data : ""
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}




var defaultTableOptions = {
    iconsPrefix: 'font-icon',
    showPaginationSwitch: false,
    formatLoadingMessage: function () {
        return '<div id="loadingPane" style="display: block; font-size:190px; text-align: center; color: #DDD;padding-top:80px">\n' +
            '    <i class="fa fa-spin fa-refresh" aria-hidden="true"></i>\n' +
            '</div>';
    },
    icons: {
        paginationSwitchDown: 'font-icon-arrow-square-down',
        paginationSwitchUp: 'font-icon-arrow-square-down up',
        refresh: 'font-icon-refresh',
        toggle: 'font-icon-list-square',
        columns: 'font-icon-list-rotate',
        export: 'font-icon-download',
        detailOpen: 'font-icon-plus',
        detailClose: 'font-icon-minus-1'
    },
    rowStyle: function rowStyle(row, index) {
        return {classes: 'text-nowrap'}
    },
    paginationPreText: '<i class="font-icon font-icon-arrow-left"></i>',
    paginationNextText: '<i class="font-icon font-icon-arrow-right"></i>'
}

jQuery.fn.visible = function() {
    return this.css('visibility', 'visible');
};

jQuery.fn.invisible = function() {
    return this.css('visibility', 'hidden');
};

