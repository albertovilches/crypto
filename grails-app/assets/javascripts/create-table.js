function createTable($table, $bulk, options, onBulkClick) {
    var selections = [];

    window.operateEvents = {
        'click .like': function (e, value, row, index) {
            alert('You click like action, row: ' + JSON.stringify(row));
        },
        'click .remove': function (e, value, row, index) {
            $table.bootstrapTable('remove', {
                field: 'id',
                values: [row.id]
            });
        }
    };

    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }


    var newOpts = $.extend({}, defaultTableOptions, options);
    $table.bootstrapTable(newOpts);

    $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
        $bulk.prop('disabled', !$table.bootstrapTable('getSelections').length);
        // save your data, here just save the current page
        selections = getIdSelections();
        // push or splice the selections if you want to save all data selections
    });

    $bulk.click(function (e) {
        var ids = getIdSelections();
        var button = e.target.id;
        console.log(button)
        if (typeof onBulkClick == "function") {
            onBulkClick(button, ids)
        } else if (typeof onBulkClick == "object") {
            onBulkClick[button](ids)
        } else if (typeof onBulkClick == "string") {
            $.ajax(onBulkClick, {
                data: {ids: ids.join(","), button: button},
                success: function () {
                    $table.bootstrapTable('refresh')
                },
                error: function (xhr, status, thrown) {
                    alert(status + " / " + thrown)
                }
            })


        }
        // $table.bootstrapTable('remove', {
        //     field: 'id',
        //     values: ids
        // });
        // $bulk.prop('disabled', true);
    });

    $('#toolbar').find('select').change(function () {
        $table.bootstrapTable('refreshOptions', {
            exportDataType: $(this).val()
        });
    });

    return $table
}
