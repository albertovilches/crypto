package mamespin

import crypto.CurrencyService

class BootStrap {

    CurrencyService currencyService

    def init = { servletContext ->

        User u = User.findOrCreateWhere(username: "admin")
        if (!u.id) {
            u.password = "admin"
            u.admin = true
            u.email = "vilches@gmail.com"
            u.save(failOnError: true)
        }

        currencyService.refreshCacheFromDb()
    }
    def destroy = {
    }
}
