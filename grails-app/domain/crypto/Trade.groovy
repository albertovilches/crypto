package crypto

import mamespin.User

class Trade {

    CurrencyService currencyService

    User user
    boolean hidden = false

    enum Type {
        trade(false, false),      // intercambio

        deposit(true, false),
        mining(true, false),
        income(true, false),
        gift(true, false),

        expend(false, true),
        withdraw(false, true),
        lost(false, true)

        boolean inc
        boolean out
        Type(boolean i, boolean o) {
            inc = i
            out = o
        }

        Set ins() {
            values().findAll { it.inc == true}
        }

        Set outs() {
            values().findAll { it.out == true}
        }
    }

    Type type
    BigDecimal inAmount
    Currency inCurrency
    BigDecimal inValueFiat

    BigDecimal inOldAvgUnitPriceFiat // Unit price del holding actual de la moneda que compramos (Holding[inCurrency].unitPriceFiat)
    BigDecimal inNewAvgUnitPriceFiat // Nuevo unit price con la compra

    BigDecimal unitPrice    // precio unitario de la compra = outAmount / inAmount
    BigDecimal outCostFiat  // coste de la compra = outAmount * Holding[outCurrency].unitPriceFiat

    BigDecimal outAmount
    Currency outCurrency

    String exchange
    String comment
    Date tradeDate

    Date dateCreated
    Date lastUpdated

    static mapping = {
        version(false)
    }

    static transients = ['currencyService', 'fastInCurrency', 'fastOutCurrency']

    static constraints = {

        inAmount(blank:true, nullable: true, scale: 10)
        inCurrency(nullable: true)
        inOldAvgUnitPriceFiat(blank:true, nullable: true, scale: 10)
        inNewAvgUnitPriceFiat(blank:true, nullable: true, scale: 10)
        inValueFiat(blank:true, nullable: true, scale: 10)

        unitPrice(blank:true, nullable: true, scale: 10)
        outCostFiat(blank:true, nullable: true, scale: 10)

        outAmount(blank:true, nullable: true, scale: 10)
        outCurrency(nullable: true)

        exchange(blank:true, nullable: true)
        comment(blank:true, nullable: true)
    }

    Currency getFastInCurrency() {
        return currencyService.getCurrency(inCurrencyId)
    }

    Currency getFastOutCurrency() {
        return currencyService.getCurrency(outCurrencyId)
    }



    def beforeValidate = {
    }
}
