package crypto

class Currency {

    String name
    String displayName
    int decimals
    String ccImageUrl
    String algorithm
    String parsedTotalCoinSupply
    Long totalCoinSupply
    Long currentCoinSupply
    String proofType
    String ccUrl
    String ccId
    String twitter
    Status status = Status.ok
    String message
    String affiliateUrl
    Boolean mediaOk = false
    Status pairsStatus
    int rank = 999999
    Integer pairsFrom
    Integer pairsTo
    Integer pairsWrong

    Date startDate
    Date dateCreated
    Date lastUpdated
    boolean enabled = true

    boolean cached = false

    static transients = ['cached']

    enum Status {
        ok, error, not_found
    }

    boolean fiat = false

    static mapping = {
        dynamicUpdate true
        version(false)
    }

    static constraints = {
        name(unique:true, maxSize: 20)
        ccImageUrl(nullable: true)
        algorithm(nullable: true)
        parsedTotalCoinSupply(nullable: true)
        totalCoinSupply(nullable: true)
        currentCoinSupply(nullable: true)
        proofType(nullable: true)
        ccUrl(nullable: true)
        ccId(nullable: true)
        message(nullable: true)
        twitter(nullable: true)
        affiliateUrl(nullable: true)
        startDate(nullable: true)
        pairsStatus(nullable: true)
        status(nullable: true)
        rank(nullable: false)
        pairsFrom(nullable: true)
        pairsTo(nullable: true)
        pairsWrong(nullable: true)
    }

    String toString() {
        return name
    }

    String ccFullImageUrl() {
        return "https://www.cryptocompare.com/${ccImageUrl}"
    }

}
