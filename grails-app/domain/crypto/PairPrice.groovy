package crypto

class PairPrice {

    Currency from
    Currency to
    String sfrom
    String sto

    Date firstPriceDate
    Date lastPriceDate
    Date dateCreated

    Date lastUpdated
    Date lastImport

    Status status = Status.empty

    String message

    enum Status {
        empty, importing, ok, error
    }

    static mapping = {
        firstPriceDate(type: 'date')
        lastPriceDate(type: 'date')
        dynamicUpdate true
        version(false)
    }

    static constraints = {
        sfrom(maxSize: 20)
        sto(maxSize: 20)
        firstPriceDate(nullable: true)
        lastPriceDate(nullable: true)
        lastImport(nullable: true)
        message(nullable: true)
    }


}
