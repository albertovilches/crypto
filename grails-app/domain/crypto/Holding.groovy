package crypto

import mamespin.User

class Holding {

    User user

    CurrencyService currencyService

    Date holdDate
    BigDecimal totalIn = 0
    BigDecimal totalOut = 0
    BigDecimal amount = 0
    Currency currency
    String currencyName
    boolean currencyFiat
    BigDecimal costFiat = 0
    BigDecimal valueFiat = 0
    BigDecimal unitPriceFiat = 0
    BigDecimal profit = 0
    boolean last

    Date dateCreated
    static constraints = {
        currencyName(maxSize: 20)
        holdDate(maxSize: 6)
        totalIn(scale: 10)
        totalOut(scale: 10)
        amount(scale: 10)
        costFiat(scale: 10)
        valueFiat(scale: 10)
        profit(scale: 10)
        currency()
        unitPriceFiat(scale: 10)
    }

    static transients = ['currencyService', 'unrealizedProfit', 'varProfit', 'price', 'fastCurrency']

    // Si vendo, cuando % esto ganando
    BigDecimal getVarProfit() {
        costFiat ? (unrealizedProfit / costFiat) : 0
    }

    Currency getFastCurrency() {
        return currencyService.getCurrency(currencyName)
    }

    // Lo que ganaria ahora mismo si vendiera
    BigDecimal getUnrealizedProfit() {
        return valueFiat - costFiat
    }

    // El precio de venta unitario a la fecha (el valor / cantidad)
    BigDecimal getPrice() {
        return amount ? (valueFiat / amount) : 0
    }

    static mapping = {
        holdDate(type: 'date')
        version(false)
    }

    def beforeValidate = {
        currencyName = currency?.name
        currencyFiat = currency?.fiat
    }

}
