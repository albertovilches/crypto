package crypto

class SystemEvent {

    Date dateCreated
    String message
    String extraInfo
    String stacktrace
    Severity severity
    Section section

    enum Section {
        downloadHistoric, checkingPair, updatingHistoric, parsingHistoric

    }
    enum Severity {
        info, warn, error
    }


    static constraints = {
        message(nullable: true)
        extraInfo(nullable: true)
        stacktrace(nullable: true)
    }
    static mapping = {
//        dynamicUpdate true
        version(false)
        extraInfo(type: 'text')
        stacktrace(type: 'text')
    }


}
