package crypto

import mamespin.User

class UserCurrency {

    User user
    Currency currency
    Star star
    String comment
    boolean fav

    enum Star {
        yellow, blue, red, green, warn, love, excl, quest, follow
    }

    static constraints = {
        comment(nullable: true)
        star(nullable: true)
        fav(nullable: false)
    }

    static mapping = {
        version(false)
    }
}
