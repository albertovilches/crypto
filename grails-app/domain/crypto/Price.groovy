package crypto

class Price {

    static int SCALE = 10
    static int DATABASE_JDBC_ROUND = java.math.BigDecimal.ROUND_HALF_UP

    Currency from
    Currency to

    BigDecimal price
    Date date


    static mapping = {
        date(type: 'date')
        version(false)
    }
    
    static constraints = {
        price(scale: Price.SCALE)
        date(unique: ['from', 'to'])
    }


}
