package mamespin

class Tag {

    String name
    boolean hidden = false

    static constraints = {
        name(maxSize: 20)
    }
}
