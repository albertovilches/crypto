/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin

import crypto.Currency
import groovy.time.TimeCategory
import tools.BCrypt
import tools.StringTools

class User {

    String uuid
    String email
    String username
    String usernameLC
    String pwdHash
    Date dateCreated
    Date lastLogin

    Currency defaultCurrency

    String token
    Date tokenExpires
    TokenAction tokenAction
    enum TokenAction { recoverPwd /*, changeMail, invite*/ }

    int level = 0
    State state = State.ok
    enum State { ok, banned, disabled }

    boolean admin = false

    boolean step1 = false
//    boolean step2 = false /**/
//    boolean step3 = false

    SignService signService

    boolean hasPassword() {
        pwdHash
    }

    void setPassword(String pwd) {
        pwdHash = BCrypt.hashpw(pwd)
    }

    static transients = ['signService', 'resourceService', 'humanCredits', 'limitByLevelString', 'kbLimitByLevel']



    static constraints = {
        uuid(blank: false, unique: true, maxSize: 36)
        email(blank: false, email: true, unique: true, maxSize: 100)
        username(nullable: true, maxSize: 50)
        usernameLC(nullable: true, unique: true, maxSize: 50)
        pwdHash(nullable: true, maxSize: 70)
        dateCreated(blank: false)
        lastLogin(nullable: true)
        token(nullable: true, maxSize: 50)
        tokenExpires(nullable: true)
        tokenAction(nullable: true)
        state(nullable: false)
        level(nullable: false)
        defaultCurrency(nullable: true)
    }

    static mapping = {
        tokenAction(length: 10)
        state(length: 10)
        dynamicUpdate true
        version(false)
    }

    def beforeValidate() {
        usernameLC = username?.trim()?.toLowerCase()
        email = email?.trim()?.toLowerCase()
        uuid = uuid?:UUID.randomUUID().toString()
    }

    boolean checkPwd(String pwd) {
        if (!pwdHash || !pwd) return false
        return BCrypt.checkpw(pwd, pwdHash)
    }
}

class BanIp {
    String ip
    Date expires

    static mapping = {
        ip(blank: false, maxSize: 46)
    }
    static void until(String ip, int minutes) {
        Date until
        use(TimeCategory) {
            until = new Date() + minutes.minutes
        }
        BanIp ban = BanIp.findOrCreateWhere(ip: ip)
        ban.expires = ban.expires ? new Date(Math.max(ban.expires.time, until.time)) : until
        ban.save(flush: true)
    }

    static Date isBannedUntil(String ip) {
        // TODO: se deberia banear por partes: solo el sign y dejar el recover, y luego banear el recover
        BanIp.findByIpAndExpiresGreaterThan(ip, new Date())?.expires
    }

    static void removeBan(String ip) {
        BanIp.findAllByIp(ip).each {
            it.delete(flush:true)
        }
    }
}

class UserAction {
    String ip
    User user
    Date dateCreated
    Type type

    static int MAX_WRONG_SINGIN_TIMES = 5
    static int MAX_WRONG_SINGIN_MINUTES = 5
    static int BAN_MINUTES = 1

    static constraints = {
        user(nullable: true)
        ip(blank: false, maxSize: 46)
    }

    static mapping = {
        type(length: 20)
    }

    enum Type {
        signinOk,
        signinCookieOk,
        signinWrong,
        signout,
        sendRecoverPwdOk,
        sendRecoverPwdWrong,
        recoverPwdOk,
        recoverPwdWrong,
        sendRegisterDupeUser, // 20 max!
        sendRegisterNewUser
    }

    static addSigninWrong(String ip, User user = null) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.signinWrong).save(flush: true)
        if (countEventMinutesAgo(ip, UserAction.Type.signinWrong, MAX_WRONG_SINGIN_MINUTES) > MAX_WRONG_SINGIN_TIMES) {
            BanIp.until(ip, BAN_MINUTES)
        }
    }

    static void addSendRegisterDupeUser(String ip, User user) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.sendRegisterDupeUser).save(flush: true)
    }

    static void addSendRegisterNewUser(String ip, User user) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.sendRegisterNewUser).save(flush: true)
    }

    static void addSendRecoverPwdOk(String ip, User user) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.sendRecoverPwdOk).save(flush: true)
    }

    static void addSendRecoverPwdWrong(String ip) {
        new UserAction(ip: ip, type: UserAction.Type.sendRecoverPwdWrong).save(flush: true)
    }

    static void addRecoverPwdOk(String ip, User user) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.recoverPwdOk).save(flush: true)
    }

    static void addRecoverPwdWrong(String ip) {
        new UserAction(ip: ip, type: UserAction.Type.recoverPwdWrong).save(flush: true)
    }


    static void addSigninOk(String ip, User user) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.signinOk).save(flush: true)
    }

    static void addSigninCookieOk(String ip, User user) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.signinCookieOk).save(flush: true)
    }

    static void addSignout(String ip, User user) {
        new UserAction(ip: ip, user: user, type: UserAction.Type.signout).save(flush: true)
    }

    static int countEventMinutesAgo(String ip, Type type, int ago) {
        Date since = minutesAgoToSince(ago)
        int count = UserAction.createCriteria().count {
            ge("dateCreated", since)
            eq("type", type)
            eq("ip", ip)
        }
        return count
    }

    private static Date minutesAgoToSince(int ago) {
        Date since
        use(TimeCategory) {
            since = new Date() - ago.minutes
        }
        return since
    }

}