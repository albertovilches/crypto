package mamespin

import tools.StringTools

class Category {

    String name
    String description
    String url
    Category parent
    String image

    boolean hidden = false

//    CreditType creditType = CreditType.normal
//    DownloadType type = DownloadType.slow


    static constraints = {
        name()
        description(nullable: true)
        url(nullable: true, url: true, maxSize: 255)
        parent(nullable: true)
        image(nullable: true, maxSize: 255)
    }

    static transients = ['resourcePackCount', 'size', 'humanSize']
    int resourcePackCount = 0
    long size = 0

    String getHumanSize() {
        StringTools.humanReadableString(size)
    }

    static mapping = {
        description(type: 'text')
//        creditType(length: 10)
    }
}

enum CreditType {
    normal, free
}

enum DownloadType {
    slow, fast
}
