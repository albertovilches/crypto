package crypto

import grails.converters.JSON
import grails.core.GrailsApplication
import groovy.transform.CompileStatic
import org.grails.plugins.web.taglib.FormatTagLib
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.beans.factory.InitializingBean
import tools.RoundedMetricPrefixFormat

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols


class CurrencyService implements InitializingBean {

    GrailsApplication grailsApplication
    FormatTagLib formatTagLib
    static transactional = false

    Set configFiatNamesSet

    Map fiatIndex
    List fiats
    Map cryptoIndex
    List cryptos

    Currency BTC
    Currency ETH

    Currency USD
    Currency EUR
    Currency CNY
    Currency GBP

    @Override
    void afterPropertiesSet() throws Exception {
        configFiatNamesSet = grailsApplication.config.app.fiats as Set
        formatTagLib = grailsApplication.mainContext.getBean('org.grails.plugins.web.taglib.FormatTagLib')
    }

    String typeAheadCoinsJson
    String typeAheadCoinsJsonG() {

        ((cryptos+fiats).collect { [name:it.name, tokens: "${it.displayName} ${it.name}", img: it.ccFullImageUrl(),  displayName: it.displayName] } as JSON).toString()

    }

    void refreshCacheFromDb() {
        fiats = findAllEnabledFiatCurrencies(null)
        cryptos = findAllEnabledCryptoCurrencies(null)

        fiatIndex = fiats.inject([:]) { Map map, Currency fiat ->
            map[fiat.name] = fiat
            map["ID:${fiat.id}".toString()] = fiat
            fiat.cached = true
            return map
        }
        cryptoIndex = cryptos.inject([:]) { Map map, Currency crypto ->
            map[crypto.name.toUpperCase()] = crypto
            map["ID:${crypto.id}".toString()] = crypto
            crypto.cached = true
            return map
        }
        USD = fiatIndex.USD
        EUR = fiatIndex.EUR
        GBP = fiatIndex.GBP
        CNY = fiatIndex.CNY

        BTC = cryptoIndex.BTC
        ETH = cryptoIndex.ETH

        typeAheadCoinsJson = ((cryptos+fiats).collect { it.displayName+" (${it.name})" } as JSON).toString()

        fiats*.discard()
        cryptos*.discard()
    }

    Currency getCurrency(Long id) {
        return getCurrency("ID:${id}")
    }

    Currency getCurrency(String name) {
        name = name.toUpperCase()
        if (name == "IOTA") name = "IOT"
        return fiatIndex[name] ?: cryptoIndex[name]
    }

    Currency findCurrencyByName(String name) {
        if (name == "IOTA") name = "IOT"
        return Currency.findByName(name)
    }

    List<Currency> findAllEnabledFiatCurrencies(Integer limit = null) {
        return findAllEnabledCurrencies(true, limit)
    }

    List<Currency> findAllEnabledCryptoCurrencies(Integer limit = null) {
        return findAllEnabledCurrencies(false, limit)
    }

    List<Currency> findAllEnabledCurrencies(boolean fiat, Integer limit = null) {
        return Currency.createCriteria().list {
            eq("fiat", fiat)
            eq("enabled", true)
            or {
                gt("pairsFrom", 0)
                gt("pairsTo", 0)
            }
            if (limit) {
                maxResults(limit)
            }
            if (fiat) {
                order("name", "asc")
            } else {
                order("rank", "asc")
            }
        }
    }

    String formatPrice(BigDecimal amount, def currency, String format = null) {
        if (format == "K" || format == "k") {
            return formatKMG(amount.longValue())
        }
        return formatTagLib.formatNumber(number: amount, format: format ?: getFormatFromCurrency(currency))
    }

    char[] suffix = [' ', 'k', 'M', 'B', 'T', 'P', 'E']

    @CompileStatic
    String formatKMG(Long numValue) {
        int value = (int) Math.floor(Math.log10(numValue))
        int base = (value / 3) as int;
        Locale locale = GrailsWebRequest.lookup().getLocale()
        if (value >= 3 && base < suffix.length) {
            DecimalFormatSymbols dcfs = locale ? new DecimalFormatSymbols(locale) : new DecimalFormatSymbols()
//          return new DecimalFormat("#0.0", dcfs).format(numValue / Math.pow(10, base * 3)) + suffix[base];
            return new DecimalFormat("#0.00", dcfs).format(numValue / Math.pow(10, base * 3)) + suffix[base]
        } else {
            return numValue as String
//            return new DecimalFormat("#,##0", dcfs).format(numValue);
        }
    }

    String getFormatFromCurrency(def currency) {
        if (currency == "%") {
            return "0.00"
        } else if (currency instanceof Currency) {
            if (currency.fiat) {
                return "0.${'0'.multiply(currency.decimals)}"
            } else {
                return "0.##"
            }
        } else {
            return "0.000"
        }
    }


}
