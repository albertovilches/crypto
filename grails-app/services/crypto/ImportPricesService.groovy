package crypto

import grails.core.GrailsApplication
import groovy.time.TimeCategory
import groovy.transform.CompileStatic
import jodd.http.HttpRequest
import jodd.json.JsonParser
import org.springframework.beans.factory.InitializingBean
import tools.BoundedPersistenceExecutor

class ImportPricesService implements InitializingBean {

    static transactional = false
    GrailsApplication grailsApplication
    PersistenceContextService persistenceContextService
    ImportCurrenciesService importCurrenciesService
    File dataFolder

    CurrencyService currencyService

    @Override
    void afterPropertiesSet() throws Exception {
        dataFolder = new File(grailsApplication.config.app.localDataFolder ?: "/srv/crypto/data")
    }

    void checkPricesInCurrencies(int minimumDays, Collection<Currency> currencies = null) {
        currencies = currencies ?:currencyService.findAllEnabledCryptoCurrencies()
        currencies.eachWithIndex { Currency currency, int indx ->
            PairPrice.withNewSession {
                List<PairPrice> pairPrices = PairPrice.findAllByFromOrToInList(currency, currencies)
                pairPrices.eachWithIndex { PairPrice pairPrice, int pairIndx ->
                    checkPricesInPairPrice(minimumDays, pairPrice)
                }
            }
        }
    }

    void checkPricesOutdated() {
        def outdated = PairPrice.createCriteria().list {
            and {
                delegate.from {
                    eq("enabled", true)
                }
                delegate.to {
                    eq("enabled", true)
                }
            }
            or {
                isNull("lastPriceDate")
                lt("lastPriceDate", new Date().clearTime())
            }
            // TODO: hacer
//            eq("enabled", true)
        }.sort { PairPrice pairPrice ->
            if (pairPrice.status != PairPrice.Status.ok || pairPrice.lastPriceDate == null) {
                return Integer.MIN_VALUE
            }
            return pairPrice.from.rank * pairPrice.to.rank
        }

        outdated.each { PairPrice pairPrice ->
            println "${pairPrice.sfrom}-${pairPrice.sto}"
        }

        outdated.each { PairPrice pairPrice ->
            checkPricesInPairPrice(0, pairPrice)
        }
    }

    // minimumDays -1 es borrar y bajar to_do
    // minimumDays 0 es solo leer los dias que faltan
    // minimumDays 1 al menos actualiza hoy (aunque esté
    void checkPricesInPairPrice(int minimumDays, PairPrice pairPrice) {
        int prices = Price.countByFromAndTo(pairPrice.from, pairPrice.to)
        try {
            String json
            if (!prices || minimumDays == -1) {
                log.info "Fetching full prices for ${pairPrice.from.name}-${pairPrice.to.name}..."
                json = requestCryptoCompareFullHistory(pairPrice.from.name, pairPrice.to.name)
            } else {
                updateLastAndFirstPrice(pairPrice)
                int days = getDaysSinceLastPrice(pairPrice) + minimumDays
                if (days > 0) {
                    log.info "${pairPrice.from.name}-${pairPrice.to.name}: ${days} days missing. Fetching prices..."
                    json = requestCryptoCompareLastDays(pairPrice.from.name, pairPrice.to.name, days + minimumDays)
                } else {
                    log.info "${pairPrice.from.name}-${pairPrice.to.name} - OK! (${pairPrice.lastPriceDate.format("dd/MM/yyyy")})"
                    markPriceAsOk(pairPrice)
                }
                pairPrice.save(flush: true)
            }

            if (json) {
                Map jsonParsed = new JsonParser().parse(json)
                if (jsonParsed.Response != "Success") {
                    log.error("Error JSON prices ${pairPrice.from.name}-${pairPrice.to.name}... ${jsonParsed.Message}")
                    markPairPriceAsError(pairPrice, jsonParsed.Message)
                } else {
                    updateHistoricPrices(minimumDays == -1 || !prices, pairPrice, jsonParsed)
                }
            }
        } catch (e) {
            log.error("Error fetching prices ${pairPrice.from.name}-${pairPrice.to.name}...", e)
            markPairPriceAsError(pairPrice, e.message)
        }
        importCurrenciesService.updateCurrencyDataFromPairs(pairPrice.from)
        importCurrenciesService.updateCurrencyDataFromPairs(pairPrice.to)
        pairPrice.from.save(flush: true)
        pairPrice.to.save(flush: true)
    }

    private void markPairPriceAsError(PairPrice pairPrice, String message) {
        pairPrice.status = PairPrice.Status.error
        pairPrice.lastUpdated = new Date()
        pairPrice.message = message
        pairPrice.save(flush: true)
    }

    private int getDaysSinceLastPrice(pairPrice) {
        int days = 0
        use(TimeCategory) {
            def duration = new Date() - pairPrice.lastPriceDate
            days = duration.days
        }
        days
    }

    @CompileStatic
    void logError(SystemEvent.Section section, String message, String extraInfo, Exception ex) {
        log.error(message, ex)
        SystemEvent.withNewSession {
            StringWriter errors = new StringWriter()
            ex.printStackTrace(new PrintWriter(errors))
            new SystemEvent(severity: SystemEvent.Severity.error, message: message, extraInfo: extraInfo, stacktrace: errors.toString(), section: section).save(flush: true)
        }
    }

    @CompileStatic
    void logWarn(SystemEvent.Section section, String message, String extraInfo = null) {
        log.warn(message)
        SystemEvent.withNewSession {
            new SystemEvent(severity: SystemEvent.Severity.warn, message: message, extraInfo: extraInfo, section: section).save(flush: true)
        }
    }

    void updateHistoricPrices(boolean onlyInsert, PairPrice pairPrice, Map data) {
        try {
            pairPrice.status = PairPrice.Status.importing
            pairPrice.lastUpdated = new Date()
            pairPrice.message = null
            pairPrice.save(flush: true)

            if (onlyInsert) {
                Price.executeUpdate("delete from Price p where p.from.id = :from and p.to.id = :to", [from: pairPrice.from.id, to: pairPrice.to.id])
                log.info("Inserting all ${data.Data.size()} prices for ${pairPrice.from.name}-${pairPrice.to.name}...")
            } else {
                log.info("Updating last ${data.Data.size()} prices for ${pairPrice.from.name}-${pairPrice.to.name}...")
            }
            data.Data.collate(300).eachWithIndex { List list, int idx ->
                Currency.withNewSession {
                    Currency.withNewTransaction {
                        list.each { Map m ->
                            checkAndUpdateHistoricPrice(onlyInsert, pairPrice, m)
                        }
                    }                                                                  1
                }
            }
            markPriceAsOk(pairPrice)
            pairPrice.lastImport = new Date()
        } catch (e) {
            log.error("Error updating  prices for ${pairPrice.from.name}-${pairPrice.to.name}...", e)
            markPairPriceAsError(pairPrice, e.message)
            logError(SystemEvent.Section.updatingHistoric, "Exception updating historic from ${pairPrice.sfrom}-${pairPrice.sto}", null, e)
        }
        updateLastAndFirstPrice(pairPrice)
        pairPrice.save(flush: true)
    }

    private void markPriceAsOk(PairPrice pairPrice) {
        pairPrice.status = PairPrice.Status.ok
        pairPrice.lastUpdated = new Date()
        pairPrice.message = null
    }

    private void updateLastAndFirstPrice(PairPrice pairPrice) {
        List prices = Price.executeQuery("select max(date), min(date) from Price p where p.from.id = :from and p.to.id = :to", [from: pairPrice.from.id, to: pairPrice.to.id])[0]
        pairPrice.lastPriceDate = prices[0] as Date
        pairPrice.firstPriceDate = prices[1] as Date
    }

    private Price checkAndUpdateHistoricPrice(boolean onlyInsert, PairPrice pairPrice, Map m) {
        Currency from = pairPrice.from
        Currency to = pairPrice.to
        BigDecimal price = new BigDecimal(m.high + m.low + m.open + m.close) / 4
        price = price.setScale(Price.SCALE, Price.DATABASE_JDBC_ROUND)
        Date date = new Date((m.time as long) * 1000)
        Price p
        try {
            if (onlyInsert) {
                p = new Price(from: from, to: to, date: date, price: price)
                p.save(flush: true)
            } else {
                p = Price.findOrCreateWhere(from: from, to: to, date: date)
                if (price.compareTo(p.price ?: -1) != 0) {
                    p.price = price
                    p.save(flush: true)
                }
            }
        } catch (e) {
            throw e
        }
        return p
    }


    String requestCryptoCompareFullHistory(String coin, String to) {
        return new HttpRequest().get("https://min-api.cryptocompare.com/data/histoday?fsym=${coin}&tsym=${to}&aggregate=1&e=CCCAGG&allData=true").send().bodyText()
    }

    String requestCryptoCompareLastDays(String coin, String to, int days) {
        return new HttpRequest().get("https://min-api.cryptocompare.com/data/histoday?fsym=${coin}&tsym=${to}&aggregate=1&e=CCCAGG&limit=${days}").send().bodyText()
    }

}