package crypto

import grails.persistence.support.PersistenceContextInterceptor
import groovy.transform.CompileStatic
import tools.BoundedPersistenceExecutor

import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor

class PersistenceContextService {

    static transactional = false

    PersistenceContextInterceptor persistenceInterceptor

    @CompileStatic
    BoundedPersistenceExecutor createBoundedPersistenceExecutor(int threads = 5) {
        BoundedPersistenceExecutor executor = new BoundedPersistenceExecutor()
        executor.persistenceContextService = this
        executor.executorFactory = { ->
            return (ThreadPoolExecutor) Executors.newFixedThreadPool(threads);
        }
        executor.init()
        return executor
    }


    @CompileStatic
    Object run(Callable callable) {
        return wrapWithPersistence(callable).call()
    }


    @CompileStatic
    Callable wrapWithPersistence(Callable callable) {
        Closure wrapped = { ->
            try {
                createPersistentContext()
                return callable.call()
            } finally {
                destroyPersistentContext()
            }
        }
        return wrapped
   	}

    @CompileStatic
    private void createPersistentContext() {
        persistenceInterceptor.init()
    }

    @CompileStatic
    private void destroyPersistentContext() {
        try {
            persistenceInterceptor.flush()
        } catch (Throwable e) {
            log.error("Failed to flush persistent context", e)
        } finally {
            try {
                persistenceInterceptor.destroy()
            } catch (Throwable e) {
                log.error("Failed to destroy persistent context", e)
            }
        }
    }

}
