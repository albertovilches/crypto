package crypto

import grails.core.GrailsApplication
import groovy.transform.CompileStatic
import jodd.http.HttpRequest
import jodd.json.JsonParser
import org.springframework.beans.factory.InitializingBean
import tools.BoundedPersistenceExecutor

class ImportCurrenciesService implements InitializingBean {

    static transactional = false
    GrailsApplication grailsApplication
    PersistenceContextService persistenceContextService
    File dataFolder
    CurrencyService currencyService
    File coinsFile
    File fiatFile

    static int collate = 300

    @Override
    void afterPropertiesSet() throws Exception {
        dataFolder = new File(grailsApplication.config.app.localDataFolder ?: "/srv/crypto/data")
        coinsFile = new File(dataFolder, "coins.json")
        fiatFile = new File(dataFolder, "fiats.xml")
    }

    @CompileStatic
    private File createFullInfoFile(Currency currency) {
        File f = new File(dataFolder, "full/${currency.name}.json")
        f.parentFile.mkdirs()
        return f
    }

    @CompileStatic
    private File createCryptoCompareMediaFile(Currency currency) {
        File f = new File(dataFolder, "cryptoCompareMedia/${currency.ccImageUrl}")
        f.parentFile.mkdirs()
        return f
    }

    Map updateFiat() {
        println "Updating fiat..."
        new XmlParser().parse(fiatFile).CcyTbl[0].CcyNtry.each {
            String code = it.Ccy.text()
            if (code in currencyService.configFiatNamesSet) {
                String displayName = it.CcyNm.text()?.trim()
                int decimals = it.CcyMnrUnts.text().trim() as int
                Currency currency = Currency.findOrCreateWhere(name: code)
                currency.displayName = displayName
                currency.fiat = true
                currency.decimals = decimals
                currency.enabled = true
                if (!currency.id || currency.dirty) {
                    currency.lastUpdated = new Date()
                    currency.save(flush: true)
                }
            } else {
                Currency currency = Currency.findWhere(name: code)
                if (currency) {
                    currency.enabled = false
                    if (!currency.id || currency.dirty) {
                        currency.lastUpdated = new Date()
                        currency.save(flush: true)
                    }
                }
            }
        }
        println "Fiat updated ok"
    }


    void businessUpdateCrypto(boolean full, Collection<Currency> onlyThese = null) {
        try {
            fetchCrypto() // Baja coins.json solo
        } catch (e) {
            // Si falla no importa, se coge el de la vez anterior...
            e.printStackTrace()
        }

        // parse y actualiza coins.json. Las nuevas monedas se quedan como enabled
        updateCrypto(false, onlyThese)

        // Baja (si force es true) el full.json de cada moneda
        // Necesita antes el id de cryptocompare, por lo que hay que hacer siempre un updateCrypto(false)
        // Chequea y baja si no hay los medias (si forceMedia, baja si o si)
        checkMediaAndFetchFullInfo(full, false, onlyThese)

        // Actualiza la informacion del full
        updateCrypto(true, onlyThese)

        // Si es full, se baja de nuevo todos los pares de todas las monedas (o de las indicadas)
        // Si no es full, solo se baja los pares de las monedas que no tengan json en disco bajado (las nuevas)
        fetchPairs(full, onlyThese)
        // Actualiza pares (pone a enabled = false los que tengan 0 pares)
        updatePairs(false, onlyThese)

        currencyService.refreshCacheFromDb()

    }

    @CompileStatic
    void fetchCrypto() {
        println "Fetching ${coinsFile.name}"
        coinsFile.text = requestCryptoCompareCoins()
    }

    void updateCrypto(boolean full, Collection<Currency> onlyThese = null) {
        println "Updating cryptos..."
        Map coins = new JsonParser().parse(coinsFile.text)
        def set = coins.Data.keySet()
        set.collate(collate).eachWithIndex { list, collateIndx ->
            Currency.withNewTransaction {
                Currency.withNewSession {
                    list.eachWithIndex { k, indx ->
                        Map map = coins.Data[k]
                        if (onlyThese && !onlyThese.find { it.name == map.Name }) {
                            return
                        }
                        println "${(collateIndx * collate) + indx}/${set.size()} Updating crypto ${map.Name}"
                        updateCryptoCurrency(map, full, onlyThese)
                    }
                }
            }
        }
        int count = Currency.executeUpdate("update Currency c set enabled = false where c.fiat = false and c.name not in (${set.collect { "'${it}'"}.join(",")})")
        println "Disabled ${count} coins not found in ${coinsFile.name} anymore"
    }

    @CompileStatic
    void checkMediaAndFetchFullInfo(boolean forceFullInfo, boolean forceMedia, Collection<Currency> onlyThese = null) {
        BoundedPersistenceExecutor executor = persistenceContextService.createBoundedPersistenceExecutor()
        try {
            println "checkMediaAndFetchFullInfo START"
            Collection<Currency> currencies = onlyThese?:currencyService.findAllEnabledCryptoCurrencies()
            currencies.eachWithIndex { Currency currency, int indx ->
                executor.submit {
                    try {
                        println "${indx}/${currencies.size()} Fetching full info ${currency.name}"
                        fetchFullInfo(currency, forceFullInfo)
                    } catch (e) {
                        log.error("Error fetching full info ${currency.name}", e)
                    }
                }
                executor.submit {
                    try {
                        println "${indx}/${currencies.size()} Fetching media ${currency.name}"
                        fetchMedia(currency, forceMedia)
                    } catch (e) {
                        log.error("Error fetching media ${currency.name}", e)
                    }
                }
            }
        } finally {
            executor.destroy()
        }
        println "checkMediaAndFetchFullInfo DONE"
    }


    private Currency updateCryptoCurrency(Map v, boolean full, Collection<Currency> onlyThese = null) {
        Currency currency = Currency.findOrCreateWhere(name: v.Name)
        currency.displayName = v.CoinName?.trim()
        currency.ccImageUrl = v.ImageUrl?.trim()
        currency.ccUrl = v.Url?.trim()
        currency.ccId = v.Id?.trim()
        currency.proofType = v.ProofType?.trim()
        String totalCoinSupply = v.TotalCoinSupply?.trim()
        if (v == "N/A" || !totalCoinSupply) {
            currency.totalCoinSupply = 0
            currency.parsedTotalCoinSupply = null
        } else {
            try {
                totalCoinSupply = totalCoinSupply.replaceAll(",","").replaceAll(" ", "")
                if (totalCoinSupply.count(".") > 1) {
                    totalCoinSupply = totalCoinSupply.replaceAll(".","")
                }
                currency.totalCoinSupply = new Double(totalCoinSupply) as Long
                currency.parsedTotalCoinSupply = null
            } catch (e) {
                currency.parsedTotalCoinSupply = totalCoinSupply
            }
        }
        currency.algorithm = v.Algorithm?.trim()
        currency.decimals = 8
        currency.rank = v.SortOrder as int
        currency.fiat = false
        if (!currency.id) {
            currency.enabled = true
        }

        //            println v.ImageUrl
        //     "ImageUrl" -> "/media/19808/mrs.png"
        //     "SortOrder" -> "398"
        //     "TotalCoinSupply" -> "0"
        //     "Algorithm" -> "Scrypt"
        //     "Url" -> "/coins/sphr/overview"
        //     "Name" -> "SPHR"
        //     "ProofType" -> "PoW/PoS"
        //     "PreMinedValue" -> "N/A"
        //     "FullName" -> "Sphere Coin (SPHR)"
        //     "TotalCoinsFreeFloat" -> "N/A"
        //     "Id" -> "6111"
        //     "FullyPremined" -> "0"
        //     "CoinName" -> "Sphere Coin"

        Map fullInfo = full ? createFullInfoMapOrMarkCurrencyAsFailed(currency) : null

        if (fullInfo) {
            currency.twitter = fullInfo.General.Twitter
            currency.affiliateUrl = fullInfo.General.AffiliateUrl
            currency.startDate = Date.parse("dd/MM/yyyy HH:mm:ss", "${fullInfo.General.StartDate} 00:00:00")
        }

//        General.Technology
//        General.Features
//        General.Description


        File image = createCryptoCompareMediaFile(currency)
        currency.mediaOk = image.exists()

        if (!currency.id || currency.dirty) {
            currency.lastUpdated = new Date()
            currency.save(flush: true)
        }
        return currency
    }

    private Map createFullInfoMapOrMarkCurrencyAsFailed(Currency currency) {
        File full = createFullInfoFile(currency)
        if (!full.exists()) {
            setCurrencyError(currency, Currency.Status.not_found)
            return null
        }

        try {
            Map jsonParsed = new JsonParser().parse(full.text)
            if (jsonParsed.Response != "Success") {
                setCurrencyError(currency, Currency.Status.error, jsonParsed.Message)
                return null
            } else {
                currency.status = Currency.Status.ok
                currency.message = null
                return jsonParsed.Data
            }
        } catch (e) {
            setCurrencyError(currency, Currency.Status.error, e.message)
        }
        return null
    }


    @CompileStatic
    private File createPairFile(String currency) {
        File f = new File(dataFolder, "pairs/${currency}.json")
        f.parentFile.mkdirs()
        return f
    }


    void fetchPairs(boolean forceDownload, Collection<Currency> onlyThese = null) {
        BoundedPersistenceExecutor executor = persistenceContextService.createBoundedPersistenceExecutor()
        try {
            Collection<Currency> currencies = onlyThese?:Currency.findAll()
            currencies.eachWithIndex { Currency currency, int indx ->
                executor.submit {
                    File file = createPairFile(currency.name)
                    if (forceDownload || !file.exists()) {
                        log.info "${indx}/${currencies.size()} Fetching pairs ${currency.name}..."
                        file.text = requestCryptoComparePairs(currency.name)
                    }
                }
            }
        } finally {
            executor.destroy()
        }
        println "fetchPairs DONE"
    }

    void updatePairs(boolean forceAddUnknownCurrencies, Collection<Currency> onlyThese = null) {
        List<Currency> currencies = onlyThese?:Currency.findAll()
        currencies.eachWithIndex { Currency currency, int indx ->
            try {
                File file = createPairFile(currency.name)
                if (!file.exists()) {
                    // aqui se modifica una currency, pero no hace falta actualizar la cache pq
                    // se guarda solamente el mensaje de error :)
                    setCurrencyPairError(currency, Currency.Status.not_found, null)
                    return
                }
                Map jsonParsed = new JsonParser().parse(file.text)
                if (jsonParsed.Response != "Success") {
                    setCurrencyPairError(currency, Currency.Status.error, jsonParsed.Message)
                    return
                }
                Set toCurrencyNames = jsonParsed.Data*.toSymbol

                Currency.withNewTransaction {
                    Currency.withNewSession {
                        toCurrencyNames.each { String to ->
                            log.info "Checking new pair ${currency.name}-${to}..."
                            checkNewPairPrice(currency, to, forceAddUnknownCurrencies)
                        }
                    }
                }
                currency.pairsStatus = Currency.Status.ok
                currency.message = null
                updateCurrencyDataFromPairs(currency)
                currency.save(flush: true)
            } catch (e) {
                setCurrencyPairError(currency, Currency.Status.error, e.message)
                currency.save(flush: true)
            }
        }
    }

    private void setCurrencyPairError(Currency currency, Currency.Status status, String message = null) {
        currency.pairsStatus = status
        currency.message = "Pairs: ${message}"
        updateCurrencyDataFromPairs(currency)
        currency.save()
    }
    private void setCurrencyError(Currency currency, Currency.Status status, String message = null) {
        currency.status = status
        currency.message = "Info: ${message}"
        currency.save()
    }

    void updateCurrencyDataFromPairs(Currency currency) {
        currency.pairsFrom = PairPrice.countByFrom(currency)
        currency.pairsTo = PairPrice.countByTo(currency)
        currency.pairsWrong = PairPrice.createCriteria().get {
            or {
                eq("to", currency)
                eq("from", currency)
            }
            ne("status", PairPrice.Status.ok)
            projections {
                count("id")
            }
        }
    }

    @CompileStatic
    void checkNewPairPrice(Currency from, String _to, boolean forceAddUnknownCurrencies) {
        Currency to = currencyService.findCurrencyByName(_to)
        if (!to && !forceAddUnknownCurrencies) {
            return
        }
        if (!to) {
            to = new Currency(name: _to, displayName: "!", fiat: false).save(flush: true)
//            logWarn(SystemEvent.Section.checkingPair, "New unknonw currency created ${_to} when checking pair ${from.name}-${_to}")
        }
        PairPrice pairPrice = PairPrice.findOrCreateWhere(from: from, to: to)
        if (!pairPrice.id) {
            pairPrice.status = PairPrice.Status.empty
        }
        pairPrice.sfrom = from.name
        pairPrice.sto = _to
        pairPrice.save(flush: true)
    }



    @CompileStatic
    void fetchFullInfo(Currency currency, boolean force) {
        File file = createFullInfoFile(currency)
        if (force || !file.exists()) {
            file.text = requestCryptoCompareFullCoinInfo(currency.ccId)
        }
    }

    @CompileStatic
    void fetchMedia(Currency currency, boolean force) {
        if (currency.ccImageUrl) {
            File image = createCryptoCompareMediaFile(currency)
            if (force || !image.exists()) {
                image.bytes = new HttpRequest().get(currency.ccFullImageUrl()).send().bodyBytes()
            }
        }
    }

    String requestCryptoComparePairs(String coin) {
        return new HttpRequest().get("https://min-api.cryptocompare.com/data/top/pairs?fsym=${coin}&limit=2000").send().bodyText()
    }

    @CompileStatic
    String requestCryptoCompareFullCoinInfo(String id) {
        return new HttpRequest().get("https://www.cryptocompare.com/api/data/coinsnapshotfullbyid/?id=${id}").send().bodyText()
    }

    @CompileStatic
    String requestCryptoCompareCoins() {
        return new HttpRequest().get("https://www.cryptocompare.com/api/data/coinlist/").send().bodyText()
    }


}