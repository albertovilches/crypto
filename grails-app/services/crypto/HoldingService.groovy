package crypto

import grails.core.GrailsApplication
import groovy.transform.CompileStatic
import mamespin.User
import org.springframework.beans.factory.InitializingBean

import java.math.RoundingMode

class HoldingService implements InitializingBean {

    static transactional = false
    GrailsApplication grailsApplication
    CurrencyService currencyService
    PriceService priceService

    @Override
    void afterPropertiesSet() throws Exception {
    }

    List<Holding> listAllUserHoldings(User user, Currency currency = null, Boolean fiat = null) {
        listUserHoldings(user, false, currency, fiat)

    }

    Holding listLastUserHolding(User user, Currency currency) {
        List<Holding> list = listUserHoldings(user, true, currency)
        if (list) {
            return list[0]
        }
        return null
    }

    List<Holding> listLastUserHoldings(User user, Boolean fiat = null) {
        listUserHoldings(user, true, null, fiat)
    }

    List<Holding> listMenuHoldings(User user, int max = 8) {
        return Holding.createCriteria().list(max: max) {
            eq("user", user)
            eq("last", true)
            eq("currencyFiat", false)

            order("valueFiat", "desc")
            projections {
                property("currency")
            }
        }
    }
    List<Holding> listUserHoldings(User user, boolean lastOnly, Currency currency = null, Boolean fiat = null) {
        return Holding.createCriteria().list {
            eq("user", user)
            if (lastOnly) {
                eq("last", true)
            }
            if (currency) {
                eq("currency", currency)
            }
            if (fiat != null) {
                eq("currencyFiat", fiat)
            }

            order("holdDate", "asc")
        }
    }

    @CompileStatic
    List<Map> getHoldingChartForCurrency(User user, Currency currency, Currency toCurrency = null) {
        toCurrency = toCurrency ?: user.defaultCurrency
        List<Holding> holdings = listAllUserHoldings(user, currency)
        return holdingListToChartList(holdings, currency, toCurrency)
    }

    @CompileStatic
    List getHoldingChartCryptosAggregate(User user, Boolean fiat = null, Currency toCurrency = null) {
        toCurrency = toCurrency ?: user.defaultCurrency
        List<Holding> holdings = listAllUserHoldings(user, null, fiat)
        return aggregateHoldingListToChartList(holdings, toCurrency)
    }

    List<Map> holdingListToChartList(List<Holding> history, Currency currency, Currency toCurrency) {
        List<Map> historyJson = history.collect { Holding holding ->
            [date     : holding.holdDate,
             amount   : holding.amount,
             costFiat : holding.costFiat.setScale(toCurrency.decimals, RoundingMode.HALF_UP),
             valueFiat: holding.valueFiat.setScale(toCurrency.decimals, RoundingMode.HALF_UP)
            ]
        }

        // Rellenamos hasta el dia de hoy copiando el ultimo dato
        Map last = historyJson.last()
        Date now = new Date().clearTime()
        if (last.date.clearTime() < now) {
            historyJson << [date    : now,
                            amount  : last.amount,
                            costFiat: last.costFiat]
        }

        // Rellenamos datos intermedios
        List<Map> chartList = []
        def previous
        historyJson.eachWithIndex { Map m, int pos ->
            if (pos > 0) {
                Date middleDate = previous.date + 1
                while (middleDate < m.date) {
                    chartList << [date    : middleDate,
                                  amount  : previous.amount,
                                  costFiat: previous.costFiat]
                    middleDate++
                }
            }
            previous = m
            chartList << m
        }

        // Obtenemos todas las cotizaciones que vamos a necesitar para crear la serie
        PriceService.BulkHistoricPriceConverter bulk = priceService.createBulkHistoricPriceConverter(currency, toCurrency, chartList.first().date, chartList.last().date)
        chartList.each {
//            it.valueFiat = it.valueFiat != null ? it.valueFiat : convertToHistoricPrice(it.amount, currency, toCurrency, it.date).setScale(2, RoundingMode.HALF_UP)
            it.valueFiat = it.valueFiat != null ? it.valueFiat : bulk.convertPrice(it.amount, it.date).setScale(toCurrency.decimals, RoundingMode.HALF_UP)
            it.dateString = it.date.format("yyyy-MM-dd")
//            it.remove("amount")
        }

        return chartList
    }

    List aggregateHoldingListToChartList(List<Holding> holdings, Currency toCurrency = null) {
        Map<Currency, List<Holding>> group = holdings.groupBy { it.fastCurrency }

        List<Map> allCryptoMerged = []
        List<Map> allFiatMerged = []
        Date minDate = new Date().clearTime()
        Date maxDate = minDate
        
        group.each { Currency currency, List<Holding> holdingList ->
            def chartList = holdingListToChartList(holdingList, currency, toCurrency)
            if (chartList) {
                Date other = chartList.first().date
                minDate = minDate.time <= other.time ? minDate : new Date(other.time)
                if (currency.fiat)
                    allFiatMerged.addAll(chartList)
                else
                    allCryptoMerged.addAll(chartList)
            }
        }
        Map cryptoByDate = allCryptoMerged.groupBy { it.date }
        Map fiatByDate = allFiatMerged.groupBy { it.date }

        List aggregatedList = []
        while (minDate <= maxDate) {
            BigDecimal fiatCostFiat = fiatByDate[minDate]?.sum { it.costFiat } ?: 0
            BigDecimal fiatValueFiat = fiatByDate[minDate]?.sum { it.valueFiat } ?: 0
            BigDecimal cryptoCostFiat = cryptoByDate[minDate]?.sum { it.costFiat } ?: 0
            BigDecimal cryptoValueFiat = cryptoByDate[minDate]?.sum { it.valueFiat } ?: 0
            aggregatedList << [date           : minDate,
                               dateString     : minDate.format("yyyy-MM-dd"),
                               cryptoCostFiat : cryptoCostFiat,
                               fiatCostFiat   : fiatCostFiat,
                               allCostFiat    : fiatCostFiat + cryptoCostFiat,
                               cryptoValueFiat: cryptoValueFiat,
                               fiatValueFiat  : fiatValueFiat,
                               allValueFiat   : cryptoValueFiat + fiatValueFiat
            ]
            minDate++
        }
        return aggregatedList
    }

}