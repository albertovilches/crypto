package crypto

import grails.core.GrailsApplication
import mamespin.CacheService
import org.springframework.beans.factory.InitializingBean

import java.text.SimpleDateFormat

class PriceService implements InitializingBean {

    static transactional = false
    GrailsApplication grailsApplication
    CurrencyService currencyService
    CacheService cacheService

    @Override
    void afterPropertiesSet() throws Exception {
    }


    BigDecimal convertToHistoricPrice(BigDecimal amount, Currency from, Currency to, Date date) {
        BigDecimal result = from.id == to.id ? amount : amount * findHistoricPrice(from, to, date)
        return result

    }

    BigDecimal findHistoricPrice(Currency from, Currency to, Date date, boolean convertUsingBTCETH = true, boolean convertUSD = true) {
        date = (Date) date.clone()
        date.clearTime()
        println "-------------> ${from} in ${to} in ${date.format("dd/MMM/yyyy")}"
        Price p = Price.findByFromAndToAndDateLessThanEquals(from, to, date, [max: 1, sort: "date", order: "desc"])
        if (p) {
            if (p.date != date) {
                // TODO: Enviar un warning, faltan datos!
                println "oh oh"
            }
            return p.price
        }

        if (!convertUsingBTCETH) {
            return 0
        }

        // Intentar a traves de BTC
        Price p2 = Price.findByFromAndToAndDate(from, currencyService.BTC, date)
        Price p3 = !p2 ? null : Price.findByFromAndToAndDate(currencyService.BTC, to, date)
        if (p3) return p2.price * p3.price

        // Intentar a traves de ETH
        p2 = Price.findByFromAndToAndDate(from, currencyService.ETH, date)
        p3 = !p2 ? null : Price.findByFromAndToAndDate(currencyService.ETH, to, date)
        if (p3) return p2.price * p3.price

        if (!convertUSD) {
            return 0
        }

        // Intentar a traves de USD
        p2 = Price.findByFromAndToAndDate(from, currencyService.USD, date)
        BigDecimal usdToOtherFiat = !p2 ? null : usdToFiat(to, date)

        if (usdToOtherFiat) {
            return p2.price * usdToOtherFiat
        } else {
            return 0
        }
    }

    /**
     * usdToFiat("EUR") -> 0.84
     */
    BigDecimal usdToFiat(Currency currency, Date date = new Date()) {
        date = (Date) date.clone()
        date.clearTime()
        Price btcOther = Price.findByFromAndToAndDateLessThanEquals(currencyService.BTC, currency, date, [max: 1, sort: "date", order: "desc"])
        // EUR = 3600
        Price btcUsd = Price.findByFromAndToAndDateLessThanEquals(currencyService.BTC, currencyService.USD, date, [max: 1, sort: "date", order: "desc"])
        // USD = 4200
        return btcUsd && btcOther ? btcOther.price / btcUsd.price : null
        // USD-EUR = 0.85 (EUR / USD ==> 3600 / 4200 ==> 0.85)
    }

    /**
     * usdToFiat("EUR") -> 0.84
     */
    BigDecimal cnyToFiat(Currency currency, Date date = new Date()) {
        date = (Date) date.clone()
        date.clearTime()
        Price btcToFiat = Price.findByFromAndToAndDateLessThanEquals(currencyService.BTC, currency, date, [max: 1, sort: "date", order: "desc"])
        Price btcToCny = Price.findByFromAndToAndDateLessThanEquals(currencyService.BTC, currencyService.CNY, date, [max: 1, sort: "date", order: "desc"])
        return btcToCny && btcToFiat ? btcToFiat.price / btcToCny.price : null
    }

    class BulkHistoricPriceConverter {
        Map<Long, BigDecimal> prices
        Integer firstDateIndex
        Currency from
        Currency to
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd")

        int dateToIndex(Date date) {
            sdf.format(date) as int
        }

        BulkHistoricPriceConverter create(Currency from, Currency to, Date start, Date end) {
            this.from = from
            this.to = to
            if (from.name == to.name) {
                return this
            }

            List data = loadPrices(from, to, start, end)
            if (data) {
                prices = createPriceMapByDateIndex(data)
            } else {
                Map dataToBtc = createPriceMapByDateIndex(loadPrices(from, currencyService.BTC, start, end))
                if (dataToBtc) {
                    Map dataFromBtc = createPriceMapByDateIndex(loadPrices(currencyService.BTC, to, start, end))
                    dataToBtc.each { entry ->
                        entry.value *= dataFromBtc[entry.key] ?: 0
                    }
                    prices = dataToBtc
                } else {
                    Map dataToEth = createPriceMapByDateIndex(loadPrices(from, currencyService.ETH, start, end))
                    if (dataToEth) {
                        Map dataFromEth = createPriceMapByDateIndex(loadPrices(currencyService.ETH, to, start, end))
                        dataToEth.each { entry ->
                            entry.value *= dataFromEth[entry.key] ?: 0
                        }
                        prices = dataToEth
                    }
                }

            }
            firstDateIndex = prices?.keySet()?.sort()?.first() ?: 0
            return this
        }

        private List loadPrices(Currency from, Currency to, Date start, Date end) {
            return Price.createCriteria().list {
                projections {
                    property("date")
                    property("price")
                }
                eq("from", from)
                eq("to", to)
                between("date", start, end)
            }
        }

        private Map createPriceMapByDateIndex(List data) {
            return data.inject([:]) { Map prices, row ->
                prices[dateToIndex(row[0])] = row[1]
                return prices
            }
        }

        BigDecimal convertPrice(BigDecimal amount, Date date) {
            BigDecimal price = findPriceOn(date)
            if (price == null) {
                // NO HABIA NINGUNO??
                println "BUF"
            }
            return amount * price
        }

        BigDecimal findPriceOn(Date date) {
            if (from.name == to.name) return 1
            int index = dateToIndex(date)
            BigDecimal price = prices[index]
            if (price != null) return price

            // TODO: Enviar un warning
//            println "ohoh"

            Calendar calendar = (Calendar) Calendar.getInstance().clone();
            calendar.setTime(date);

            while (price == null && index > firstDateIndex) {
                // Retrocede, dia a dia, hasta encontrar un precio valido (hasta el primer dia claro)
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                index = dateToIndex(calendar.getTime())
                price = prices[index]
            }
            if (price == null) {
                // TODO: ERROR!!!!
                return 0
            }
            return price
        }
    }


    BulkHistoricPriceConverter createBulkHistoricPriceConverter(Currency from, Currency to, Date start, Date end) {
        return new BulkHistoricPriceConverter().create(from, to, start, end)
    }

    List<BigDecimal> listLastPricesWithConversion(Currency from, Currency to, int days = 30) {
        cacheService.cache("lastPrices", "${from.name}-${to.name}-${days}", 60*60*4 /* 4 horas */) {
            List<BigDecimal> prices = listLastPrices(from, to, days)
            if (prices) return prices

            List<BigDecimal> prices2 = listLastPrices(from, currencyService.BTC, days)
            List<BigDecimal> prices3 = listLastPrices(currencyService.BTC, to, days)

            // Fill by zeros by the left
            prices2 = [0] * (days - prices2.size()) + prices2
            prices3 = [0] * (days - prices2.size()) + prices2


            prices2.eachWithIndex { def entry, int i ->
                prices3[i] = prices3[i] * entry
            }
            return prices3
        }
    }

    List<BigDecimal> listLastPrices(Currency from, Currency to, int days) {
        return Price.createCriteria().list() {
            eq("from", from)
            eq("to", to)
            maxResults(days)
            order("date", "desc")
            projections {
                property("price")
            }
        }.reverse()
    }


}