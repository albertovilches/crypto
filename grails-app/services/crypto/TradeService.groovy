package crypto

import com.xlson.groovycsv.CsvParser
import com.xlson.groovycsv.PropertyMapper
import grails.core.GrailsApplication
import groovy.transform.CompileStatic
import mamespin.User
import mamespin.command.TradeCommand
import mamespin.personal.TradeController
import org.springframework.beans.factory.InitializingBean

class TradeService implements InitializingBean {

    static transactional = false
    GrailsApplication grailsApplication
    CurrencyService currencyService
    PriceService priceService

    @Override
    void afterPropertiesSet() throws Exception {
    }

    long countUserTrades(User user, Currency currency = null) {
        Trade.createCriteria().get() {
            eq("user", user)
            if (currency) {
                or {
                    eq("inCurrency", currency)
                    eq("outCurrency", currency)
                }
            }
            projections {
                count("id")
            }
        }
    }
    List<Trade> findUserTrades(User user, Integer offset, Integer limit, String sort, String order, String search) {
        sort = sort ?: "tradeDate"
        order = order ?: "desc"
        limit = Math.min(limit ?: 50, 200)

        List parts = (search ?: "").split(" ").findAll()*.toUpperCase() // remove empty estring

        List currencies = parts.collect { String part -> currencyService.getCurrency(part) }.findAll( /* remove nulls */ )

        def result = Trade.createCriteria().list(["offset": offset, "max": limit]) {
            eq("user", user)

            if (currencies) {
                or {
                    currencies.each { Currency currency ->
                        eq("inCurrency", currency)
                        eq("outCurrency", currency)
                    }
                }
            }
            delegate.order(sort, order)
        }
        result
    }

    @CompileStatic
    Trade addNewTransaction(User user, TradeCommand tradeCommand) {
        if (!user || !tradeCommand || !tradeCommand.type || !tradeCommand.tradeDate) return null
        Trade trade = new Trade(user: user)
        updateTransaction(tradeCommand, trade)
        if (!validateTrade(trade)) return null
        trade.save(flush: true)
    }

    @CompileStatic
    boolean validateTrade(Trade trade) {
        if (!trade.type) return false
        if (trade.type.inc || trade.type == Trade.Type.trade) {
            if (!trade.inCurrency || !trade.inAmount) return false
        }
        if (trade.type.out || trade.type == Trade.Type.trade) {
            if (!trade.outCurrency || !trade.outAmount) return false
        }
        return true
    }

    @CompileStatic
    void updateTransaction(TradeCommand tradeCommand, Trade trade) {
        if (!tradeCommand || !tradeCommand.type || !tradeCommand.tradeDate) return

        trade.tradeDate = tradeCommand.tradeDate
        trade.type = tradeCommand.type
//        trade.comment = tradeCommand.comment
//        trade.exchange = tradeCommand.exchange

        if (tradeCommand.type.inc || tradeCommand.type == Trade.Type.trade) {
            trade.inCurrency = currencyService.getCurrency(tradeCommand.inCurrency)
            trade.inAmount = tradeCommand.inAmount
        }
        if (tradeCommand.type.out || tradeCommand.type == Trade.Type.trade) {
            trade.outCurrency = currencyService.getCurrency(tradeCommand.outCurrency)
            trade.outAmount = tradeCommand.outAmount
        }
    }

    @CompileStatic
    static class UserData {
        List<Trade> trades
        List<Holding> holdings
        User user
    }

    int deleteTrades(User user, List idList) {
        int deleted = 0
        idList = idList?.collect { it instanceof Number ? it : Integer.parseInt(it as String) }.findAll()
        if (idList) {
            deleted = Trade.executeUpdate("delete from Trade t where t.user.id = ${user.id} and t.id in (${idList.join(",")})")
        }
        return deleted
    }

    List<Trade> listUserTrades(User user) {
//        return Trade.findAllByUserAndHidden(user, false).sort { it.tradeDate }
        // TODO PAGINAR!!!!!!!!!!!!!!!!
        return Trade.createCriteria().list() {
            eq("user", user)
            eq("hidden", false)
            order("tradeDate", "asc")
        }
    }

    class RefreshProcess {

        Map<String, List<Holding>> holdingMap
        List<Holding> holdingList
        List<Trade> trades
        User user
        Currency defaultCurrency

        @CompileStatic
        Holding findHolding(Currency currency, Date date) {
            date = (Date) date.clone()
            date.clearTime()
            Holding latestHoling = holdingMap[currency.name] ? holdingMap[currency.name].last() : null
            if (latestHoling?.holdDate == date) {
                return latestHoling
            }
            Holding newHolding = new Holding(user: user, currency: currency, holdDate: date, last: true, unitPriceFiat: currency.fiat ? 1 : 0)
            newHolding.validate() // para que se ejecuten los beforeValidate
            if (latestHoling) {
                newHolding.totalIn = latestHoling.totalIn
                newHolding.totalOut = latestHoling.totalOut
                newHolding.amount = latestHoling.amount
                newHolding.costFiat = latestHoling.costFiat
                newHolding.valueFiat = latestHoling.valueFiat
                newHolding.unitPriceFiat = latestHoling.unitPriceFiat
                newHolding.profit = latestHoling.profit

                latestHoling.last = false
            }
            holdingMap[currency.name] << newHolding
            holdingList << newHolding
            return newHolding
        }

        UserData execute(User user, int max = Integer.MAX_VALUE) {
            this.user = user
            this.defaultCurrency = user.defaultCurrency ?: currencyService.USD
            holdingMap = [:].withDefault { k -> [] }
            holdingList = []

            // TODO: paginar!!
            trades = listUserTrades(user) // mas viejo a mas nuevo

            if (max < trades.size()) {
                trades = trades.subList(0, max)
            }
            trades.eachWithIndex { Trade trade, int idx ->
                updateTrade(trade)
            }
            Holding.withTransaction {
                Holding.executeUpdate("delete Holding where user = :user", [user: user])
                holdingList.each { Holding holding ->
                    if (holding.amount > 0) {
                        holding.valueFiat = priceService.convertToHistoricPrice(holding.amount, holding.fastCurrency, defaultCurrency, holding.holdDate)
                        holding.save(flush: true)
                    } else {
                        holding.discard()
                    }
                }
            }
            List<Holding> holdingState = holdingMap.inject((List<Holding>) []) { List result, String key, List<Holding> all ->
                result << all.last()
                return result
            }
            verifyAmounts(trades, holdingState)
            return new UserData(user: user, trades: trades, holdings: holdingState.sort { it.currencyName })
        }

        @CompileStatic
        private void updateTrade(Trade trade) {
            if (trade.type == Trade.Type.trade) {
                trade.unitPrice = trade.outAmount / trade.inAmount
                // Ejemplo: compra de 2 ETH por 400 EUR

                // Buscamos el mapa acumulado de EUR
                Holding payingCoin = findHolding(trade.fastOutCurrency, trade.tradeDate)

                // Calculoamos el coste real de lo que hemos comprado
                BigDecimal costFiat = trade.outAmount * payingCoin.unitPriceFiat

                // Calculamos lo que valia en esa fecha lo lo que hemos comprado para calcular el beneficio
                BigDecimal valueFiat = trade.outCurrencyId == defaultCurrency.id ? costFiat :
                        // TODO: guardar el precio de la inCurrency contra defaultCurrency en el trade en el momento de la insercion para evitar buscar el precio
                        priceService.convertToHistoricPrice(trade.inAmount, trade.fastInCurrency, defaultCurrency, trade.tradeDate)
                BigDecimal profit = valueFiat - costFiat

                // Si lo ponemos aqui entonces todas las compras que no se han vendido todavia dan perdidas
                BigDecimal inProfit = 0

                // lo normal es que este aqui, ya que se pone en la venta... las compras sin vender tienen 0 profit (ni gana ni pierde...todavia)
                BigDecimal outProfit = profit

                // Y le quitamos la cantidad al holding (quitar dinero no cambia el precio medio pero si puede
                // añadir un beneficio :)
                removeAmount(payingCoin, trade.outAmount, outProfit)

                // Buscamos los ETH
                // Le añadimos la cantidad que hemos comprado
                // Y el importe en euro que hemos pagado
                // (Añadir dinero SI que cambia el precio medio)
                Holding buyingCoin = findHolding(trade.fastInCurrency, trade.tradeDate)

                trade.inOldAvgUnitPriceFiat = buyingCoin.unitPriceFiat
                addAmount(buyingCoin, trade.inAmount, valueFiat, inProfit)

                // El nuevo precio medio fiat para XRP se guarda en el registro a modo de historico
                trade.inNewAvgUnitPriceFiat = buyingCoin.unitPriceFiat
                trade.outCostFiat = costFiat
                trade.inValueFiat = valueFiat

            } else if (trade.type.out) {
                // Ejemplo: nos gastamos unos ETH en comer

                // Buscamos el mapa acumulado de ETH en euros
                // Y le quitamos la cantidad al holding (quitar dinero no cambia el precio medio :)
                Holding payingCoin = findHolding(trade.fastOutCurrency, trade.tradeDate)
                removeAmount(payingCoin, trade.outAmount)


            } else if (trade.type.inc) {
                // Me pagan 0.05 BTC por un disco
                // Mino 1 DASH
                // Meto EUR en Coinbase para gastarlos

                // Obtenemos el mapa de acumulado de BTC en euros (puede estar vacio)
                Holding buyingCoin = findHolding(trade.fastInCurrency, trade.tradeDate)

                // TODO: guardar el precio de la inCurrency contra defaultCurrency en el trade en el momento de la insercion para evitar buscar el precio
                BigDecimal valueFiat = priceService.convertToHistoricPrice(trade.inAmount, trade.fastInCurrency, defaultCurrency, trade.tradeDate)

                trade.inOldAvgUnitPriceFiat = buyingCoin.unitPriceFiat
                addAmount(buyingCoin, trade.inAmount, valueFiat)
//                trade.costFiat = costInFiat

                // El nuevo coste medio fiat para XRP se guarda en el registro
                trade.inNewAvgUnitPriceFiat = buyingCoin.unitPriceFiat

            }
            trade.save(flush: true)
        }

        @CompileStatic
        void addAmount(Holding holding, BigDecimal amount, BigDecimal costFiat, BigDecimal profit = BigDecimal.ZERO) {
            holding.totalIn += amount
            holding.amount += amount
            holding.costFiat += costFiat
            holding.unitPriceFiat = holding.currencyFiat ? new BigDecimal(1) : holding.costFiat / holding.amount
            holding.profit += profit
        }

        @CompileStatic
        void removeAmount(Holding holding, BigDecimal amount, BigDecimal profit = BigDecimal.ZERO) {
            holding.totalOut += amount
            holding.amount -= amount
            holding.costFiat -= (amount * (holding.currencyFiat ? new BigDecimal(1) : holding.unitPriceFiat))
            holding.profit += profit

            // El precio medio del holding no debe cambiar!!!!!!!!!!!!
            if (holding.amount > 0) {
                BigDecimal newUnitPriceFiatShouldBeTheSame = holding.costFiat / holding.amount
                if (newUnitPriceFiatShouldBeTheSame.setScale(2, java.math.RoundingMode.CEILING).toString() != holding.unitPriceFiat.setScale(2, java.math.RoundingMode.CEILING).toString()) {
                    println "ops"
                }
            }
        }

        private void verifyAmounts(List<Trade> trades, Collection<Holding> holdings) {
            Map inTotalByCurrency = [:]
            Map outTotalByCurrency = [:]
            Map ins = trades.findAll { it.inAmount }.groupBy { it.inCurrencyId }
            Map outs = trades.findAll { it.outAmount }.groupBy { it.outCurrencyId }
            Set currencies = ins.keySet() + outs.keySet()
            currencies.each {
                inTotalByCurrency[it] = (ins[it]?.sum { it.inAmount } ?: 0)
                outTotalByCurrency[it] = (outs[it]?.sum { it.outAmount } ?: 0)
            }
            holdings.each { Holding holding ->
                if (holding.amount != inTotalByCurrency[holding.currencyId] - outTotalByCurrency[holding.currencyId]) {
                    println "ops"
                }
                if (holding.totalIn != inTotalByCurrency[holding.currencyId]) {
                    println "ops"
                }
                if (holding.totalOut != outTotalByCurrency[holding.currencyId]) {
                    println "ops"
                }
            }
        }

    }


    UserData refresh(User user, int max = Integer.MAX_VALUE) {
        return new RefreshProcess().execute(user, max)
    }

    void importCoinTrackingCsv(boolean deleteAll, User user, Reader reader) {
        Trade.withNewTransaction {
            if (deleteAll) {
                Trade.executeUpdate("delete Trade where user = :user", [user: user])
            }
            def csv = CsvParser.parseCsv(reader)
            csv.each { PropertyMapper row ->

                Trade trade = new Trade(user: user)
                if (row[0] in ["Operación", "Trade"]) {
                    trade.type = Trade.Type.trade
                } else if (row[0] in ["Retirada", "Withdrawal"]) {
                    trade.type = Trade.Type.withdraw
                } else if (row[0] in ["Gasto", "Spend"]) {
                    trade.type = Trade.Type.expend
                } else if (row[0] in ["Gift/Tip"]) {
                    trade.type = Trade.Type.gift
                } else if (row[0] in ["Ingreso", "Income"]) {
                    trade.type = Trade.Type.income
                } else if (row[0] in ["Depósito", "Deposit"]) {
                    trade.type = Trade.Type.deposit
                } else if (row[0] in ["Lost"]) {
                    trade.type = Trade.Type.lost
                } else {
                    throw new RuntimeException("Tipo de operacion desconocida: ${row[0]}")
                }

                if (trade.type.inc || trade.type == Trade.Type.trade) {
                    trade.inAmount = new BigDecimal(row[1])
                    trade.inCurrency = currencyService.getCurrency(row[2])
                    if (!trade.inCurrency) {
                        throw new RuntimeException("Currency ${row[2]} not found")
                    }
                }

                if (trade.type.out || trade.type == Trade.Type.trade) {
                    trade.outAmount = new BigDecimal(row[3])
                    trade.outCurrency = currencyService.getCurrency(row[4])
                    if (!trade.outCurrency) {
                        throw new RuntimeException("Currency ${row[4]} not found")
                    }
                }

//            if (row[6] && row[5]) {
//                trade.feeTotal = new BigDecimal(row[5])
//                trade.feeCurrency = row[6]
//            }
                trade.exchange = row[7]
                // group = row[8]
                trade.comment = row[9]
                trade.tradeDate = Date.parse("yyyy-MM-dd HH:mm:ss", row[10])

                trade.save(flush: true)

            }
        }
    }
}