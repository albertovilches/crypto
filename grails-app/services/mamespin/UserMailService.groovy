package mamespin

import grails.core.GrailsApplication
import org.grails.plugins.web.taglib.ApplicationTagLib
import org.springframework.beans.factory.InitializingBean

class UserMailService implements InitializingBean {

    def asyncMailService
    GrailsApplication grailsApplication
    def mailService
    SignService signService

    String from
    String subjectRegister
    String subjectRecoverPwd

    CacheService cacheService

    static transactional = false

//    AsynchronousMailService asyncMailService

    @Override
    void afterPropertiesSet() throws Exception {
        from = grailsApplication.config.app.mail.from
        subjectRegister = grailsApplication.config.app.mail.subjectRegister
        subjectRecoverPwd = grailsApplication.config.app.mail.subjectRecoverPwd
    }

    def sendRecoverPassword(User user) {
//        https://es.sendinblue.com
        String body = cacheService.read("mail/recoverpwd.html")
        body = body.replaceAll(/\{minutes\}/, signService.MINUTES_VALID_TOKEN_RECOVER_PWD.toString())
        body = body.replaceAll(/\{link\}/, signService.createNewPasswordUrl(user))
        body = body.replaceAll(/\{email\}/, user.email)
        mailService.sendMail {
            to user.email
            from from
            subject subjectRecoverPwd
            html body
        }
    }

    def sendRegisterMail(User user) {
//        https://es.sendinblue.com
        String body = cacheService.read("mail/register.html")
        body = body.replaceAll(/\{minutes\}/, signService.MINUTES_VALID_TOKEN_RECOVER_PWD.toString())
        body = body.replaceAll(/\{link\}/, signService.createNewPasswordUrl(user))
        body = body.replaceAll(/\{email\}/, user.email)
        mailService.sendMail {
            to user.email
            from from
            subject subjectRegister
            html body
        }
/*

        asyncMailService.sendMail {
            // Mail parameters
            to 'vilches@gmail.com'
            subject 'Test';
            html '<body><u>Test</u></body>';
//            attachBytes 'test.txt', 'text/plain', byteBuffer;

            // Additional asynchronous parameters (optional)
            beginDate new Date(System.currentTimeMillis()+60000)    // Starts after one minute, default current date
            endDate new Date(System.currentTimeMillis()+3600000)   // Must be sent in one hour, default infinity
            maxAttemptsCount 3;   // Max 3 attempts to send, default 1
            attemptInterval 300000;    // Minimum five minutes between attempts, default 300000 ms
            delete true;    // Marks the message for deleting after sent
            immediate true;    // Run the send job after the message was created
            priority 10;   // If priority is greater then message will be sent faster
        }

*/
    }
}
