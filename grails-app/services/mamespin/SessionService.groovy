/*
 * @author Alberto Vilches
 * @date 18/12/2016
 */
package mamespin

import grails.core.GrailsApplication
import grails.plugin.cookie.CookieService
import grails.web.api.ServletAttributes
import org.springframework.beans.factory.InitializingBean

class SessionService implements ServletAttributes, InitializingBean {

    String COOKIE_UUID
    int COOKIE_EXPIRE_SECONDS
    CookieService cookieService
    GrailsApplication grailsApplication

    static transactional = false

    @Override
    void afterPropertiesSet() throws Exception {
        COOKIE_EXPIRE_SECONDS = (60*60*24 /*1 day*/) * grailsApplication.config.app.cookie.rememberMe.days
        COOKIE_UUID = grailsApplication.config.app.cookie.rememberMe.name
    }

    void logout(boolean deleteCookies = true) {
        session.invalidate()
        if (deleteCookies) {
            cookieService.deleteCookie(COOKIE_UUID, "/")
        }
    }

    String getUUIDFromCookie() {
        return cookieService.getCookie(COOKIE_UUID)
    }

    User getUser() {
        Long userId = session.currentUserId
        if (!userId) return null

        User user = request.currentUser
        if (userId == user?.id) {
            return user
        }
        request.currentUser = User.get(userId)
    }

    void signedIn(User user, boolean rememberMe = true) {
        session.currentUserId = user.id
        if (user.admin) {
            session.admin = true
        }
        request.currentUser = user
        if (rememberMe) {
            cookieService.setCookie(COOKIE_UUID, user.uuid, COOKIE_EXPIRE_SECONDS, "/") // 30 dias
        }
    }
}