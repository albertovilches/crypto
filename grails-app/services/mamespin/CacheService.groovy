package mamespin

import grails.core.GrailsApplication
import grails.plugin.cache.GrailsValueWrapper
import groovy.transform.CompileStatic
import org.grails.plugin.cache.GrailsCacheManager
import org.springframework.beans.factory.InitializingBean
import org.springframework.cache.Cache

import java.util.concurrent.ConcurrentHashMap

class CacheService implements InitializingBean {

    static transactional = false
    GrailsApplication grailsApplication
    GrailsCacheManager grailsCacheManager

    ConcurrentHashMap blockCache
    File htmlRoot

    @Override
    void afterPropertiesSet() throws Exception {
        htmlRoot = new File(grailsApplication.config.app.html.root)
        blockCache = new ConcurrentHashMap()
    }

    @CompileStatic
    Object cache(String cacheName = "main", String key, int ttl = 0, Closure closure) {
        Cache cache = grailsCacheManager.getCache(cacheName)
        boolean expired = false

        if (ttl > 0) {
            expired = honorTTL(key, ttl)
        }

        def content = cache.get(key)
        if (content == null || expired) {
            content = closure.call()
            cache.put(key, content)
        } else {
            content = content.get()
        }
        return content
    }

    @CompileStatic
    protected Boolean honorTTL(String key, int secondsTtt) {
   		Cache cache = grailsCacheManager.getCache("ttlCache")
   		String ttlKey = key + ":ttl"
   		Long ttlInMilliseconds = secondsTtt * 1000
   		Long currentTime = System.currentTimeMillis()
   		Boolean expired
   		Cache.ValueWrapper valueInCache
   		Long cacheInsertionTime

   		try {
   			valueInCache = cache.get(ttlKey)
   			cacheInsertionTime = valueInCache ? valueInCache.get() as Long : 0L
   			expired = valueInCache && ((currentTime - cacheInsertionTime) > ttlInMilliseconds)
   		} catch (Exception e) {
   			cache.put(ttlKey, currentTime)
   			return true // we overwrote the cache key
   		}
   		if (expired || !valueInCache) {
   			cache.put(ttlKey, currentTime)
   		}
   		return expired
   	}




    String read(String filename, boolean refresh = false) {
        return block("file:${filename}", { -> loadFromDisk(filename) }, refresh)
    }

    def block(String key, Closure body, boolean refresh = false) {
        def cached = refresh ? null : blockCache[key]
        if (cached == null) {
            blockCache[key] = cached = body()
        }
        return cached
    }

    private String loadFromDisk(String filename) {
        File f = new File(htmlRoot, filename)
        if (!f.exists() || !f.file) {
            log.info("Reading ${f.absolutePath} error! not found")
            return "$filename NOT FOUND"
        }
        log.info("Reading ${f.absolutePath} ok")
        return f.getText("UTF8")
    }

    void clear() {
        blockCache.clear()
    }
}
