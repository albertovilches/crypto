/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin

import grails.transaction.Transactional
import groovy.time.TimeCategory
import org.apache.commons.validator.routines.EmailValidator
import org.grails.plugins.web.taglib.ApplicationTagLib
import org.springframework.beans.factory.InitializingBean

import java.security.SecureRandom

class SignService implements ControllerTools, InitializingBean {

    ApplicationTagLib applicationTagLib
    public Integer MINUTES_VALID_TOKEN_RECOVER_PWD = 60
    UserMailService userMailService
    static SecureRandom random = new SecureRandom()

    static transactional = false

    @Override
    void afterPropertiesSet() throws Exception {
        applicationTagLib = grailsApplication.mainContext.getBean("org.grails.plugins.web.taglib.ApplicationTagLib")
        MINUTES_VALID_TOKEN_RECOVER_PWD = grailsApplication.config.app.token.recoverPwd.minutes
    }

    User signIn(String ip, String usernameOrMail, String pwd, boolean rememberMe) {
        if (!usernameOrMail || !pwd) {
            UserAction.addSigninWrong(ip)
            return null
        }
        User user = findUserByUsernameOrEmail(usernameOrMail)
        if (!user) {
            UserAction.addSigninWrong(ip)
            return null
        } else if (!user.checkPwd(pwd)) {
            UserAction.addSigninWrong(ip, user)
            return null
        }
        UserAction.addSigninOk(ip, user)
        user.lastLogin = new Date()
        user.save(flush: true)
        sessionService.signedIn(user, rememberMe)
        return user
    }

    @Transactional
    User register(String ip, String email) {
        email = email?.trim()?.toLowerCase()
        if (!EmailValidator.instance.isValid(email)) return null
        User user = User.findByEmail(email)
        if (!user) {
            user = new User(email: email, username: "#${email.split("@")[0]}_${System.currentTimeSeconds()}")
            user.save(flush: true)
            UserAction.addSendRegisterNewUser(ip, user)
        } else {
            UserAction.addSendRegisterDupeUser(ip, user)
        }
        if (user.state != User.State.ok) {
            return user
        }
        user.token = generateToken()
        use(TimeCategory) {
            user.tokenExpires = new Date() + MINUTES_VALID_TOKEN_RECOVER_PWD.minutes
        }
        user.tokenAction = User.TokenAction.recoverPwd
        user.save(flush: true)
        userMailService.sendRegisterMail(user)
        return user
    }

    @Transactional
    User recoverPassword(String ip, String usernameOrMail) {
        User user = findUserByUsernameOrEmail(usernameOrMail)
        if (!user) {
            UserAction.addSendRecoverPwdWrong(ip)
            return null
        }
        createTokenAndSendEmailForRecoverPwd(ip, user)
    }

    @Transactional
    User createTokenAndSendEmailForRecoverPwd(String ip, User user) {
        UserAction.addSendRecoverPwdOk(ip, user)
        user.token = generateToken()
        use(TimeCategory) {
            user.tokenExpires = new Date() + MINUTES_VALID_TOKEN_RECOVER_PWD.minutes
        }
        user.tokenAction = User.TokenAction.recoverPwd
        user.save(flush: true)
        userMailService.sendRecoverPassword(user)
        return user
    }

    void logout(String ip, User user, boolean deleteCookies = true) {
        UserAction.addSignout(ip, user)
        sessionService.logout(deleteCookies)
    }

    User findUserByUUID(String uuid) {
        User.findByUuidAndStateNotEqual(uuid, User.State.disabled)
    }

    User findUserByUsernameOrEmail(String usernameOrMail) {
        usernameOrMail = usernameOrMail?.trim()?.toLowerCase()
        if (!usernameOrMail) return null
        return usernameOrMail.contains("@") ? User.findByEmailAndStateNotEqual(usernameOrMail, User.State.disabled) : User.findByUsernameLCAndStateNotEqual(usernameOrMail, User.State.disabled)
    }

    static String generateToken() {
        return new BigInteger(246, random).toString(36) // 48 caracteres
//        return new BigInteger(329, random).toString(36) // 64 caracteres
//        return new BigInteger(124, random).toString(36) // 24 caracteres
//        return new BigInteger(72, random).toString(36) // 14 caracteres
//        return new BigInteger(530, random).toString(36)  // 103 caracteres
    }
    boolean isUsernameFree(User user, String username) {
        username = username?.trim()?.toLowerCase()
        if (username) {
            return User.countByUsernameLCAndIdNotEqual(username, user.id) == 0
        }
        return false

    }

    User findUserByToken(String uuid, String token, User.TokenAction action) {
        User.createCriteria().get {
            eq("uuid", uuid)
            eq("token", token)
            ne("state", User.State.disabled)
            eq("tokenAction", action)
            gt("tokenExpires", new Date())
        }
    }

    User findUserByRecoverPwdToken(String ip, String uuid, String token) {
        User user = findUserByToken(uuid, token, User.TokenAction.recoverPwd)
        if (!user) {
            UserAction.addRecoverPwdWrong(ip)
        }
        return user
    }

    void changePasswordFromToken(String ip, User user, String newPwd) {
        user.password = newPwd
        user.token = null
        user.save(flush: true)
        UserAction.addRecoverPwdOk(ip, user)
    }

    User tryCookieLogin(String ip) {
        String uuid = sessionService.getUUIDFromCookie()
        User user = uuid ? findUserByUUID(uuid) : null
        if (user) {
            UserAction.addSigninCookieOk(ip, user)
            sessionService.signedIn(user, false)
        }
        return user
    }

    String createNewPasswordUrl(User user) {
        return applicationTagLib.createLink(absolute:true, controller:"token", action: "newPassword", params: [uuid: user.uuid, token:user.token])
    }
}