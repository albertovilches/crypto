<g:set var="wallp" value="${(request.random * 8) as int /* x = (0..x-1) */}"/>
%{--<g:set var="wallp" value="${7}"/>--}%
<!-- wp:${wallp} -->
<style type="text/css">
    <g:if test="${wallp == 0}">
    body {
        background: url('<asset:assetPath src="background/chunli.png"/>') no-repeat fixed 100% 100%;
        background-color: #3d63a2;
        background-position: top;
    }
    </g:if>
    <g:elseif test="${wallp == 1}">
    body {
        background: url('<asset:assetPath src="background/gaming-girl.jpg"/>') no-repeat fixed 100% 100%;
        background-position: top;
        background-size: cover;
    }
    </g:elseif>
    <g:elseif test="${wallp == 2}">
    body {
        background: url('<asset:assetPath src="background/Background.png"/>') no-repeat fixed 100% 100%;
        background-position: top;
        background-size: cover;
    }
    </g:elseif>
    <g:elseif test="${wallp == 3}">
    body {
        background: url('<asset:assetPath src="background/retroroom.jpg"/>') no-repeat fixed 100% 100%;
        background-color: #3d63a2;
        background-position: top;
        background-size: cover;
    }
    </g:elseif>
    <g:elseif test="${wallp == 4}">
    body {
        background: url('<asset:assetPath src="background/background-night.jpg"/>') no-repeat fixed 100% 100%;
        background-size: cover;
    }
    </g:elseif>
    <g:elseif test="${wallp == 5}">
    body {
        background: url('<asset:assetPath src="background/anime-wallpaper-headphone-girl-wallpapers-hot-images.jpg"/>') no-repeat fixed 100% 100%;
        background-position: top center;
        background-size: cover;
    }
    </g:elseif>
    <g:elseif test="${wallp == 6}">
    body {
        background: url('<asset:assetPath src="background/gaming-future-girl.jpg"/>') no-repeat fixed 100% 100%;
        background-position: center;
        background-size: cover;

    }
    </g:elseif>
    <g:else>
    body {
        background: url('<asset:assetPath src="background/parasite-eve.jpg"/>') no-repeat fixed 100% 100%;
        background-color: black;
        background-position: right top;
        background-size: cover;
    }

    @media only screen and (max-width: 1200px) {
        body {
            background: url('<asset:assetPath src="background/parasite-eve.jpg"/>') no-repeat fixed 100% 100%;
            background-position: top;
            background-size: cover;
        }
    }
    </g:else>
</style>
