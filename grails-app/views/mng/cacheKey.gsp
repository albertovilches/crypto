<%@ page defaultCodec="none" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cambiar usuario</title>
</head>

<body>
<div class="col-lg-9 col-md-8 col-sm-8" style="margin-top:50px">
    <div class="modal-content">
        <div class="modal-header">
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="mng">Cache list</g:link></li>
                    <li class="active">Key</li>
                </ul>
            </div>
        </div>
        Key: <strong>${cacheKey}</strong> |
    Class: <strong>${cacheValue == null ? "null" : cacheValue.getClass()?.simpleName}</strong> |
    Size: <strong>${cacheSize}</strong>

        <div class="form-group">
            <g:link action="clearKey" params="[key: cacheKey]" class="btn btn-info"
                    style="width: 100%">Clear this entry</g:link>
        </div>
        <pre>
            <%=cacheValue.encodeAsHTML()%>
        </pre>

    </div>
</div>
</body>
</html>
