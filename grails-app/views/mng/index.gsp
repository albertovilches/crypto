<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cambiar usuario</title>
</head>

<body>
<div class="col-lg-9 col-md-8 col-sm-8" style="margin-top:50px">
    <div class="modal-content">
        <div class="modal-header">
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li class="active">Cache list</li>
                </ul>
            </div>
        </div>

        <div class="modal-body">
            <div class="form-group">
                Cache size: ${cacheSize}
                Cache elements: ${cacheCount}
                <ul>
                    <g:each in="${cacheKeys}" var="k">
                        <li><g:link action="cacheKey" params="[key: k]">${k}</g:link></li>
                    </g:each>
                </ul>
            </div>

            <div class="form-group">
                <g:link action="clearCache" class="btn btn-info" style="width: 100%">Clear all entries</g:link>
            </div>
        </div>
    </div>
</div>
</body>
</html>
