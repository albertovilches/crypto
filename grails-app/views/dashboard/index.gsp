<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Dashboard</title>
    %{--<asset:stylesheet src="lib/lobipanel/lobipanel.min.css"/>--}%
    %{--<asset:stylesheet src="separate/vendor/lobipanel.min.css"/>--}%
    %{--<asset:stylesheet src="lib/jqueryui/jquery-ui.min.css"/>--}%
    <asset:stylesheet src="separate/pages/widgets.min.css"/>

    <style type="text/css">
    .statistic-box .unit {
        font-weight: 300;
        font-size: 1.25rem;
        color: #fff;
        margin: 0 0 0 .3em;
        position: relative;
        top: -1em;
    }
    </style>
</head>


<body>

%{--<g:render template="widgets"/>--}%

<div class="row">
    <div class="col-xl-12">
        <div class="chart-statistic-box">
            <div class="chart-txt" style="height: 314px">
                <div class="chart-txt-top">
                    <p><span class="number"><cr:format amount="${totals.all.valueFiat}" format="K"/><span
                            class="unit">€</span></span>
                    </p>

                    <p class="caption">Total portfolio value</p>
                </div>
                <table class="tbl-data">
                    <tr>
                        <td class="price color-yellow text-right"><cr:format amount="${totals.crypto.valueFiat}"
                                                                           format="K"/> €</td>
                        <td>In crypto</td>
                    </tr>
                    <tr>
                        <td class="price color-cyan text-right"><cr:format amount="${totals.fiat.valueFiat}"
                                                                           format="K"/> €</td>
                        <td>In fiat</td>
                    </tr>
                    <tr>
                        <td class="price text-right color-${totals.crypto.unrealizedProfit < 0 ? 'salmon' : 'lime'}"><span><cr:format amount="${totals.crypto.unrealizedProfit}" format="k"/></span> <span class="unit">€</span></td>
                        <td>Profit<br/>
                            <span><cr:format amount="${totals.crypto.varProfit}" format="0.00 %"/>
                                <span class="caret ${totals.crypto.unrealizedProfit < 0 ? 'caret-down color-salmon' : 'caret-up color-lime'}"></span>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="price color-yellowX text-right">${tradeCount}</td>
                        <td>Trades</td>
                    </tr>
                    %{--<tr>--}%
                    %{--<td class="price color-yellow text-right">Today</td>--}%
                    %{--<td>First trade</td>--}%
                    %{--</tr>--}%
                </table>
            </div>

            <div class="chart-container">
                <div class="chart-container-in">

                    <div id="chartId" style="width: 100%; height: 314px; background-color: #FFF"></div>

                    <g:render template="balanceSerialChart"
                              model="[chart: holdingSerialChart, title: 'Portfolio value by date', divId: 'chartId']"/>

                </div>
            </div>

        </div><!--.chart-statistic-box-->
    </div><!--.col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="row">
            %{--
                        <div class="col-sm-3">
                            <article class="statistic-box red">
                                <div>
                                    <div class="number"><cr:format amount="${totals.all.valueFiat}" format="K"/><span class="unit">${currentUser.defaultCurrency}</span></div>

                                    <div class="caption"><div>Crypto value</div></div>

                                    <div class="percent">
                                        <div class="arrow up"></div>

                                        <p>15%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
                        <div class="col-sm-3">
                            <article class="statistic-box purple">
                                <div>
                                    <div class="number">12</div>

                                    <div class="caption"><div>Closes tickets</div></div>

                                    <div class="percent">
                                        <div class="arrow down"></div>

                                        <p>11%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
                        <div class="col-sm-3">
                            <article class="statistic-box yellow">
                                <div>
                                    <div class="number">104</div>

                                    <div class="caption"><div>New clients</div></div>

                                    <div class="percent">
                                        <div class="arrow down"></div>

                                        <p>5%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
                        <div class="col-sm-3">
                            <article class="statistic-box green">
                                <div>
                                    <div class="number">29</div>

                                    <div class="caption"><div>Here is an example of a long name</div>
                                    </div>

                                    <div class="percent">
                                        <div class="arrow up"></div>

                                        <p>84%</p>
                                    </div>
                                </div>
                            </article>
                        </div><!--.col-->
            --}%
            <div class="col-xl-6">

                <section class="widget">
                    <header class="widget-header-dark">Portfolio value by currency in €</header>

                    <div class="widget-tabs-content">

                        <div id="pieId" style="width: 500px; height: 500px; background-color: #FFF;margin: auto"></div>

                        <g:render template="portfolioPieChart" model="[chart: portfolioValuePie, divId: 'pieId']"/>

                        %{--
                                            <div id="pieGoogleId" style="width: 500px; height: 500px; background-color: #FFF;margin: auto"></div>

                                            <g:render template="portfolioPieChartGoogle" model="[chart: holdingSerialChart, divId: 'pieGoogleId']"/>
                        --}%
                    </div>

                </section><!--.widget-->

            </div>

            <div class="col-xl-6">
                <section class="widget">
                    <header class="widget-header-dark">Balance</header>
                    <g:render template="holdingList"
                              model="${[titleX: "Crypto", holdingList: allHoldings, columns: ["icon", "amountShort", "price", "value", "var24h"]]}"/>
                </section>
            </div>
        </div><!--.row-->
    </div><!--.col-->
</div><!--.row-->



%{--<asset:javascript src="application.js"/>--}%
%{--<asset:javascript src="lib/match-height/jquery.matchHeight.min.js"/>--}%
%{--<asset:javascript src="lib/jqueryui/jquery-ui.min.js"/>--}%
%{--<asset:javascript src="lib/lobipanel/lobipanel.min.js"/>--}%
%{--<asset:javascript src="lib/asPieProgress/jquery-asPieProgress.min.js"/>--}%

%{--<g:render template="widgetsFooter"/>--}%

%{--
<script>
    $(document).ready(function () {
        try {
            $('.panel').lobiPanel({
                sortable: true
            }).on('dragged.lobiPanel', function (ev, lobiPanel) {
                $('.dahsboard-column').matchHeight();
            });
        } catch (err) {
        }
    });
</script>
--}%

%{--<asset:javascript src="app.js"/>--}%
<g:render template="holdingLive"/>

</body>
</html>