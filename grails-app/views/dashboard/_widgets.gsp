<header class="page-content-header widgets-header">
    <div class="container-fluid">
        <div class="tbl tbl-outer">
            <div class="tbl-row">
                <g:each in="${widgets}" var="w">
                <div class="tbl-cell">
                    <div class="tbl tbl-item">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <div class="title">${w.title}</div>
                                <div class="amount ${w.style}">${w.big}</div>
                                <div class="amount-sm">${w.small}</div>
                            </div>
                            <g:if test="${w.percent != null}">
                                <div class="tbl-cell tbl-cell-progress">
                                    <div class="circle-progress-bar-typical size-56 pieProgress"
                                         role="progressbar" data-goal="${w.percent}"
                                         data-barcolor="${w.barColor}"
                                         data-barsize="10"
                                         aria-valuemin="0"
                                         aria-valuemax="100">
                                        <span class="pie_progress__number">0%</span>
                                    </div>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </div>
                </g:each>
            </div>
        </div>
    </div>
</header><!--.page-content-header-->
