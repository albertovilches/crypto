<g:if test="${!amchartsJs}">
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
</g:if>
<g:else>
    <g:set var="amchartsJs" value="${true}"/>
</g:else>
<g:if test="${!amchartsSerialJs}">
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
</g:if>
<g:else>
    <g:set var="amchartsSerialJs" value="${true}"/>
</g:else>
<g:if test="${!amchartsCommonJs}">
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all"/>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
</g:if>
<g:else>
    <g:set var="amchartsCommonJs" value="${true}"/>
</g:else>

<g:set var="divId" value="${divId ?: 'chartDiv' + System.currentTimeMillis()}"/>

<script>
    (function () {
        var data = ${raw((chart.data as grails.converters.JSON) as String)};
        var fiat = "${currentUser.defaultCurrency}";
        var chart = AmCharts.makeChart("${divId}", {
            "type": "serial",
            "categoryField": "dateString",
            "dataDateFormat": "YYYY/MM/DD", // "YYYY-MM-DD",
            "mouseWheelZoomEnabled": true,
            "marginRight": 20,
            "marginLeft": 10,
            "marginTop": 20,
            "theme": "light",
            "chartCursor": {
                "enabled": true
            },
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 20,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "trendLines": [],
            "graphs": [
                {
                    "id": "g1",
                    "lineThickness": 1,
                    "fillAlphas": 0.6,
                    "lineAlpha": 1,
//            "lineColor": "#0000FF",
//            "type": "line",
                    "title": "Fiat",
                    "balloonText": "Fiat: [[value]] " + fiat,
                    "valueField": "fiatValueFiat"
                },
                {
                    "id": "g2",
//                    "lineThickness": 2,
                    "fillAlphas": 0.6,
                    "lineAlpha": 1,
//            "lineColor": "#00FF00",
//            "type": "line",
                    "title": "Crypto",
                    "balloonText": "Crypto: [[value]] " + fiat,
                    "valueField": "cryptoValueFiat"
                }
            ],
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 1,
                "cursorColor": "#258cbb",
                "limitToGraph": "g1",
                "valueLineAlpha": 0.2,
                "valueZoomable": true
            },
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "stackType": "regular",
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "EUR"
                }
            ],
            "export": {
                "enabled": true
            },
            "categoryAxis": {
                "parseDates": true,
                "startOnAxis": true,
                "axisColor": "#DADADA",
                "gridAlpha": 0.07
//                "title": "Year",
            },

            /*
                "guides": [{
                    date: "18/06/2017",
        //            toCategory: "20/06/2017",
                    lineColor: "#CC0000",
                    lineAlpha: 1,
                    fillAlpha: 0.2,
                    fillColor: "#CC0000",
                    dashLength: 2,
                    inside: true,
                    labelRotation: 45,
                    label: "0.231231231 BTC sold"
                }, {
                    date: "19/06/2017",
                    lineColor: "#CC0000",
                    expand: true,
                    lineAlpha: 1,
                    dashLength: 2,
                    inside: true,
                    labelRotation: 45,
                    label: "1.471987123 BTC bought"
                }],
        */


            "allLabels": [],
            "balloon": {
                "borderThickness": 1
            },
            "legend": {
                "enabled": true
            },
            <g:if test="${title}">
            "titles": [
                {
                    "id": "Title-1",
                    "size": 13,
                    "text": "${title}"
                }
            ],
            </g:if>
            "dataProvider": data
        });
        chart.addListener("rendered", zoomChart);
        if (chart.zoomChart) {
            chart.zoomChart();
        }

        function zoomChart() {
//    chart.zoomToIndexes(Math.round(chart.dataProvider.length * 0.4), Math.round(chart.dataProvider.length * 0.55));
            chart.zoomToDates(parseDate("${chart.zoomFrom}"), new Date());
        }

        function parseDate(input) {
            var parts = input.split('-');
            // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
            return new Date(parts[0], parts[1] - 1, parts[2]); // Note: months are 0-based
        }
    })();
</script>