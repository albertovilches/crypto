<style type="text/css">
.valign-top {
    vertical-align: top !important;
}

.holding:hover {
    text-decoration: underline;
    cursor: pointer;
    color: #518CF6;
}

.amount {
    padding-right: 5px !important;
    font-size: 18px;
    color: #000;
}

.currency {
    color: #999;
    display: inline-block;;
}

table.sortable th:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after {
    content: " \25B2\25BC";
    color: #ddd;
    cursor: pointer;
}

.sorttable {
    cursor: pointer;
}

.table.table-sm td:first-child,
.table.table-sm td:last-child {
    padding:8px 10px;
}
.table.table-sm td {
    font-size: 15px;
    vertical-align: top;
    padding:8px 0px 4px 4px;
}

.favorite {
    color: darkred;
    cursor: pointer;
}

.table tr.comment td {
    border-top: 0;
}
</style>

<g:if test="${title}">
<section class="box-typical">
    <header class="box-typical-header">
        <div class="tbl-row">
            <div class="tbl-cell tbl-cell-title">
                <h3>${title}</h3>
            </div>
        </div>
    </header>

    <div class="box-typical-body">
</g:if>
        <div class="table-responsive">
            <script type="application/javascript">
                window.charts = window.charts || [];
            </script>

            <table id="holdingTable" class="table table-hover sortable table-sm">
                <thead>
                <tr>
                    <g:each in="${columns}" var="columnName">
                        <g:set var="column" value="${[(columnName): true]}"/>
                        <g:if test="${column.detail}">
                            <th class="text-nowrap sorttable_nosort"><!--actions--></th>
                        </g:if>
                        <g:elseif test="${column.rank}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">#</th>
                        </g:elseif>
                        <g:elseif test="${column.amount || column.amountShort}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Amount</th>
                        </g:elseif>
                        <g:elseif test="${column.icon}">
                            <th class="text-nowrap sorttable text-center"><!--icon --></th>
                        </g:elseif>
                        <g:elseif test="${column.unitCost}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Unit cost</th>
                        </g:elseif>
                        <g:elseif test="${column.price}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Price</th>
                        </g:elseif>
                        <g:elseif test="${column.var24h}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">24h</th>
                        </g:elseif>
                        <g:elseif test="${column.cost}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Cost</th>
                        </g:elseif>
                        <g:elseif test="${column.value}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Value</th>
                        </g:elseif>
                        <g:elseif test="${column.valueProfit}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Profit unrealized</th>
                        </g:elseif>
                        <g:elseif test="${column.varProfit}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Profit var.</th>
                        </g:elseif>
                        <g:elseif test="${column.profit}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Profit realized</th>
                        </g:elseif>
                        <g:elseif test="${column.valueAndProfit}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Value / profit</th>
                        </g:elseif>
                        <g:elseif test="${column.last30d}">
                            <th class="text-nowrap sorttable_nosort text-center ">Last 30 days</th>
                        </g:elseif>
                        <g:elseif test="${column.volume24hFiat}">
                            <th class="text-nowrap sorttable text-right sorttable_numeric">Volume 24h</th>
                        </g:elseif>
                        <g:elseif test="${column.actions}">
                            <th class="text-nowrap sorttable_nosort"></th>
                        </g:elseif>
                    </g:each>
                </tr>
                </thead>
                <tbody>
                <script type="application/javascript">
                    var goToHolding = function(clickable, currency) {
                        if (clickable) {
                            location.href = "${g.createLink(mapping: "holding", id:"XXX")}".replace("XXX", currency);
                        }
                    }
                </script>

                <g:each in="${holdingList}" var="${holding}" idx="i">
                    <g:set var="holdingCurrency" value="${holding.fastCurrency}"/>
                    <g:set var="clickable" value="${!("detail" in columns)}"/>
                    <tr class="holding_${holding.currencyName}" style="${clickable?'cursor:pointer':''}" onclick="goToHolding(${clickable}, '${holding.currencyName}')">

                        <g:each in="${columns}" var="columnName">
                            <g:set var="column" value="${[(columnName): true]}"/>

                            <g:if test="${column.detail}">
                                <td><i class="font-icon font-icon-plus" style="color:#adb7be"></i></td>
                            </g:if>
                            <g:elseif test="${column.rank}">
                                <!-- DB: rank -->
                                <td class="text-right"
                                    sorttable_customkey="${holdingCurrency.rank}">${holdingCurrency.rank}</td>
                            </g:elseif>

                            <g:elseif test="${column.amount || column.amountShort}">
                                <!-- DB: amount (full decimal format) -->
                                <cr:price tdClass="valign-top" amount="${holding.amount}" currency="${holdingCurrency}" format="${column.amountShort?"0.00":null}"
                                          class="holding"  onclick="goToHolding(true, '${holding.currencyName}')"/>
                            </g:elseif>

                            <g:elseif test="${column.icon}">
                                <!-- DB: icon -->
                                <td class="valign-top text-nowrap text-center"
                                    sorttable_customkey="${holding.currencyName}"><img
                                        src="${holdingCurrency.ccFullImageUrl()}" width="20"/></td>
                            </g:elseif>

                            <g:elseif test="${column.unitCost}">
                                <!-- DB: cost (unit price) -->
                                <cr:price tdClass="valign-top" amount="${holding.unitPriceFiat}"
                                          currency="${currentUser.defaultCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.price}">
                                <!-- LIVE: price -->
                                <cr:price tdClass="valign-top" amount="${holding.price}"
                                          currency="${currentUser.defaultCurrency}"
                                          class="valueUnit_${holdingCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.var24h}">
                                <!-- LIVE: change 24h -->
                                <cr:price tdClass="valign-top" amount="loading" currency="%"
                                          class="var24h_${holdingCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.cost}">
                                <!-- DB: your total cost -->
                                <cr:price tdClass="valign-top" amount="${holding.costFiat}"
                                          currency="${currentUser.defaultCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.value}">
                                <!-- LIVE: value (live price * amount) -->
                                <cr:price class="value_${holdingCurrency}" amount="${holding.valueFiat}"
                                          currency="${currentUser.defaultCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.valueProfit}">
                                <!-- LIVE: profit unrealized => live total value (live price * amount) - cost -->
                                <cr:price tdClass="valign-top" amount="${holding.unrealizedProfit}"
                                          currency="${currentUser.defaultCurrency}"
                                          class="valueProfit_${holdingCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.varProfit}">
                                <!-- LIVE: profit unrealized var % -->
                                <cr:price tdClass="valign-top" amount="${holding.varProfit * 100}" currency="%"
                                           class="varProfit_${holdingCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.profit}">
                                <!-- DB: profit realized -->
                                <cr:profit tdClass="valign-top" amount="${holding.profit}"
                                           currency="${currentUser.defaultCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.valueAndProfit}">
                                <!-- LIVE: value (live price * amount)
                                     LIVE: profit unrealized
                                 -->
                                <td class="text-nowrap text-right amount"><span
                                        class="value_${holdingCurrency}"><cr:format amount="${holding.valueFiat}" currency="${currentUser.defaultCurrency}"/></span>
                                    <span class="currency">${currentUser.defaultCurrency}</span><br/>
                                    <span class="small valueProfit_${holdingCurrency}"><cr:format amount="${holding.unrealizedProfit}" currency="${currentUser.defaultCurrency}"/></span>
                                </td>
                            </g:elseif>

                            <g:elseif test="${column.last30d}">
                                <!-- DB: 30days chart -->
                                <td id="chart_${holdingCurrency}_td" class="text-center">
                                    <g:if test="${jsonHolding[holding.currencyName].last30d}">
                                        <i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>

                                        <div id="chart_${holdingCurrency}" style="display: none">
                                            <span class="line"
                                                  style="color: white;background: white">${jsonHolding[holding.currencyName].last30d.join(", ")}</span>
                                        </div>
                                        <script type="application/javascript">
                                            charts.push(function () {
                                                $("#chart_${holdingCurrency} span.line").peity("line", {
                                                    fill: "#c6d9fd",
                                                    height: 32,
                                                    max: ${jsonHolding[holding.currencyName].last30d.max()},
                                                    min: ${jsonHolding[holding.currencyName].last30d.min()},
                                                    stroke: "#4d89f9",
                                                    width: 100
                                                });
                                                $("#chart_${holdingCurrency}_td i").hide();
                                                $("#chart_${holdingCurrency}").show();
                                            })
                                        </script>
                                    </g:if>
                                </td>
                            </g:elseif>

                            <g:elseif test="${column.volume24hFiat}">
                                <!-- LIVE: 24h var -->
                                <cr:price tdClass="valign-top" amount="loading"
                                          currency="${currentUser.defaultCurrency}"
                                          class="volume24hFiat_${holdingCurrency}"/>
                            </g:elseif>

                            <g:elseif test="${column.actions}">
                                <td class="text-nowrap"
                                    style="border-rigt:1px solid #eceeef;max-width: 50px;color: #76838e;font-siz:19px">
                                    <!-- <span class="font-icon font-icon-pencil" style="cursor: pointer; colo:#76838e"></span>-->
                                    <i class="fa fa-sticky-note favorit text-primary"></i>
                                    <i class="fa fa-heart-o favorit"></i>
                                </td>
                            </g:elseif>
                        </g:each>
                    </tr>
                </g:each>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
<g:if test="${title}">
    </div>
</section>
</g:if>

