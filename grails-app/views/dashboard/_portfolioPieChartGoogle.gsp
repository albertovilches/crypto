<g:if test="${!amchartsJs}">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</g:if>
<g:else>
    <g:set var="amchartsCommonJs" value="${true}"/>
</g:else>

<g:set var="divId" value="${divId?:'chartDiv'+System.currentTimeMillis()}"/>


<!-- Chart code -->
<script>
    (function () {
        google.charts.load('current', {'packages': ['corechart']});

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day',],
                ['Work', 11],
                ['Eat', 2],
                ['Commute', 2],
                ['Watch TV', 2],
                ['Eat', 2],
                ['Commute', 2],
                ['Watch TV', 2],
                ['Eat', 2],
                ['Commute', 2],
                ['Watch TV', 2],
                ['Sleep', 7]
            ]);

            var options = {
//                title: 'My Daily Activities',
                pieHole: 0.2,
                sliceVisibilityThreshold: 0.05 ,
                legend: {maxLines:10, position: 'top', alignment: 'center', textStyleX: {color: 'blue', fontSize: 16}},
//                chartArea: {left:0, top:0},
                pieSliceText: 'label'
//                legend: 'none'
            };

            var chart = new google.visualization.PieChart(document.getElementById('${divId}'));
            chart.draw(data, options);
        }

        google.charts.setOnLoadCallback(drawChart);

    })();
</script>