<style type="text/css">

.flash {
    -moz-animation: flash 2s ease-out;
    -moz-animation-iteration-count: 1;

    -webkit-animation: flash 2s ease-out;
    -webkit-animation-iteration-count: 1;

    -ms-animation: flash 2s ease-out;
    -ms-animation-iteration-count: 1;
}

@-webkit-keyframes flash {
    0% {
        background-color: none;
    }
    20% {
        background-color: #daf3dc;
    }
    100% {
        background-color: none;
    }
}

@-moz-keyframes flash {
    0% {
        background-color: none;
    }
    20% {
        background-color: #daf3dc;
    }
    100% {
        background-color: none;
    }
}

@-ms-keyframes flash {
    0% {
        background-color: none;
    }
    20% {
        background-color: #daf3dc;
    }
    100% {
        background-color: none;
    }
}

</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.2/socket.io.js"></script>
<script src="https://cryptoqween.github.io/streamer/ccc-streamer-utilities.js"></script>
<asset:javascript src="sorttable.js"/>
<asset:javascript src="cryptocompare.js"/>
<asset:javascript src="lib/peity/jquery.peity.min.js"/>
%{--<script src="js/lib/table-edit/jquery.tabledit.min.js"></script>--}%

<script type="application/javascript">
    window.charts = window.charts || [];
    $(function () {
        var arrayLength = charts.length;
        for (var i = 0; i < arrayLength; i++) {
            charts[i]();
        }
        var holdings = ${raw((jsonHolding as grails.converters.JSON) as String)};
        /*
            $('#table-edit').Tabledit({
                url: 'example.php',
                columns: {
                    identifier: [0, 'id'],
                    editable: [[1, 'name'], [2, 'description']]
                }
            });
        */

        CCStream.start();
        CCStream.subscribeHoldingsToFiat(holdings, "${fiat.name}", {
            var24h: "color",
            value: "color",
            valueUnit: "caret",
            valueProfit: "color",
            varProfit: "color",
            onUpdate: function (o, quote) {
                sorttable.makeSortable(document.getElementById("holdingTable"));
            }

        })
    });
</script>