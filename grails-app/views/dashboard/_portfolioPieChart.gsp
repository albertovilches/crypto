<g:if test="${!amchartsJs}">
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
</g:if>
<g:else>
    <g:set var="amchartsJs" value="${true}"/>
</g:else>
<g:if test="${!amchartsPieJs}">
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
</g:if>
<g:else>
    <g:set var="amchartsPieJs" value="${true}"/>
</g:else>
<g:if test="${!amchartsCommonJs}">
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all"/>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
</g:if>
<g:else>
    <g:set var="amchartsCommonJs" value="${true}"/>
</g:else>

<g:set var="divId" value="${divId?:'chartDiv'+System.currentTimeMillis()}"/>

<!-- Chart code -->
<script>
    (function() {
        var data = ${raw((chart.data as grails.converters.JSON) as String)};
        var chart = AmCharts.makeChart("${divId}", {
            "groupPercent": 2.5,
//            "groupedDescription": "All less than 4% cool",
            "groupedTitle": "Others",
//            "handDrawn": "All less than 4%",
            "labelRadius": -10,
            "type": "pie",
            "startDuration": 0,
//            labelsEnabled: false,
             "autoMargins": false,
             "pullOutRadius": 0,
            "marginLeft": 0,
            "marginRight": 0,
            "marginTop": 0,
            "marginBottom": 10,
            "theme": "light",
            "colors":[
                "#cb7c79",
                "#d07357",
                "#ec7b35",
                "#f1ae3f",
                "#fae64c",
                "#d9ea4b",
                "#8dcc6f",
                "#98c993",
                "#90cdae",
                "#acd8db",
                "#aac3e6",
                "#8db4d1",
                "#909fe6",
                "#867ee9"
            ],
            "addClassNames": true,
            "legend": {
                "position": "bottom",
                "marginRight": 0,
                "autoMargins": false
            },
            "innerRadius": "30%",
//            baseColor: "#a54344",
//            brightnessStep: 10,
            "defs": {
                "filter": [{
                    "id": "shadow",
                    "width": "200%",
                    "height": "200%",
                    "feOffset": {
                        "result": "offOut",
                        "in": "SourceAlpha",
                        "dx": 0,
                        "dy": 0
                    },
                    "feGaussianBlur": {
                        "result": "blurOut",
                        "in": "offOut",
                        "stdDeviation": 5
                    },
                    "feBlend": {
                        "in": "SourceGraphic",
                        "in2": "blurOut",
                        "mode": "normal"
                    }
                }]
            },
            "dataProvider": data,
            "valueField": "value",
            "titleField": "currency",
            "export": {
                "enabled": true
            }
        });

        chart.addListener("init", handleInit);

        chart.addListener("rollOverSlice", function (e) {
            handleRollOver(e);
        });

        function handleInit() {
            chart.legend.addListener("rollOverItem", handleRollOver);
        }

        function handleRollOver(e) {
            var wedge = e.dataItem.wedge.node;
            wedge.parentNode.appendChild(wedge);
        }
    })();
</script>