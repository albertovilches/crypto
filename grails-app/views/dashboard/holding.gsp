<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Sign In</title>


    <asset:stylesheet src="lib/jqueryui/jquery-ui.min.css"/>
    <asset:stylesheet src="separate/pages/widgets.min.css"/>

</head>


<body>

%{--
<header class="page-content-header widgets-header">
    <div class="container-fluid">
        <div class="tbl tbl-outer">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <div class="tbl tbl-item">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <div class="title">Pageviews</div>
                                <div class="amount color-blue">20 750 000</div>
                                <div class="amount-sm">20 750 000</div>
                            </div>
                            <div class="tbl-cell tbl-cell-progress">
                                <div class="circle-progress-bar-typical size-56 pieProgress"
                                     role="progressbar" data-goal="79"
                                     data-barcolor="#00a8ff"
                                     data-barsize="10"
                                     aria-valuemin="0"
                                     aria-valuemax="100">
                                    <span class="pie_progress__number">0%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-cell">
                    <div class="tbl tbl-item">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <div class="title">Pageviews</div>
                                <div class="amount color-blue">20 750 000</div>
                                <div class="amount-sm">20 750 000</div>
                            </div>
                            <div class="tbl-cell tbl-cell-progress">
                                <div class="circle-progress-bar-typical size-56 pieProgress"
                                     role="progressbar" data-goal="79"
                                     data-barcolor="#00a8ff"
                                     data-barsize="10"
                                     aria-valuemin="0"
                                     aria-valuemax="100">
                                    <span class="pie_progress__number">0%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-cell">
                    <div class="tbl tbl-item">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <div class="title">Pages/Visit</div>
                                <div class="amount">2,4</div>
                                <div class="amount-sm">2,3</div>
                            </div>
                            <div class="tbl-cell tbl-cell-progress">
                                <div class="circle-progress-bar-typical size-56 pieProgress"
                                     role="progressbar" data-goal="75"
                                     data-barcolor="#929faa"
                                     data-barsize="10"
                                     aria-valuemin="0"
                                     aria-valuemax="100">
                                    <span class="pie_progress__number">0%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-cell">
                    <div class="tbl tbl-item">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <div class="title">Visit Duration</div>
                                <div class="amount">2m23s</div>
                                <div class="amount-sm">2m10s</div>
                            </div>
                            <div class="tbl-cell tbl-cell-progress">
                                <div class="circle-progress-bar-typical size-56 pieProgress"
                                     role="progressbar" data-goal="75"
                                     data-barcolor="#929faa"
                                     data-barsize="10"
                                     aria-valuemin="0"
                                     aria-valuemax="100">
                                    <span class="pie_progress__number">0%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbl-cell">
                    <div class="tbl tbl-item">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <div class="title">Revenue</div>
                                <div class="amount">N/A</div>
                                <div class="amount-sm">N/A</div>
                            </div>
                            <div class="tbl-cell tbl-cell-progress">
                                <div class="circle-progress-bar-typical size-56 pieProgress"
                                     role="progressbar" data-goal="75"
                                     data-barcolor="#929faa"
                                     data-barsize="10"
                                     aria-valuemin="0"
                                     aria-valuemax="100">
                                    <span class="pie_progress__number">0%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header><!--.page-content-header-->
--}%


<div class="row">
    <div class="col-xl-12">
        <div class="chart-statistic-box">
            <div class="chart-txt" style="height: 400px">
                <div class="chart-txt-top">
                    <p><span class="number value_${holding.fastCurrency.name}"><cr:format amount="${holding.valueFiat}" format="K"/></span><span class="unit">€</span></p>
                    <p class="caption">${holding.fastCurrency.displayName} <g:if test="${holding.fastCurrency.name != holding.fastCurrency.displayName}">(${holding.fastCurrency.name})</g:if></p>
                    <p><span style="color:#DDD !important"><cr:format amount="${holding.amount}" currency="${holding.fastCurrency}"/></span></p>
                </div>
                <table class="tbl-data">
                    <tr>
                        <td class="price color-orange text-right" style="coor:#dcab5f !important;"><cr:format amount="${holding.costFiat}" currency="${fiat}"/> <span class="unit">€</span></td>
                        <td>Cost</td>
                    </tr>
%{--
                    <tr>
                        <td class="price text-right color-yellow"><span class="value_${holding.fastCurrency.name}"><cr:format  amount="${holding.valueFiat}" currency="${fiat}"/></span> <span class="unit">€</span></td>
                        <td>Value</td>
                    </tr>
--}%
                    <tr>
                        <td class="price text-right" style="colr:#dcab5f !important;"><cr:format amount="${holding.unitPriceFiat}" currency="${fiat}"/> <span class="unit">€</span></td>
                        <td>Unit cost</td>
                    </tr>
                    <tr>
                        <td class="price text-right"><span class="valueUnit_${holding.fastCurrency.name}"><cr:format amount="${holding.price}" currency="${fiat}"/></span> <span class="unit">€</span></td>
                        <td>Price</td>
                    </tr>
                    <tr>
                        <td class="price text-right  color-${holding.unrealizedProfit < 0 ? 'salmon' : 'lime'}"><span class="valueProfit_${holding.fastCurrency.name}"><cr:format amount="${holding.unrealizedProfit}" currency="${fiat}"/></span> <span class="unit">€</span></td>
                        <td>Profit<br/>
                            <span class="varProfit_${holding.fastCurrency.name}">
                            <cr:format amount="${holding.varProfit}" format="0.00 %"/></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="price color-purpleXX text-right">${tradeCount}</td>
                        <td>Trades</td>
                    </tr>
%{--
                    <tr>
                        <td class="price color-lime text-right">May/2013</td>
                        <td>Last trade</td>
                    </tr>
                    <tr>
                        <td class="price color-lime text-right">Today</td>
                        <td>First trade</td>
                    </tr>
--}%
                </table>
            </div>

            <div class="chart-container">
                <div class="chart-container-in">
                    <div id="chartId" style="width: 100%; height: 400px; background-color: #FFF"></div>
                    <g:render template="holdingSerialChart" model="[chart: holdingSerialChart, title: 'Cost vs value by date', divId: 'chartId']"/>

                    </div>
                </div>
            </div>
        </div><!--.chart-statistic-box-->
    </div><!--.col-->


%{--
    <div class="col-xl-12">
        <div class="row">
            <div class="col-sm-3">
                <article class="statistic-box red">
                    <div>
                        <div class="number valueUnit_${holding.fastCurrency.name}"></div>

                        <div class="caption"><div>Unit price</div></div>

                        <div class="percent">
                            <div class="arrow up"></div>
                            <p>15%</p>
                        </div>
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-3">
                <article class="statistic-box purple">
                    <div>
                        <div class="number">12</div>

                        <div class="caption"><div>Closes tickets</div></div>

                        <div class="percent">
                            <div class="arrow down"></div>

                            <p>11%</p>
                        </div>
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-3">
                <article class="statistic-box yellow">
                    <div>
                        <div class="number">104</div>

                        <div class="caption"><div>New clients</div></div>

                        <div class="percent">
                            <div class="arrow down"></div>

                            <p>5%</p>
                        </div>
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-3">
                <article class="statistic-box green">
                    <div>
                        <div class="number">29</div>

                        <div class="caption"><div>Here is an example of a long name</div>
                        </div>

                        <div class="percent">
                            <div class="arrow up"></div>

                            <p>84%</p>
                        </div>
                    </div>
                </article>
            </div><!--.col-->
        </div><!--.row-->
    </div><!--.col-->
--}%
</div><!--.row-->


<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.2/socket.io.js"></script>
<script src="https://cryptoqween.github.io/streamer/ccc-streamer-utilities.js"></script>
<asset:javascript src="cryptocompare.js"/>
<asset:javascript src="lib/peity/jquery.peity.min.js"/>

<script type="application/javascript">
    window.charts = window.charts || [];
    $(function () {
        var arrayLength = charts.length;
        for (var i = 0; i < arrayLength; i++) {
            charts[i]();
        }
        var holdings = ${raw((jsonHolding as grails.converters.JSON) as String)};
        CCStream.start();
        CCStream.subscribeHoldingsToFiat(holdings, "${fiat.name}", {
            classes: {up:"color-lime", down:"color-salmon"},
            var24h: "color",
            varProfit: "caret",
            valueFormat: "K",
            valueProfit: "color",
            onUpdate: function (o) {


            }

        })
    });
</script>
</body>
</html>