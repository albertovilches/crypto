<script type="application/javascript">
$(function() {
    $(".circle-progress-bar").asPieProgress({
        namespace: 'asPieProgress',
        speed: 500
    });

    $(".circle-progress-bar").asPieProgress("start");


    $(".circle-progress-bar-typical").asPieProgress({
        namespace: 'asPieProgress',
        speed: 25
    });

    $(".circle-progress-bar-typical").asPieProgress("start");
});
</script>