<%@ page import="grails.converters.JSON; tools.StringTools; tools.UAgent" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>

    <style type="text/css">
    .flash {
      -moz-animation: flash 1s ease-out;
      -moz-animation-iteration-count: 1;

      -webkit-animation: flash 1s ease-out;
      -webkit-animation-iteration-count: 1;

      -ms-animation: flash 1s ease-out;
      -ms-animation-iteration-count: 1;
    }

    @-webkit-keyframes flash {
        0% { background-color: none; }
        50% { background-color: #000000; }
        100% { background-color: none; }
    }

    @-moz-keyframes flash {
        0% { background-color: none; }
        50% { background-color: #000000; }
        100% { background-color: none; }
    }

    @-ms-keyframes flash {
        0% { background-color: none; }
        50% { background-color: #000000; }
        100% { background-color: none; }
    }
    </style>

    <script type="application/javascript">

/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

;( function( $, window, document, undefined ) {
    var s = document.body || document.documentElement, s = s.style, prefixAnimation = '', prefixTransition = '';

    if (s.WebkitAnimation == '') prefixAnimation = '-webkit-';
    if (s.MozAnimation == '') prefixAnimation = '-moz-';
    if (s.OAnimation == '') prefixAnimation = '-o-';

    if (s.WebkitTransition == '') prefixTransition = '-webkit-';
    if (s.MozTransition == '') prefixTransition = '-moz-';
    if (s.OTransition == '') prefixTransition = '-o-';

    $.fn.extend({
        onCSSAnimationEnd: function (callback) {
            var $this = $(this).eq(0);
            $this.one('webkitAnimationEnd mozAnimationEnd oAnimationEnd oanimationend animationend', callback);
            if (( prefixAnimation == '' && !( 'animation' in s ) ) || $this.css(prefixAnimation + 'animation-duration') == '0s') callback();
            return this;
        },
        onCSSTransitionEnd: function (callback) {
            var $this = $(this).eq(0);
            $this.one('webkitTransitionEnd mozTransitionEnd oTransitionEnd otransitionend transitionend', callback);
            if (( prefixTransition == '' && !( 'transition' in s ) ) || $this.css(prefixTransition + 'transition-duration') == '0s') callback();
            return this;
        }
    });
})( jQuery, window, document );

        var quote = {};

        var createDom = function(pair) {
        	var wrapper = document.getElementById("content");
        	var div = document.createElement("div");
        	var html = '<div class="wrapper">';
        	html += '<h1><span id="fsym_'+ pair +'"></span> - <span id="tsym_'+ pair +'"></span>   <strong><span class="price" id="price_'+ pair +'"></span></strong></h1>';
        	html += '<div class="label">24h Change: <span class="value" id="change_'+ pair +'"></span> (<span class="value" id="changepct_'+ pair +'"></span>)</div>';
        	html += '<div class="label">Last Market: <span class="market" id="market_'+ pair +'"></span></div>';
        	html += '<div class="label">Last Trade Id: <span class="value" id="tradeid_'+ pair +'"></span></div>';
        	html += '<div class="label">Last Trade Volume: <span class="value" id="volume_'+ pair +'"></span></div>';
        	html += '<div class="label">Last Trade VolumeTo: <span class="value" id="volumeto_'+ pair +'"></span></div>';
        	html += '<div class="label">24h Volume: <span class="value" id="24volume_'+ pair +'"></span></div>';
        	html += '<div class="label">24h VolumeTo: <span class="value" id="24volumeto_'+ pair +'"></span></div>';
        	html += '<div class="source"> Source: <a href="http://www.cryptocompare.com">CryptoCompare</a></div>';
        	html += '</div>';
        	div.innerHTML = html;
        	wrapper.appendChild(div);
        };

        var displayQuote = function(_quote) {
        	var fsym = CCC.STATIC.CURRENCY.SYMBOL[_quote.FROMSYMBOL];
        	var tsym = CCC.STATIC.CURRENCY.SYMBOL[_quote.TOSYMBOL];
        	var pair = _quote.FROMSYMBOL + _quote.TOSYMBOL;
        	console.log(pair, _quote);
        	document.getElementById("market_" + pair).innerHTML = _quote.LASTMARKET;
        	document.getElementById("fsym_" + pair).innerHTML = _quote.FROMSYMBOL;
        	document.getElementById("tsym_" + pair).innerHTML = _quote.TOSYMBOL;
        	document.getElementById("price_" + pair).innerHTML = _quote.PRICE;
        	document.getElementById("volume_" + pair).innerHTML = CCC.convertValueToDisplay(fsym, _quote.LASTVOLUME);
        	document.getElementById("volumeto_" + pair).innerHTML = CCC.convertValueToDisplay(tsym, _quote.LASTVOLUMETO);
        	document.getElementById("24volume_" + pair).innerHTML = CCC.convertValueToDisplay(fsym, _quote.VOLUME24HOUR);
        	document.getElementById("24volumeto_" + pair).innerHTML = CCC.convertValueToDisplay(tsym, _quote.VOLUME24HOURTO);
        	document.getElementById("tradeid_" + pair).innerHTML = _quote.LASTTRADEID.toFixed(0);
        	document.getElementById("tradeid_" + pair).innerHTML = _quote.LASTTRADEID.toFixed(0);
        	document.getElementById("change_" + pair).innerHTML = CCC.convertValueToDisplay(tsym, _quote.CHANGE24H);
        	document.getElementById("changepct_" + pair).innerHTML = _quote.CHANGEPCT24H.toFixed(2) + "%";

        	if (_quote.FLAGS === "1"){
        		document.getElementById("price_" + pair).className = "up";
        	}
        	else if (_quote.FLAGS === "2") {
        		document.getElementById("price_" + pair).className = "down";
        	}
        	else if (_quote.FLAGS === "4") {
        		document.getElementById("price_" + pair).className = "";
        	}
        }

        var socket = io.connect('https://streamer.cryptocompare.com/');

        var updateQuote = function(result) {
            if (isNaN(result.PRICE)) {
           	    return;
            }

        	var keys = Object.keys(result);
        	var pair = result.FROMSYMBOL + result.TOSYMBOL;

        	if (!quote.hasOwnProperty(pair)) {
        		quote[pair] = {}
//        		createDom(pair);
        	}
        	if (result.TOSYMBOL != "EUR") {
                if (quote.hasOwnProperty(result.FROMSYMBOL+"EUR")) {
                    // Me llega una cotizacion contra ETH o BTC y ya la tengo en FIAT, la borro
                    socket.emit('SubRemove', {subs: ['5~CCCAGG~'+result.FROMSYMBOL + '~' + result.TOSYMBOL]});
                    delete quote[pair];
                    return
                } else if (result.TOSYMBOL == "ETH" && quote.hasOwnProperty(result.FROMSYMBOL+"BTC")) {
                    socket.emit('SubRemove', {subs: ['5~CCCAGG~'+result.FROMSYMBOL + '~' + result.TOSYMBOL]});
                    delete quote[pair];
                    return
                }
            }

        	for (var i = 0; i <keys.length; ++i) {
        		quote[pair][keys[i]] = result[keys[i]];
        	}
        	quote[pair]["CHANGE24H"] = quote[pair]["PRICE"] - quote[pair]["OPEN24HOUR"];
        	quote[pair]["CHANGEPCT24H"] = quote[pair]["CHANGE24H"]/quote[pair]["OPEN24HOUR"] * 100;
//            console.log(quote);
//        	displayQuote(quote[pair]);
        	updateTable(result.FROMSYMBOL, result.TOSYMBOL, quote[pair])
        }

        var updateTable = function(from, to, _quote) {
            var price = _quote.PRICE;
            if (to != "EUR") {
                if (!quote.hasOwnProperty(to+"EUR")) {
                    console.log("No hay conversion intermedia todavia")
                    return;
                }
                // El precio me esta viniendo en BTC o ETH
                price = price * quote[to+"EUR"].PRICE;
                if (isNaN(price)) {
                    console.log("oh no")
                    return;
                }

            }

            var currency = _quote.FROMSYMBOL;
            var amount = holdings[currency].amount;
            var unitCost = holdings[currency].unitCost;
            var cost = amount * unitCost;
            var value = amount * price;
            var profit = value - cost;

            $("#value_"+currency).html(value.toFixed(2));
            $("#valueProfit_"+currency).html(profit.toFixed(2));
            $("#valueUnit_"+currency).html(price.toFixed(2));
            $("#var_"+currency).html(((profit/cost)*100).toFixed(2));

            var tds = $("#holding_"+currency).find('td')

            for (var i = 0, len = tds.length; i < len; i++) {
                $(tds[i]).addClass("flash").onCSSAnimationEnd( function() { $( this ).removeClass("flash"); });;;
            };


//            $("#value_"+currency).parent().addClass("flash").onCSSAnimationEnd( function() { $( this ).removeClass("flash"); });;;
//            $("#valueProfit_"+currency).parent().addClass("flash").onCSSAnimationEnd( function() { $( this ).removeClass("flash"); });;;
//            $("#valueUnit_"+currency).parent().addClass("flash").onCSSAnimationEnd( function() { $( this ).removeClass("flash"); });;;
//            $("#var_"+currency).parent().addClass("flash").onCSSAnimationEnd( function() { $( this ).removeClass("flash"); });;;


        }

        var holdings = ${raw((jsonHolding as grails.converters.JSON) as String)}

        //Format: {SubscriptionId}~{ExchangeName}~{FromSymbol}~{ToSymbol}
        //Use SubscriptionId 0 for TRADE, 2 for CURRENT and 5 for CURRENTAGG
        //For aggregate quote updates use CCCAGG as market
        var subscription = []; // '5~CCCAGG~BTC~EUR','5~CCCAGG~ETH~EUR'];
        var holdingCurrencies = Object.keys(holdings);
        subscription.push("5~CCCAGG~ETH~EUR");
        subscription.push("5~CCCAGG~BTC~EUR");
        for (var i = 0, len = holdingCurrencies.length; i < len; i++) {
            var coin = holdingCurrencies[i];
            subscription.push("5~CCCAGG~"+coin+"~EUR");
            subscription.push("5~CCCAGG~"+coin+"~BTC");
            subscription.push("5~CCCAGG~"+coin+"~ETH");
        }
        socket.emit('SubAdd', {subs: Array.from(new Set(subscription))} );

        socket.on("m", function(message){
        	var messageType = message.substring(0, message.indexOf("~"));
        	var res = {};
        	if (messageType === CCC.STATIC.TYPE.CURRENTAGG) {
        		res = CCC.CURRENT.unpack(message);
//        		console.log(res);
        		updateQuote(res);
            }
        });

    </script>
</head>

<body>

<div id="content"></div>
<div class="col-lg-9 col-md-9 col-sm-8">
    <div class="panel">
        <nav class="navbar" style="margin-bottom:0;margin-top:10px">
            <div class="container-fluid">
                <div class="navbar-header">
                    <span class="navbar-brand info-header"><span class="info-number">${credits}</span></span>
                    <span class="navbar-brand info-header" style="color: #666">Total acumulado: <span
                            style="color: #666;" class="info-number">${totalAccumulated}</span></span>
                </div>

                <g:if test="${ops}">
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            %{--<li><a href=""> Copiar todos</a></li>--}%
                            %{--
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">Acciones <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#"><i class="fa fa-trash fa-fw" aria-hidden="true"></i> Borrar todos</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            --}%
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><g:link controller="info" action="credits"><i class="fa fa-info-circle fa-fw"
                                                                              aria-hidden="true"></i> Ayuda</g:link>
                            </li>
                        </ul>
                    </div>
                </g:if>
            </div>
        </nav>

        <g:if test="${holdings}">
            <style type="text/css">
                .amount {
                    padding-right: 3px !important;
                }
                .currency {
                    padding-left: 0px !important;
                }
            </style>

            <h3>Fiat</h3>
            <table class="table table-striped table-hover panel panel-info"
                   style="margin:0 auto 15px auto; width: 97.5%">
                <thead>
                <tr>
                    <th colspan="2" class="text-right">In</th>
                    <th colspan="2" class="text-right">Out</th>
                    <th colspan="2" class="text-right">Balance</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${fiatHoldings}" var="${holding}" idx="i">
                    <tr>
                        <cr:price amount="${holding.totalIn}" currency="${holding.currency}"/>
                        <cr:price amount="${holding.totalOut*-1}" currency="${holding.currency}"/>
                        <cr:price amount="${holding.amount}" currency="${holding.currency}"/>
                        <cr:price amount="${holding.profit}" currency="${holding.currency}"/>
                    </tr>

                </g:each>
                </tbody>
            </table>
            <h3>Crypto</h3>
            <table class="table table-striped table-hover panel panel-info"
                   style="margin:0 auto 15px auto;">
                <thead>
                <tr>
                    <th colspan="2">Currency</th>
                    <th colspan="2" class="text-right">Holding</th>
                    <th colspan="2" class="text-right">Cost unit</th>
                    <th colspan="2" class="text-right">Value unit<br/>today</th>
                    <th colspan="2" class="text-right">Variación</th>
                    <th colspan="2" class="text-right">Cost</th>
                    <th colspan="2" class="text-right">Value<br/>today</th>
                    <th colspan="2" class="text-right">Profit unrealized</th>
                    <th colspan="2" class="text-right">Profit realized</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${cryptoHoldings}" var="${holding}" idx="i">
                    <tr id="holding_${holding.currency}">
                        <td class="text-nowrap"><img src="${holding.currency.ccFullImageUrl()}" width="16"/> </td>
                        <td class="text-nowrap">${holding.currency}</td>
                        <cr:price amount="${holding.amount}" currency="" format="0.00000000"/>
                        <cr:price amount="${holding.unitPriceFiat}" currency="${currentUser.defaultCurrency}"/>
                        <cr:price id="valueUnit_${holding.currency}" amount="${holding.valueFiat/(holding.amount?:1)}" currency="${currentUser.defaultCurrency}"/>
                        <cr:profit id="var_${holding.currency}" amount="${((holding.valueFiat-holding.costFiat)/(holding.costFiat?:1))*100}" currency="%"/>
                        <cr:price amount="${holding.costFiat}" currency="${currentUser.defaultCurrency}"/>
                        <cr:price id="value_${holding.currency}" amount="${holding.valueFiat}" currency="${currentUser.defaultCurrency}"/>
                        <cr:profit id="valueProfit_${holding.currency}"  amount="${holding.valueFiat-holding.costFiat}" currency="${currentUser.defaultCurrency}"/>
                        <cr:profit amount="${holding.profit}" currency="${currentUser.defaultCurrency}"/>
                    </tr>

                </g:each>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4"></td>
                        <td colspan="4"></td>
                        <td colspan="2"></td>

                        <cr:price amount="${cryptoHoldings.sum { it.costFiat }}" currency="${currentUser.defaultCurrency}"/>
                        <cr:price amount="${cryptoHoldings.sum { it.valueFiat }}" currency="${currentUser.defaultCurrency}"/>
                        <cr:profit amount="${cryptoHoldings.sum { it.valueFiat - it.costFiat }}" currency="${currentUser.defaultCurrency }"/>
                        <cr:profit amount="${cryptoHoldings.sum { it.profit }}" currency="${currentUser.defaultCurrency }"/>
                    </tr>

                </tfoot>
            </table>
        </g:if>
        <g:else>
            <div class="text-center panel panel-default text-primary"
                 style="margin:0 15px 15px;padding:15px;">No data to show.</div>
        </g:else>
    </div>
</div>
</body>
</html>
