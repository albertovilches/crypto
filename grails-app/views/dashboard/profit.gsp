<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Sign In</title>
    <style type="text/css">
    </style>

</head>

<body>

<g:render template="holdingList" model="${[title:"Profit", holdingList: allHoldings, columns: ["detail", "amount", "icon", "unitCost", "price", "cost", "valueAndProfit", "varProfit","profit"]]}"/>
<g:render template="holdingLive"/>

</body>
</html>