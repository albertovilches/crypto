<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Sign In</title>
    <style type="text/css">
    </style>

</head>

<body>

<g:render template="holdingList" model="${[title:"Holdings", holdingList: allHoldings, columns: ["detail", "amount", "icon", "price", "var24h", "cost", "value", "last30d", "volume24hFiat"]]}"/>
<g:render template="holdingLive"/>

</body>
</html>