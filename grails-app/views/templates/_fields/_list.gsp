        <div class="well bs-component">
                <fieldset class="form">
                <g:each in="${domainClass.persistentProperties}" var="p">
                    <div class="row">
                    <div class="form-group">
                        <label class="col-lg-2 control-label" style="text-align: right">${p.naturalName}</label>

                        <div class="col-lg-10">
                            ${body(p)}
                        </div>
                    </div>
                    </div>
                </g:each>
                </fieldset>
        </div>

