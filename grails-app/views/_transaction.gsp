
<asset:javascript src="app.js"/>
<asset:javascript src="plugins.js"/>

<asset:stylesheet  src="lib/flatpickr/flatpickr.min.css"/>
<asset:stylesheet  src="separate/vendor/bootstrap-daterangepicker.min.css"/>
%{--<asset:stylesheet  src="separate/vendor/bootstrap-select/bootstrap-select.min.css"/>--}%
%{--<asset:stylesheet src="separate/vendor/select2.min.css"/>--}%
%{--<asset:stylesheet src="separate/vendor/typeahead.min.css"/>--}%

<style type="text/css">
.flatpickr-calendar {
margin-top: 4px;
border: 1px solid #e9ebee;
-webkit-border-radius: 3px;
border-radius: 3px
}

.flatpickr-calendar::after, .flatpickr-calendar::before {
display: none
}

.flatpickr-calendar:not(.inline).arrowBottom {
margin-top: -4px
}

.flatpickr-calendar.noCalendar.showTimeInput.hasTime .flatpickr-time {
border-top: none
}

.flatpickr-input {
border: solid 1px rgba(197, 214, 222, .7);
-webkit-box-shadow: none;
box-shadow: none;
font-size: 1rem;
display: block;
width: 100%;
padding: .5rem .75rem;
line-height: 1.25;
background-color: #fff;
background-image: none;
background-clip: padding-box;
-webkit-border-radius: .25rem;
border-radius: .25rem;
-webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
color: #343434 !important
}

.flatpickr-input:focus {
border-color: #c5d6de
}

.flatpickr-input.form-control-error {
background-color: #fff6f6;
border-color: #fa424a;
color: #fa424a !important
}

.flatpickr-input:disabled {
background-color: #eceff4
}

.flatpickr-current-month {
margin-right: -20px
}

.flatpickr-days {
display: -webkit-box;
display: -webkit-flex;
display: -ms-flexbox;
display: flex;
-webkit-box-pack: justify;
-webkit-justify-content: space-between;
-ms-flex-pack: justify;
justify-content: space-between;
-webkit-flex-wrap: wrap;
-ms-flex-wrap: wrap;
flex-wrap: wrap
}

.flatpickr-day {
border: 0;
-webkit-border-radius: 3px;
border-radius: 3px;
color: #000;
line-height: 35px
}

.flatpickr-day.today {
color: #fff;
background: #00a8ff;
border: none;
font-weight: 700
}

.flatpickr-day.today:hover {
border-color: #00a8ff
}

.flatpickr-day.selected, .flatpickr-day:hover {
color: #fff;
background-color: #00a8ff
}

.flatpickr-day.selected:hover, .flatpickr-day:hover:hover {
color: #fff;
background-color: #00a8ff
}

.flatpickr-day .disabled:hover {
color: #f6f8fa
}

.flatpickr-day.nextMonthDay, .flatpickr-day.prevMonthDay {
color: #999
}

#tradeBox {

}
#tradeBox section {

    -webkit-box-shadow: 0 5px 30px rgba(0, 0, 0, 0.14);
       -moz-box-shadow: 0 5px 30px rgba(0, 0, 0, 0.14);
            box-shadow: 0 5px 30px rgba(0, 0, 0, 0.14);

}

.closePure {
    z-index: 10;
  position: absolute;
  right: 18px;
  top: 12px;
  width: 30px;
  height: 30px;
  opacity: 0.3;
    cursor: pointer;
}
.closePure:hover {
    cursor: pointer!important;
  opacity: 1;
}
.closePure:before, .closePure:after {
  position: absolute;
  left: 15px;
  content: ' ';
  height: 33px;
  width: 2px;
  background-color: #333;
}
.closePure:before {
  transform: rotate(45deg);
}
.closePure:after {
  transform: rotate(-45deg);
}

</style>


<div id="tradeBox" class="modal hide" role="dialog">
<div class="modal-dialog" id="subTradeBox">
<section class="card box-typical">
    <form id="transactionForm" name="transactionForm" method="POST">

    %{--<span id="closeButton" style="display:inline;margin:10px 10px 0 0; font-size: 20px; color: #adb7be!important; cursor:pointer;float: right"><i class="glyphicon glyphicon-remove"></i></span>--}%
        <a href="#" class="closePure" id="closeButton"></a>

    <div class="card-block" style="padding: 16px 30px">
        <div class="row">
            <div class="col-lg-12">
            <h3>Add new transaction</h3>
            %{--<hr/>--}%
            </div>

            <div class="col-lg-12">
                <fieldset style="margin-bottom: 1rem">
                    <label class="form-label label-big" for="tradeDate">Date</label>
                    <div class="form-group" style="margin-bottom: 0">
                        <div class="input-group date" style="width: 210px">
                            <input id="tradeDate" name="tradeDate"
                                   data-inputmask-alias="datetime"
                                   data-inputmask-inputformat="yyyy-mm-dd HH:MM"
                                   data-inputmask-placeholder="_"
                                   class="form-control flatpickr"
                                   data-enable-time="true" data-time_24hr="true"
                                   data-allow-input="true"
                                   data-default-date="${new Date().format("yyyy-MM-dd HH:mm")}"
                                   data-click-opens="false"
                                   data-min-date="2000-01-01">
                            <span class="input-group-addon" id="tradeDateIcon">
                                <i class="fa fa-circle-o-notch fa-spin"></i>
                            </span>
                        </div>
                        <a href="javascript:;" id="setTradeDateNow">Now</a>

                        %{--<small class="text-muted">Date mask input: 00/00/0000</small>--}%
                    </div>
                    <div class="error-list" data-error-list><ul><li id="errorFortradeDate">&nbsp;</li></ul></div>
                </fieldset>
            </div>

            <div class="col-lg-12">
                <label class="form-label label-big">Transaction</label>
                <div class="checkbox-detailed">
                    <input type="radio" name="type" id="typeDeposit" value="deposit"/>
                    <label for="typeDeposit">
                    <span class="checkbox-detailed-tbl">
                        <span class="checkbox-detailed-cell">
                            <span class="checkbox-detailed-title"><i class="glyphicon glyphicon-plus"></i> Add funds</span>
                            <small class="text-muted">Deposit, mining...</small>
                        </span>
                    </span>
                    </label>
                </div>
                <div class="checkbox-detailed">
                    <input type="radio" name="type" id="typeTrade" value="trade"/>
                    <label for="typeTrade">
                    <span class="checkbox-detailed-tbl">
                        <span class="checkbox-detailed-cell">
                            <span class="checkbox-detailed-title"><i class="fa fa-handshake-o"></i> Trade</span>
                            <small class="text-muted">Exchange coins</small>
                        </span>
                    </span>
                    </label>
                </div>
                <div class="checkbox-detailed">
                    <input type="radio" name="type" id="typeWithdraw" value="withdraw"/>
                    <label for="typeWithdraw">
                    <span class="checkbox-detailed-tbl">
                        <span class="checkbox-detailed-cell">
                            <span class="checkbox-detailed-title"><i class="glyphicon glyphicon-minus"></i> Remove funds</span>
                            <small class="text-muted">Withdraw, expend ...</small>
                        </span>
                    </span>
                    </label>
                </div>
            </div>
        </div><!--.row-->


        <style type="text/css">
        #subtypeDeposit .form-group-radios .form-label, .form-group-radios .radio {
            margin-bottom: 25px;
            display: inline-block;
            margin-right: 30px;
        }
        #transactionForm .label-big {
            font-size: 1.25rem;
            margin-bottom:20px;
            /*font-weight: bold;*/
        }

        #transactionForm .error-list {
            font-size: 12.8px;
        }


        </style>


        <div class="row">
            <div class="col-lg-12" id="subtypeDeposit" style="display:none">

                <div class="form-group form-group-radios">
                    <label class="form-label label-big" style="margin-bottom: 20px;">Type</label>
                    <div style="margin-bottom: 36px;">
                        <div class="radio">
                            <input id="radioDeposit"
                                   name="depositType"
                                   type="radio"
                                   value="deposit" checked>
                            <label for="radioDeposit">Deposit</label>
                        </div>
                        <div class="radio">
                            <input id="radioMining"
                                   name="depositType"
                                   type="radio"
                                   value="mining">
                            <label for="radioMining">Mining</label>
                        </div>
                        <div class="radio">
                            <input id="radioIncome"
                                   name="depositType"
                                   type="radio"
                                   value="income">
                            <label for="radioIncome">Income</label>
                        </div>
                        <div class="radio">
                            <input id="radioGift"
                                   name="depositType"
                                   type="radio"
                                   value="income">
                            <label for="radioGift">Gift</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" id="subtypeWithdraw" style="display:none">

                <div class="form-group form-group-radios">
                    <label class="form-label label-big" style="margin-bottom: 20px;">
                       Type
                        %{--<div class="form-tooltip-error" data-error-list=""><ul><li>You must select a gender</li></ul></div>--}%
                    </label>
                    <div  style="margin-bottom: 36px;">

                        <div class="radio">
                            <input id="radioWithdraw"
                                   name="withdrawType"
                                   type="radio"
                                   value="withdraw" checked>
                            <label for="radioWithdraw">Withdraw</label>
                        </div>
                        <div class="radio">
                            <input id="radioExpend"
                                   name="withdrawType"
                                   type="radio"
                                   value="expend">
                            <label for="radioExpend">Expend</label>
                        </div>
                        <div class="radio">
                            <input id="radioLost"
                                   name="withdrawType"
                                   type="radio"
                                   value="lost">
                            <label for="radioLost">Lost</label>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--.row-->
        <div class="row">
            <div class="col-lg-12" id="inBox" style="display:none">
                <div style="float:left">
                    <label class="form-label label-big" style="margin-bottom: 0" id="buyTitle">Buy</label>

                    <fieldset class="form-group">
                        <label class="form-label" for="inAmount"><small>Amount</small></label>
                        <input id="inAmount" class="form-control"
                               style="width:180px"
                               data-inputmask="'alias': 'numeric', 'allowMinus': false, 'autoGroup': false, 'digits': 8, 'digitsOptional': false, 'placeholder': '0'" />
                        %{--<small class="text-muted">Amount</small>--}%
                        <div class="error-list" data-error-list><ul><li id="errorForinAmount">&nbsp;</li></ul></div>
                    </fieldset>
                </div>
                <div style="float:left;margin-left:10px">
                    <label class="form-label label-big" style="margin-bottom: 0">&nbsp;</label>
                    <fieldset class="form-group">
                        <label class="form-label" for="inCurrency"><small>Currency</small></label>

                        <input id="inCurrency" type="text" style="width: 70px"
                               class="form-control currencyTypeahed" placeholder="">
                        <div class="error-list" data-error-list><ul><li id="errorForinCurrency">&nbsp;</li></ul></div>
                    </fieldset>
                </div>
            </div>
        </div><!--.row-->
        <div class="row">
            <div class="col-lg-12" id="outBox" style="display:none">
                <div style="float:left">
                    <label class="form-label label-big" style="margin-bottom: 0" id="sellTitle">Sell</label>
                    <fieldset class="form-group">
                        <label class="form-label" for="outAmount"><small>Amount</small></label>
                        <input id="outAmount" class="form-control"
                               style="width:180px"
                               data-inputmask="'alias': 'numeric', 'allowMinus': false, 'autoGroup': false, 'digits': 8, 'digitsOptional': false, 'placeholder': '0'" />
                        %{--<small class="text-muted">Amount</small>--}%
                        <div class="error-list" data-error-list><ul><li id="errorForoutAmount">&nbsp;</li></ul></div>
                    </fieldset>
                </div>
                <div style="float:left;margin-left:10px">
                    <label class="form-label label-big" style="margin-bottom: 0">&nbsp;</label>
                    <fieldset class="form-group">
                        <label class="form-label" for="outCurrency"><small>Currency</small></label>

                        <input id="outCurrency" type="text" style="width: 70px"
                               class="form-control currencyTypeahed" placeholder="">
                        <div class="error-list" data-error-list><ul><li id="errorForoutCurrency">&nbsp;</li></ul></div>
                    </fieldset>
                </div>
            </div>
        </div><!--.row-->

        %{--<hr/>--}%
        <div class="row">
            <div class="col-lg-12" style="text-align: right">
                <button type="button" class="btn btn-primary btn-lg btn-rounded" id="saveButton"><i class="fa fa-circle-o-notch fa-spin"></i></button>
                %{--<button class="btn btn-default btn-lg">Cancel & end</button>--}%
                <Br/>
                <Br/>
                <div class="checkbox">
                    <input type="checkbox" id="thenAdd">
                    <label for="thenAdd" style="padding-right:8px"><small>Add another one?</small></label>
                </div>
            </div>
        </div>

    </div>
    </form>
</section>
</div>
</div>


<asset:javascript src="lib/bootstrap-select/bootstrap-select.min.js"/>
<asset:javascript src="jquery.inputmask.bundle.js"/>
<asset:javascript src="lib/blockUI/jquery.blockUI.js"/>

<asset:javascript src="lib/flatpickr/flatpickr.min.js"/>
%{--<asset:javascript src="lib/typeahead/jquery.typeahead.min.js"/>--}%
%{--<asset:javascript src="lib/select2/select2.full.min.js"/>--}%
<asset:javascript src="typeahead.bundle.js"/>
<asset:javascript src="handlebars.js"/>

<asset:javascript src="application.js"/>
<asset:javascript src="lib/moment/moment.min.js"/>


<style>
.typeahead,
.tt-query,
.tt-hint {
  /*width: 396px;*/
  /*height: 30px;*/
  /*padding: 8px 12px;*/
  /*font-size: 16px;*/
  /*line-height: 30px;*/
  /*border: 2px solid #ccc;*/
  /*-webkit-border-radius: 4px;*/
     /*-moz-border-radius: 4px;*/
          /*border-radius: 4px;*/
  /*outline: none;*/
}

.typeahead {
  /*background-color: #fff;*/
}

.typeahead:focus {
  /*border: 2px solid #0097cf;*/
}

.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999!important;
}

.tt-menu {
  width: 422px;
  margin: 6px 0;
  padding: 2px 0;
  background-color: #fff;
  /*border: 1px solid #ccc;*/
  /*border: 1px solid rgba(0, 0, 0, 0.2);*/
  border:1px solid #d8e2e7;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
/*
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.1);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.1);
          box-shadow: 0 5px 10px rgba(0,0,0,.1);
*/
}

.tt-suggestion {
  padding: 3px 12px;
  /*font-size: 16px;*/
  font-size: .9375rem;
  /*line-height: 24px;*/
  line-height: 1.42857143;
    color:#343434;
}

.tt-suggestion:hover {
  cursor: pointer;
  color: #fff;
  background-color: #0097cf;
}

.tt-suggestion.tt-cursor {
  color: #fff;
  background-color: #0097cf;

}

.tt-suggestion p {
  margin: 0;
}

.blockUI.blockOverlay{-webkit-border-radius:.25rem;border-radius:.25rem}.block-msg-default,.block-msg-message-loader{border:none!important;background-color:transparent!important}.block-msg-default .fa{font-size:42px;color:#fff;display:inline-block;width:42px;height:40px}.block-msg-default h6{margin-top:8px;color:#fff;font-size:17px;line-height:24px}.block-msg-message-loader .blockui-default-message{padding:20px;-webkit-border-radius:.25rem;border-radius:.25rem;color:#102943;font-size:17px;background:#fff}
</style>
<script type="application/javascript">

    (function() {
        var tradeDate
        var opened = false
        var blocked = false
        var typeAheadCoinsJson = ${raw(typeAheadCoinsJson)};
        var typeAheadCoins = typeAheadCoinsJson.map(a => a.name);
        var urlPost = "${createLink(controller: "trade", action: "transaction")}"


        function clearError(f, t) {
            $("#" + f).parents("fieldset").removeClass("error")
//            $("#errorFor"+f).remove()
            $("#errorFor" + f).html("&nbsp;")
            if ($("#transactionForm .error").size() == 0) {
                $("#saveButton").removeClass("disabled")
            }
        }

        function addError(f, t) {
            $("#" + f).parents("fieldset").addClass("error")
            if (t) {
                $("#errorFor" + f).html(t)
//                $("#errorFor"+f).remove()
//                $("#"+f).parents("fieldset").append($("<div id=\"errorFor"+f+"\"class=\"error-list\" data-error-list=\"\"><ul><li>"+t+"</li></ul></div>"))
            }
        }

        function clearAllErrors() {
            clearError("tradeDate")
            clearError("inAmount")
            clearError("inCurrency")
            clearError("outAmount")
            clearError("outCurrency")
        }

        function save() {
            clearAllErrors()
            var typeTradeSelected = $("#typeTrade").is(":checked");
            var typeDepositSelected = $("#typeDeposit").is(":checked");
            var typeWithdrawSelected = $("#typeWithdraw").is(":checked");

            var type = $("input[name=type]:checked").val()
            var depositType = $("input[name=depositType]:checked").val()
            var withdrawType = $("input[name=withdrawType]:checked").val()

            var inAmount = $("#inAmount").val();
            var inCurrency = $('#inCurrency').typeahead('val').toUpperCase().trim();
            var outAmount = $("#outAmount").val();
            var outCurrency = $('#outCurrency').typeahead('val').toUpperCase().trim();
            var tradeDateVal = $("#tradeDate").val();
            var error
            if (typeTradeSelected || typeDepositSelected) {
                if (!isNumber(inAmount) || parseFloat(inAmount) == 0.0) {
                    error = true
                    addError("inAmount", "Please enter an amount")
                }

                if (typeAheadCoins.indexOf(inCurrency) == -1) {
                    error = true
                    addError("inCurrency", "Wrong coin")
                } else {
                    $('#inCurrency').typeahead('val', inCurrency);
                }
            }

            if (typeTradeSelected || typeWithdrawSelected) {
                if (!isNumber(outAmount) || parseFloat(outAmount) == 0.0) {
                    error = true
                    addError("outAmount", "Please enter an amount")
                }

                if (typeAheadCoins.indexOf(outCurrency) == -1) {
                    error = true
                    addError("outCurrency", "Wrong coin")
                } else {
                    $('#outCurrency').typeahead('val', outCurrency);
                }
            }

            var tradeDateMoment = moment(tradeDateVal, "YYYY-MM-DD HH:mm") // Formato del usuario
            if (!tradeDateMoment.isValid() || !/^[\d -:]+$/.test(tradeDateVal)) {
                error = true
                addError("tradeDate", "Please enter a valid date")
            }

            if (error) {
                $("#saveButton").addClass("disabled")
            } else {
                if (typeDepositSelected) {
                    type = depositType
                } else if (typeWithdrawSelected) {
                    type = withdrawType
                } else {
                    type = "trade";
                }
                var data = {
                    inAmount: inAmount,
                    inCurrency: inCurrency,
                    outAmount: outAmount,
                    outCurrency: outCurrency,
                    tradeDate: tradeDateMoment.format("YYYY-MM-DD HH:mm"), // Formato del command
                    type: type

                }
                block()

                function clearFields() {
                    $("#inCurrency").val("")
                    $("#outCurrency").val("")
                    $("#inAmount").val("")
                    $("#outAmount").val("")
                }

                var start = new Date().getTime()
                $.post(urlPost, data, function () {
                        var end = new Date().getTime()
                        var elapsed = end - start
                        var timeout = 1000 - elapsed
                        setTimeout(function () {
                            refreshTradeList()
                            unblock()
                            clearFields()
                            var thenAdd = $("#thenAdd").is(":checked");
                            if (!thenAdd) {
                                close()
                            }
                        }, timeout)
                }).fail(function () {
                    unblock()
                })
            }
        }

        function refreshTradeList() {
            var tradeList = $("#tableTradeList")
            if (tradeList.length > 0) {
                tradeList.bootstrapTable('refresh')
            }
        }

        function refresh() {
            var thenAdd = $("#thenAdd").is(":checked");
            var typeTradeSelected = $("#typeTrade").is(":checked");
            var typeDepositSelected = $("#typeDeposit").is(":checked");
            var typeWithdrawSelected = $("#typeWithdraw").is(":checked");

            $("#subtypeDeposit").hide()
            $("#subtypeWithdraw").hide()
            $("#inBox").hide()
            $("#outBox").hide()

            if (!typeWithdrawSelected && !typeDepositSelected && !typeTradeSelected) {
                $("#saveButton").prop("disabled", true).html("Choose a transaction type first").removeClass("btn-primary").addClass("btn-default").css("margin-top", "30px")
            } else {
                $("#saveButton").prop("disabled", false).html(thenAdd ? "Save & continue" : "Save & close").addClass("btn-primary").removeClass("btn-default").css("margin-top", "0")
            }


            if (typeWithdrawSelected || typeTradeSelected) {
                $("#outBox").show()
            }
            if (typeDepositSelected || typeTradeSelected) {
                $("#inBox").show()
            }
            if (typeDepositSelected) {
                $("#subtypeDeposit").show()
                $("#buyTitle").html("Add funds")
            } else if (typeWithdrawSelected) {
                $("#subtypeWithdraw").show()
                $("#sellTitle").html("Remove funds")
            } else {
                $("#buyTitle").html("Buy")
                $("#sellTitle").html("Sell")
            }

            var visible = $("#tradeBox").is(":visible")
            console.log("visible:"+visible, "opened:"+opened)
            if (opened) {
                if (!visible) {
                    $("#tradeBox").modal({keyboard: false, backdrop: 'static', show: true})
                }
            } else {
                if (visible) {
                    $("#tradeBox").modal('hide')
                }
            }
        }

        function close() {
            opened = false
            refresh()
        }

        function open() {
            opened = true
            $('.dropdown.show').removeClass('show'); // Cerramos todos los dropdowns...
            refresh()
        }

        function unblock() {
            blocked = false
            $('#tradeBox section').unblock();
//            $.unblockUI();
        }

        function block() {
            blocked = true
/*
            $.blockUI({
                message: '<div class="blockui-default-message"><i class="fa fa-circle-o-notch fa-spin"></i><h6>Adding a new transaction.<br/>Calculating holdings related.</h6></div>',
                overlayCSS: {
                    background: 'rgba(142, 159, 167, 0.8)',
                    opacity: 1,
                    cursor: 'wait',
                    baseZ: 999999,
                },
                css: {
                    width: '20%'
                },
                blockMsgClass: 'block-msg-default'
            });
*/
            $('#tradeBox section').block({
                message: '<div class="blockui-default-message"><i class="fa fa-circle-o-notch fa-spin"></i><h6>Adding a new transaction.<br/>Calculating holdings related.</h6></div>',
                overlayCSS: {
                    background: 'rgba(142, 159, 167, 0.8)',
                    opacity: 1,
                    cursor: 'wait',
                    baseZ: 999999,
                },
                css: {
                    width: '20%'
                },
                blockMsgClass: 'block-msg-default'
            });
        }

        $(document).ready(function () {

            document.onkeyup = function (e) {
                var e = e || window.event; // for IE to cover IEs window event-object
                var inputFocused = $(":focus").attr("id")
//            console.log(e)
                if (e.which == 27) {
                    if (opened && !blocked) {
                        close();
                        e.preventDefault()
                        return false;
                    }
                }

                if (inputFocused == "inAmount") {
                    clearError("inAmount")
                } else if (inputFocused == "outAmount") {
                    clearError("outAmount")
                }
            }

            $("#tradeBox").on("hidden.bs.modal", function () {
                opened = false
            });

            $("#saveButton").click(save);

            $("#transactionForm :input").inputmask();

            $("#thenAdd").change(function (e) {
                refresh();
            });

            $("#addTransaction").click(function (e) {
                e.stopPropagation();
                if (!opened) {
                    open();
                } else {
                    close()
                }
            });

            $("#addTrade").click(function () {
                $("#typeTrade").prop("checked", true)
                open();
            });

            $("#addDeposit").click(function () {
                $("#typeDeposit").prop("checked", true)
                open();
            });

            $("#addWithdraw").click(function () {
                $("#typeWithdraw").prop("checked", true)
                refresh()
                clearAllErrors();
                open();
            });

            $("#closeButton").click(function () {
                clearAllErrors();
                close()
            });

            $("#typeDeposit").click(function () {
                clearAllErrors();
                refresh()
            })

            $("#typeWithdraw").click(function () {
                clearAllErrors();
                refresh()
            })

            $("#typeTrade").click(function () {
                clearAllErrors();
                refresh()
            })

            $("#inAmount").change(function () {
                clearError("inAmount");
            });

            $("#outAmount").change(function () {
                clearError("outAmount");
            });

            $("#tradeDate").focus(function () {
                clearError("tradeDate")
            })

            $("#setTradeDateNow").click(function (e) {
                $("#tradeDate").val(moment().format("YYYY-MM-DD HH:mm"));
                clearError("tradeDate");
            });

            $("#inCurrency").focus(function () {
                tradeDate.close()
            })
            $("#outCurrency").focus(function () {
                tradeDate.close()
            })

            var bloodhoundCoins = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace("tokens"),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                identify: function (obj) {
                    return obj.name;
                },
                // TODO: prefetch y storage en local
                local: typeAheadCoinsJson
            });

            $('.currencyTypeahed').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 0
                },
                {
                    display: 'name',
                    name: 'coins',
                    limit: 5,
                    source: bloodhoundCoins,
                    templates: {
                        suggestion: Handlebars.compile('<div><img src="{{img}}" width="16"/>&nbsp;&nbsp;{{displayName}} <span style="color:#6bafd9">({{name}})</span></div>')
                    }
                });

            $('#inCurrency').bind('typeahead:active', function (ev, suggestion) {
                clearError("inCurrency")
            });

            $('#outCurrency').bind('typeahead:active', function (ev, suggestion) {
                clearError("outCurrency")
            });


            /*
        $.typeahead({
            input: "#inCurrency",
            order: "asc",
            highlight: true,
            searchOnFocus: true,
            minLength: 0,
            hint: true,
            source: {
                data: typeAheadCoinsJson
            }
        });

        $.typeahead({
            input: "#outCurrency",
            order: "asc",
            highlight: true,
            searchOnFocus: true,
            minLength: 0,
            hint: true,
            source: {
                data: typeAheadCoinsJson
            }
        });
*/

            $("#tradeDateIcon").click(function () {
                clearError("tradeDate")
                setTimeout(function () {
                    tradeDate.open()
                }, 10);
            }).html("<i class=\"font-icon font-icon-calend\"></i>")

            tradeDate = $("#tradeDate").flatpickr();
            tradeDate.input.focus();

//        setTimeout(function() { $("#tradeDate").focus() }, 300);
            refresh();


        });
    })()
</script>



