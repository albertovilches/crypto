<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Bloqueado</title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4">

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Usuario bloqueado</h3>
                </div>

                <div class="panel-body">
                    <p>Puedes probar a
                        <g:link controller="logout">iniciar sesión con otro usuario.</g:link>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>
