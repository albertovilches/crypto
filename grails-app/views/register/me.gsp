<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Sign In</title>
</head>

<body>
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <g:form name="submitForm" class="sign-box" method="post" action="checkAndSend">
                <input type="hidden" name="nextUrl" value="${params.nextUrl}"/>

                <div class="sign-avatar">
                    <div class="sign-avatar no-photo">&plus;</div>
                </div>

                <header class="sign-title">Sign Up</header>

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="E-Mail" name="email" required/>
                </div>

                <button type="submit" class="btn btn-rounded btn-success sign-up">Create account</button>

                <p class="sign-note">Already have an account? <g:link controller="sign" action="in">Sign in</g:link></p>
                <!--<button type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            %{--<button type="button" class="close">--}%
            %{--<span aria-hidden="true">&times;</span>--}%
            %{--</button>--}%

                <div style="height: 50px">
                    <g:if test="${flash.error}">
                        <div class="alert alert-danger alert-fill alert-close alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            ${flash.error}
                        </div>
                    </g:if>
                </div>
            </g:form>
        </div>
    </div>
</div><!--.page-center-->


<script>

    $(document).ready(function () {
        $('#n').focus()
    });
</script>
</body>
</html>