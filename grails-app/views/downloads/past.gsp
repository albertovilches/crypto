<%@ page import="mamespin.FileDownload; tools.StringTools; tools.UAgent" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>
    %{--<asset:javascript src="clipboard.js"/>--}%
    <script src="https://cdn.jsdelivr.net/clipboard.js/1.5.16/clipboard.min.js"></script>

    <script type="application/javascript">
        var th
        function copy(x) {
            $("#paster").attr("data-clipboard-text", x)
            var cp = new Clipboard("#paster");
            $("#paster").click();
            cp.destroy();
        }

        function showTooltip(id, t) {
            $(id).tooltip({trigger: 'manual', placement: "bottom", container: 'body'}).attr('data-original-title', t)
            $(id).tooltip("show");
            setTimeout(function () {
                $(id).tooltip("hide");
            }, 1200)

        }
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip()
            var clipboard = new Clipboard('.copier');
            $('#questSlots').tooltip({
                title: "Número de ficheros que te puedes descargar a la vez.!",
                container: "body"
            });
        });
        var showingLinks = false
        function toggleLinks(force) {
            showingLinks = force == true || !showingLinks
            if (showingLinks) {
                $('#links').slideDown()
                $('.singleLink').slideDown()
            } else {
                $('#links').slideUp()
                $('.singleLink').slideUp()
            }
        }
        function downloadAgain(id) {
            $("#li_" + id).html('<i class="fa fa-spinner fa-spin text-success fa-fw" aria-hidden="true"></i> Generando nuevo enlace ...')
            setTimeout(function () {
                $.ajax(
                    {
                        url: "${g.createLink(controller:'downloads', action:'fileagain')}",
                        dataType: "html",
                        data: {id: id}
                    }).error(function (data) {
                }).done(function (html) {
                    $("#li_" + id).html(html)
                });
            }, 1200)
        }
        function refreshFileDownload(id) {
            $("#li_" + id).html('<i class="fa fa-spinner fa-spin text-success fa-fw" aria-hidden="true"></i> Actualizando estado ...')
            setTimeout(function () {
                $.ajax(
                    {
                        url: "${g.createLink(controller:'downloads', action:'refreshFileDownload')}",
                        dataType: "html",
                        data: {id: id}
                    }).error(function (data) {
                }).done(function (html) {
                    $("#li_" + id).html(html)
                });
            }, 15000)
        }
    </script>
</head>

<body>
<button id="paster" style="display: none"></button>

<div class="container">
    <div class="panel">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default"
                     style="margin-top:10px;margin-bottom:-20px;border:0;background:transparent">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <span class="navbar-brand info-header" style="margin-left:0;margin-right:8px"><span id="finishedCount"
                                                                         class="info-number">${finished.size()}</span> descargas terminadas
                            </span>
                            <button class="navbar-toggle" type="button" data-toggle="collapse"
                                    data-target="#navbar-sub" style="border-color: black;">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="navbar-sub">
                            <ul class="nav navbar-nav">
                                %{--
                                                            <li class="dropdown">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                                   aria-expanded="false">Acciones <span class="caret"></span></a>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="#"><i class="fa fa-trash fa-fw" aria-hidden="true"></i> Borrar todos</a></li>
                                                                    <li><a href="#">Another action</a></li>
                                                                    <li><a href="#">Something else here</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#">Separated link</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#">One more separated link</a></li>
                                                                </ul>
                                                            </li>
                                --}%

                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><g:link action="index" style="color:#7bbfe9">Descargas activas &nbsp;<span class="badge">${downloads}</span></g:link></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                 <hr/>
                <ul class="list-group" style="padding:0px 15px 0">
                    <g:if test="${!finished}">
                        <div class="text-center panel panel-default text-primary"
                             style="padding:15px;margin:0 100px">No tienes ninguna terminada todavía. <g:link
                                controller="downloads">Ir a descargas incompletas</g:link>.</div>
                    </g:if>
                    <g:each in="${finished}" var="${req}">
                        <g:set var="selected" value="${req.uuid == params.id}"/>

                        <li class="list-group-item ${selected?"active":""}" style="${selected?"border-left: 10px solid #000;":""}">

                            ${req.name} - ${req.humanSize} &raquo; <g:link controller="list" action="resource"
                                                                           params="[id: req.uuid]">Ver detalles</g:link><br/>
                            <span class="small text-primary">Créditos: ${req.humanCredits} - ${req.finished}
                            &raquo; <g:link action="repiteall"
                                            params="[id: req.id]">Descargar otra vez</g:link></span>
                            %{--${req.url}--}%
                            %{--<i class="fa fa-download" aria-hidden="true"></i> <a href="${req.url}">${req.url}</a>--}%
                            %{--
                               <div class="col-lg-2 text-right">
                                   <a id="q_${req.id}" style="z-index:99; position: absolute; right:5px;top:-7px;display:none" id="question_${req.id}" href=""><i class="fa fa-question-circle" aria-hidden="true"></i></a>
                                   <p><a id="link_${req.id}" class="btn icon-btn btn-success" href="http://mamespin-download.com:7070/download?token=${req.token}"><i id="icon_${req.id}" style="width:30px" class="btn-glyphicon fa fa-fw fa-download"></i> <span id="text_${req.id}" class="small"> ${req.humanSize}</span></a></p>
                                   <span class="coolr text-center">
                                       <label style="font-size: 1.2em;"><input type="checkbox" onclick="" value=""><span  data-toggle="tooltip" data-placement="left" data-original-title="Click para añadir a descargas" class="cr lg" style="margin:0"><i class="cr-icon fa fa-download"></i></span></label>
                                   </span>
                               </div>
                            --}%
                        </li>
                    </g:each>
                </ul>

            </div>
        </div>
    </div>
</div>

%{--
    <div class="tab-pane fade in panel" id="jdownloader" style="padding:15px;background-color: #3e444c">

        <h4>Instrucciones de descarga</h4>

        <ol>
            <li>Asegúrate de que tienes <a href="http://www.jdownloader.org/download/index" target="_blank">JDownloader</a> arrancado y configurado para bajar como máximo <strong style="font-size: 2em;padding:3px" class="text-info">2</strong> descargas simultáneas.</li>
            <li>Haz click en el botón copiar para guardar todos los enlaces en portapapeles:
                <button class="btn btn-primary btn-sm copier" data-clipboard-target="#jdTA"><i class="fa fa-clipboard" aria-hidden="true"></i> Copiar</button></li>
            <li>Al copiar los enlaces JDownloader iniciará la descarga automáticamente.</li>
            <li>Para más información sobre como descargar y configurar JDownloader haz clizk aquí: <a target="_blank" href="">tutorial JDownloader</a>.</li>
        </ol>
<pre id="jdTA" readonly="readonly" style="width: 100%;height: 200px;margin:auto">
<g:each in="${unlocked}" var="${req}">http://mamespin-download.com:7070/download?token=${req.token}
</g:each></pre>


    </div>

</div>
--}%

</body>
</html>












