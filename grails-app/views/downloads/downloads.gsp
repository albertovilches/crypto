<%@ page import="mamespin.FileDownload; tools.StringTools; tools.UAgent" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>
    %{--<asset:javascript src="clipboard.js"/>--}%
    <script src="https://cdn.jsdelivr.net/clipboard.js/1.5.16/clipboard.min.js"></script>
    <style type="text/css">
    .blink_me {
        animation: blinker 1s cubic-bezier(.5, 0, 1, 1) infinite alternate;
    }

    .blink_me:hover {
        animation: none;
    }

    @keyframes blinker {
        to {
            opacity: 0.5;
        }
    }
    </style>

    <script type="application/javascript">
        var th
        function copy(x) {
            $("#paster").attr("data-clipboard-text", x)
            var cp = new Clipboard("#paster");
            $("#paster").click();
            cp.destroy();
        }

        function showTooltip(id, t) {
            $(id).tooltip({trigger: 'manual', placement: "bottom", container: 'body'}).attr('data-original-title', t)
            $(id).tooltip("show");
            setTimeout(function () {
                $(id).tooltip("hide");
            }, 8000)

        }
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip()
            var clipboard = new Clipboard('.copier');
            $('#questSlots').tooltip({
                title: "Número de ficheros que te puedes descargar a la vez.!",
                container: "body"
            });
        });
        function downloadAgain(id) {
            $("#li_" + id).html('<i class="fa fa-spinner fa-spin text-success fa-fw" aria-hidden="true"></i> Generando nuevo enlace ...')
            setTimeout(function () {
                $.ajax(
                    {
                        url: "${g.createLink(controller:'downloads', action:'fileagain')}",
                        dataType: "html",
                        data: {id: id}
                    }).error(function (data) {
                }).done(function (html) {
                    $("#li_" + id).html(html)
                });
            }, 1200)
        }
        function refreshFileDownload(id) {
            $("#li_" + id).html('<i class="fa fa-spinner fa-spin text-success fa-fw" aria-hidden="true"></i> Actualizando estado ...')
            setTimeout(function () {
                $.ajax(
                    {
                        url: "${g.createLink(controller:'downloads', action:'refreshFileDownload')}",
                        dataType: "html",
                        data: {id: id}
                    }).error(function (data) {
                }).done(function (html) {
                    $("#li_" + id).html(html)
                });
            }, 15000)
        }
        function cNload(v) {
            console.log("aaa")
//            $("#cNloadSource").val(location.href);
//            $("#cNloadUrls").val("http://mamespin-download.com:7070/download/3eciyrmtrg5pznqd1lm1k12jq54wnxnj4c1r5pi3xo5fpfcn");
            $("#cNloadForm").submit()
            return
            var vals = v.split("\n")
            for (var x = 0; x < vals.length; x++) {
                console.log(vals[x])
                setTimeout(function() {
                    $("#cNloadUrls").val(vals[x]);
                    $("#cNSubmit").click();
                }, (100*x)+50);
            }

        }
    </script>
</head>

<body>
<button id="paster" style="display: none"></button>
%{--<form id="cNloadForm" action="http://127.0.0.1:9666/flash/add" target="hidden" method="post" style="display:none">--}%
   %{--<input type="hidden" id="cNloadSource" name="source" value="">--}%
   %{--<input type="hidden" id="cNloadUrls" name="urls" value="">--}%
    %{--<INPUT TYPE="SUBMIT" id="cNSubmit" NAME="submit" VALUE="Add Link to JDownloader">--}%
%{--</form>--}%
%{--
<FORM id="cNloadForm" ACTION="http://127.0.0.1:9666/flash/add" target="hidden" METHOD="POST">
   <INPUT TYPE="hidden" id="cNloadUrls" NAME="urls" VALUE="http://www.rapidshare.com/files/407970280/RapidShareManager2WindowsSetup.exe">
   <INPUT  id="cNSubmit" TYPE="SUBMIT" NAME="submit" VALUE="Add Link to JDownloader">
</FORM>
--}%


<div class="container">
    <div class="panel">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar navbar-default"
                     style="margin-top:10px;margin-bottom:-20px;border:0;background:transparent">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <span class="navbar-brand info-header" style="margin-left:0;margin-right:8px"><span id="unlockedCount" class="info-number">${active.size()}</span> descargas
                            </span>
                            <button class="navbar-toggle" type="button" data-toggle="collapse"
                                    data-target="#navbar-sub" style="border-color: black;">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="navbar-sub">
                            <ul class="nav navbar-nav">
                                %{--
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">Acciones <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#"><i class="fa fa-trash fa-fw" aria-hidden="true"></i> Borrar todos</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                                --}%

                                <li style="margin-right: 10px; padding-top:9px">
                                    <span id="speed"
                                          style="color:${downloadingCount >= currentUser.slots ? "red" : "white"}"
                                          class="info-slots">${currentUser.limitByLevelString}</span> <!-- level: ${currentUser.level}, kb limit: ${currentUser.kbLimitByLevel}-->
                                </li>
                                <li style="margin-right: 10px; padding-top:9px;">
                                    <span id="slotCount"
                                          style="color:${downloadingCount >= currentUser.slots ? "red" : "white"}"
                                          class="info-slots">${downloadingCount}/${currentUser.slots}</span> slots ocupados
                                </li>
                                <li><g:link controller="info" action="slots" style="color:#7bbfe9">Ampliar <i class="fa fa-level-up fa-fw"
                                                                                   aria-hidden="true"></i></g:link>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><g:link action="past" style="color:#7bbfe9">Descargas terminadas &nbsp;<span class="badge">${terminatedCount}</span></g:link></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <hr/>
                <g:if test="${!active}">
                    <ul class="list-group" style="padding:0px 15px 0">
                            <div class="text-center panel panel-default text-primary"
                                 style="padding:15px;margin:0 100px">No tienes ninguna descarga sin empezar. <g:link
                                    controller="list">Añadir descargas</g:link>.
                            </div>
                    </ul>
                </g:if>
                <g:else>

                <div class="row">
                    <div class="col-lg-4 col-md-4 text-center" id="jdNot" style="display: block">
                        <p class="well" style="width:94%;margin:0px 25px 10px 25px">
                            <span class=" text-danger"><i class="fa fa-exclamation-circle fa-3" aria-hidden="true"></i>
                                No tienes JDownloader arrancado. Arráncalo y refresca esta página: </span> <a href="javascript:void(location.reload())"><i class="fa fa-refresh fa-3" aria-hidden="true"></i></a><br/><br/>
                            Descarga: <a href="http://jdownloader.org/download/index" class="">JDownloader</a> <i class="fa fa-external-link" aria-hidden="true"></i><br/><br/><g:link controller="info" action="download">Ayuda</g:link></p>

                    </div>
                    <div class="col-lg-4 col-md-4 text-center" id="jdYes"  style="display: none">
                        <p class="well" style="width:94%;margin:0px 25px 10px 25px"><i class="fa fa-info-circle fa-3" aria-hidden="true"></i>
                            <span class="text-success">JDownloader detectado.</span><br/><br/>
                            Asegúrate de que tu JDownloader tiene configurado el mismo número de descargas simultáneas que slots. <g:link controller="info" action="download">Ayuda</g:link>.</p>

                        %{--<a id="c2l" style="margn:10px 25px 10px 25px" class="btn btn-primary"
                           href="javascript:void(cNload($('#jdTA').val()))">Enviar Click'n Load
                            <g:img file="icon/jd.png" height="20"/></a>--}%
                        <a id="copyAll2" style="width:93%; margin:10px 25px 10px 25px" class="btn btn-primary" href="javascript:void(copy($('#jdTA').val()),$('#jdTA').select(),showTooltip('#copyAll2', '¡Enlaces copiados! Ahora pégalos en tu JDownloader e inicia la descarga'))"><i class="fa fa-clipboard" aria-hidden="true"></i> Copiar enlaces</a>
                    </div>
                    <div class="col-lg-8 col-md-8 text-center">
                    <textarea id="jdTA" readonly="readonly" class="singleLink" wrap="soft"
                              style="display: block; width:95%;height: 194px;margin:0 0 0 0; white-space: pre; overflow: auto">${allLinks}</textarea>
                    </div>
                </div>

                    <script language="javascript">
                       var jdownloader = false;
                    </script>
                    <script language="javascript" src="http://127.0.0.1:9666/jdcheck.js"></script>
                    <script language="javascript">
                       if (jdownloader){
                          $("#jdNot").hide();
                          $("#jdYes").show();
                          $("#jdTA").show();
                       }
                    </script>

                <ul class="list-group" style="padding:40px 15px 0">
                    <g:each in="${active}" var="${req}" idx="i">
                        <li class="list-group-item">
                            <p style="float:right"></p>
                            <table style="background-color: transparent;width:100%"><tr>
                                %{--
                                                                        <td class="text-nowrap"><a class="btn icon-btn btn-success" style="width:110px" href="${req.url}"
                                                                               target="_blank">
                                                                            <i style="width:30px"
                                                                               class="btn-glyphicon fa fa-download fa-fw"></i> ${req.humanSize}</a></td>
                                                                        <td style="padding-left: 10px">
                                                                            <a class="btn icon-btn btn-success" id="copy_${i}" style="width:90px"
                                                                               data-toggle="tooltip"
                                                                               href="javascript:void(copy('${req.url}'),showTooltip('#copy_${i}', '¡Enlace copiado!'))">
                                                                                <g:img file="icon/jd.png"/>Copiar</a>

                                                                        </td>
                                --}%
                                <td class="text-nowrap"
                                    style="padding-left: 10px;width:100%">${req.name} - ${req.humanSize}</span> &raquo; <g:link
                                        controller="list" action="resource" params="[id: req.uuid]">Ver detalles</g:link><br/>
                                    <span class="small text-primary">Créditos: ${req.humanCredits} - ${req.dateCreated}</span>
                                    <g:if test="${req.revocable}">
                                        <g:link action="revoke" class="small text-danger" data-toggle="tooltip"
                                                data-placement="right"
                                                data-original-title="Devolución: ${req.humanCredits}"
                                                params="[id: req.id]"><i
                                                class="fa fa-trash" aria-hidden="true"></i> cancelar</g:link>
                                    </g:if>
                                    <g:else>
                                        <span class="small"></span>
                                    </g:else>
                                    <ol><g:each in="${req.links}" var="link">
                                        <li id="li_${link.id}">
                                            <g:render template="downloadActiveLink" model="[link: link]"/>
                                        </li>
                                    </g:each>
                                    </ol>
                                </td>
                                <td class="text-right" style="width:100%">

                                    %{--
                                        <g:if test="${req.dupes}">
                                            <br/><span class="small">Descargado ${req.dupes} veces.</span>
                                        </g:if>
                                        <g:if test="${req.aborts}">
                                            <br/><span class="small text-danger">La descarga falló ${req.aborts} veces.</span>
                                        </g:if>
                                     --}%
                                </td>
                            </tr></table>
                            %{--${req.url}--}%
                            %{--<i class="fa fa-download" aria-hidden="true"></i> <a href="${req.url}">${req.url}</a>--}%
                            %{--
                               <div class="col-lg-2 text-right">
                                   <a id="q_${req.id}" style="z-index:99; position: absolute; right:5px;top:-7px;display:none" id="question_${req.id}" href=""><i class="fa fa-question-circle" aria-hidden="true"></i></a>
                                   <p><a id="link_${req.id}" class="btn icon-btn btn-success" href="http://mamespin-download.com:7070/download?token=${req.token}"><i id="icon_${req.id}" style="width:30px" class="btn-glyphicon fa fa-fw fa-download"></i> <span id="text_${req.id}" class="small"> ${req.humanSize}</span></a></p>
                                   <span class="coolr text-center">
                                       <label style="font-size: 1.2em;"><input type="checkbox" onclick="" value=""><span  data-toggle="tooltip" data-placement="left" data-original-title="Click para añadir a descargas" class="cr lg" style="margin:0"><i class="cr-icon fa fa-download"></i></span></label>
                                   </span>
                               </div>
                            --}%
                        </li>
                    </g:each>
                </ul>
                </g:else>
            </div>
        </div>
    </div>

</div>

%{--
    <div class="tab-pane fade in panel" id="jdownloader" style="padding:15px;background-color: #3e444c">

        <h4>Instrucciones de descarga</h4>

        <ol>
            <li>Asegúrate de que tienes <a href="http://www.jdownloader.org/download/index" target="_blank">JDownloader</a> arrancado y configurado para bajar como máximo <strong style="font-size: 2em;padding:3px" class="text-info">2</strong> descargas simultáneas.</li>
            <li>Haz click en el botón copiar para guardar todos los enlaces en portapapeles:
                <button class="btn btn-primary btn-sm copier" data-clipboard-target="#jdTA"><i class="fa fa-clipboard" aria-hidden="true"></i> Copiar</button></li>
            <li>Al copiar los enlaces JDownloader iniciará la descarga automáticamente.</li>
            <li>Para más información sobre como descargar y configurar JDownloader haz clizk aquí: <a target="_blank" href="">tutorial JDownloader</a>.</li>
        </ol>
<pre id="jdTA" readonly="readonly" style="width: 100%;height: 200px;margin:auto">
<g:each in="${unlocked}" var="${req}">http://mamespin-download.com:7070/download?token=${req.token}
</g:each></pre>


    </div>

</div>
--}%

</body>
</html>
