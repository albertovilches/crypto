<g:if test="${link.limit}">
    <span class="label label-info" style="color:white;background-color:transparent;font-weight: bold">${link.limit}</span>
</g:if>
<g:if test="${link.state == mamespin.FileDownload.State.finished}">
    <i class="fa fa-check text-success  fa-fw" aria-hidden="true"></i>
        ${link.name} - ${link.humanSize} - <span class="small text-primary">${link.dateDownloaded} &raquo; <a href="javascript:void(downloadAgain(${link.id}))">Generar nuevo enlace</a> </span>

</g:if>
<g:elseif test="${link.state == mamespin.FileDownload.State.unlocked}">
    <a target="_blank" href="${link.url}?info" onclick="refreshFileDownload(${link.id}); return true"><i class="fa fa-download fa-fw" aria-hidden="true"></i>
${link.name}</a> - ${link.humanSize}
</g:elseif>
<g:elseif test="${link.state == mamespin.FileDownload.State.download}">
    <i class="fa fa-spinner fa-spin text-success fa-fw" aria-hidden="true"></i><span class="small" style="color:white"> ${link.downloadPercent}%</span>
    <span class="blink_me">${link.name}</span>  - ${link.humanSize}

    <div class="progress progress-striped active" style="height:5px; margin-bottom:5px;width:50%">
      <div class="progress-bar progress-bar-success" id="currentDownloadBar" style="width:${link.downloadPercent}%"></div>
        <div class="small text-info" id="currentDownloadText" style="padding: 1px 10px;text-shadow: 1px 1px  #000;position:absolute;color:white;"></div>
    </div>
</g:elseif>
