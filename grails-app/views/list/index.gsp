<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>
    <script type="application/javascript">
        $(document ).ready(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        function clickResource(uuid) {
            var l = $("#link_"+uuid)
            if (l.hasClass("state-short")) {
                location.href = "${g.createLink(controller:'credit')}"
                return
            } else if (l.hasClass("state-finished")) {
                location.href = "${g.createLink(controller:'downloads', action : 'past')}/"+uuid;
                return
            } else if (l.hasClass("state-unlocked")) {
                location.href = "${g.createLink(controller:'downloads')}"
                return
            }

            icon(uuid, "fa-spinner fa-spin")
            link(uuid, "btn-success disabled")
            $.ajax(
                {url: "${g.createLink(controller:'list', action:'unlockResource')}",
                    dataType: "json",
                    data: {uuid: uuid}
                }).error(function(data) {
                    setTimeout(function(){
                        setDisabledStyle(uuid)
                        l.tooltip({placement:"left", trigger:'hover focus', container: 'body'}).attr('data-original-title', "Error al desbloquear, intentalo más tarde.")
                        l.tooltip("show")
                    }, 1000);

                }).done(function(data) {
                    setTimeout(function(){
                        console.log(data)
                        var state, downloads, credits
                        if (data) {
                            if ('state' in data) {
                                state = data.state
                            }
                            if ('downloads' in data && !isNaN(data.downloads)) {
                                downloads = data.downloads
                            }
                            if ('credits' in data) {
                                credits = data.credits
                            }
                        }
                        setState(uuid, state)
                        if (state == "unlocked") {
                            showDownloadsRemimder()
                        }
                        if (credits != null) {
                            $('#userCredits').html(credits)
                        }
                        if (downloads != null) {
                            $('#downloads').html(downloads)
                        }
                    }, 1000);
                });
        }

        var step1done = ${currentUser.step1}
        function showDownloadsRemimder(force) {
            if (step1done && force != true) return;
            step1done = true;
            $('#downloads').popover({
                container:'#navbar-main',
                trigger:'focus',
                title:'<button type="button" class="close" onclick="closeDownloadPopover(false)">&times;</button><strong>Nuevo elemento añadido<strong>',
                html : true,
                content:function() { return $("#downloads-popover").html() },
                placement:'bottom'}).popover('show').on("hidden.bs.popover", function(e) {});
        }

        function closeDownloadPopover(forever) {
            $('#downloads').popover('hide');
            if (forever) {
                $.ajax({
                    url: "${g.createLink(controller:'list', action:'tutorialDone')}",
                    dataType: "json",
                    data: {"step": "1"}
                })
            }
        }


        function icon(uuid, clazz) {
            var i = $("#icon_"+uuid).removeClass().addClass("btn-glyphicon fa fa-fw "+clazz).css("text-shadow", "none"); return i;
        }

        function link(uuid, clazz) {
            var l = $("#link_"+uuid).removeClass().addClass("btn icon-btn "+clazz); return l;
        }

        function question(uuid, text, clazz) {
            var l = $("#q_"+uuid)
            if (text) {
                l.css("display", "inline").tooltip({placement:"bottom", trigger:'hover focus', container: 'body'}).attr('data-original-title', text).removeClass()
                if (clazz) l.addClass(clazz)
            } else {
                l.css("display", "none");
            }
            return l
        }

        function clickQuestion(uuid) {
            var l = $("#link_"+uuid)
            if (l.hasClass("state-short")) {
                location.href = "${g.createLink(controller:'credit')}"
            } else if (l.hasClass("state-finished")) {
                location.href = "${g.createLink(controller:'downloads', action : 'past')}"
            } else if (l.hasClass("state-unlocked")) {
                location.href = "${g.createLink(controller:'downloads')}"
            }
        }

        function setState(uuid, state) {
            console.log("setting state "+state)
            if (state == "locked") {
                setLockedStyle(uuid)
            } else if (state == "short") {
                setShortStyle(uuid)
            } else if (state == "unlocked") {
                setUnlockedStyle(uuid)
            } else if (state == "download") {
                setDownloadingStyle(uuid)
            } else if (state == "finished") {
                setFinishedStyle(uuid)
            } else {
                setUnknownStyle(uuid)
            }
        }

        function setDisabledStyle(uuid) {
            icon(uuid, "fa-ban")
            link(uuid, "btn-default disabled state-disabled")
            $("#tx_"+uuid).html("No disponible")
            question(uuid)
        }

        function setUnknownStyle(uuid) {
            icon(uuid, "fa-spinner fa-pulse")
            link(uuid, "btn-default disabled state-unknown")
            $("#tx_"+uuid).html("Procesando...")
            question(uuid)
        }

        function setLockedStyle(uuid) {
            icon(uuid, "fa-plus")
            link(uuid, "btn-success state-locked")
            $("#tx_"+uuid).html("Añadir enlaces")
            question(uuid)
        }

        function setFinishedStyle(uuid) {
            icon(uuid, "fa-check")
            link(uuid, "btn-success state-finished")
            $("#tx_"+uuid).html("Completado")
            question(uuid)
        }

        function setShortStyle(uuid) {
            icon(uuid, "fa-lock")
            link(uuid, "btn-danger state-short")
            $("#tx_"+uuid).html("Sin crédito")
            question(uuid, "No tienes créditos suficiente para desbloquear este elemento. Click para conseguir más créditos.", "text-danger")
        }

        function setUnlockedStyle(uuid) {
            icon(uuid, "fa-download")
            link(uuid, "btn-primary state-unlocked")
            $("#tx_"+uuid).html("Preparado")
            question(uuid, "Ya puedes descargar este archivo desde Mis descargas", "text-primary")
        }

        function setDownloadingStyle(uuid) {
            icon(uuid, "fa-spin fa-refresh")
            link(uuid, "btn-primary disabled state-downloading")
            $("#tx_"+uuid).html("Descargando")
            question(uuid)
        }

    </script>
</head>
<body>

<div style="display: none" id="downloads-popover">
    <p class="small text-primary">Desde aquí podrás iniciar la descarga de este y de todos los futuros elementos que añadas a tu cola.</p>
    <p class="text-right"><input type="checkbox" onclick="closeDownloadPopover(true)">&nbsp;No volver a recordar</p>
</div>

${raw(renderList)}

<script type="application/javascript">
    $(document).ready(function () {
        <g:each in="${reqStates}" var="req">
        setState('${req.uuid}', '${req.state}')
        </g:each>

        <g:each in="${0..(currentUser.level)}" var="l">
        $(".level_${l}").hide()
        </g:each>
    })
</script>
</body>
</html>
