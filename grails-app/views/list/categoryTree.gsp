<div class="list-group">
    %{--<h4 class="list-group-item" style="background:#252525;margin-top:0">Inicio</h4>--}%
    <g:link elementId="category_" class="list-group-item" style="background:#151515" uri="/"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Inicio<span class="badge" style="background-color: transparent"></span></g:link>
    <g:link elementId="category_fav" class="list-group-item" style="background:#151515" controller="list" action="fav"><i class="fa fa-star fa-fw" aria-hidden="true"></i> Mis favoritos<span class="badge" style="background-color: transparent"></span></g:link>
</div>
<div class="list-group">
    <h4 class="list-group-item" style="background:#252525">Información</h4>
    <g:link elementId="category_info" class="list-group-item" style="background:#151515" controller="info" action="download"><i class="fa fa-download fa-fw" aria-hidden="true"></i> Cómo descargar<span class="badge" style="background-color: transparent"></span></g:link>
    <g:link elementId="category_credits" class="list-group-item" style="background:#151515" controller="info" action="credits"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> Añadir créditos<span class="badge" style="background-color: transparent"></span></g:link>
    <g:link elementId="category_slots" class="list-group-item" style="background:#151515" controller="info" action="slots"><i class="fa fa-level-up fa-fw" aria-hidden="true"></i> Slots y velocidad<span class="badge" style="background-color: transparent"></span></g:link>
</div>
<div class="list-group">
    <h4 class="list-group-item" style="background:#252525">Sistemas</h4>
    <g:each in="${roots}" var="root">
        <g:set var="active" value="${params.getLong("catId") == root.id}"/>
        <g:if test="${root.resourcePackCount > 0}">
            <g:link elementId="category_${root.id}" class="list-group-item" style="background:#151515" controller="list" action="cat" id="${root.id}">${root.name} <span class="badge small" style="background-color: #052545; font-size: 74%;font-weight: normal; position:absolute;top:-3px;right:-3px">${root.humanSize}</span></g:link>
        </g:if>
        <g:else>
            <div class="list-group-item" style="background:#151515" >${root.name}</div>
        </g:else>

        <g:each in="${map[root.id]}" var="child">
            <g:set var="active" value="${params.getLong("catId") == child.id}"/>
            <g:if test="${child.resourcePackCount > 0}">
                <g:link elementId="category_${child.id}" class="list-group-item" style="font-size:95%; background-color:#151515" controller="list" params="[catId:child.id]"><i class="angle fa fa-angle-right" aria-hidden="true"></i> ${child.name} <span class="badge small" style="background-color: #052545; font-weight: normal;font-size: 74%;z-index:99;position:absolute;top:-3px;right:-3px">${child.humanSize}</span></g:link>
            </g:if>
            <g:else>
                <div class="list-group-item" style="font-size:95%; background-color:#151515" href="#navbar">${child.name}</div>
            </g:else>
        </g:each>
    </g:each>
</div>
