<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>
    <script type="application/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
</head>

<body>

<div class="col-lg-9 col-md-9 col-sm-8">
    <ul class="list-group">
        <g:each in="${list}" var="${resource}">
            <li class="list-group-item">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 ">
                        <g:if test="${total > 0}">
                            <h4>%{--<a href="" data-toggle="tooltip" class="text-warning" data-placement="bottom" data-original-title="Añadir a favoritos" <i class="fa fa-star"></i></a>>--}%${resource.name}
                            &raquo; <g:link class="small" style="color: #6bafd9" controller="list" action="resource"
                                            params="[id: resource.uuid]">Ver detalles</g:link></h4>

                            <span class="small text-primary">${raw(resource.description)}</span>
                        </g:if>
                        <g:else>
                            <h4>%{--<a href="" data-toggle="tooltip" class="text-warning" data-placement="bottom" data-original-title="Añadir a favoritos" <i class="fa fa-star"></i></a>>--}%${resource.name}

                            <span class="small text-primary">${raw(resource.description)}</span>
                            <hr/>
                            <span class="small text-primary">${raw(resource.content)}</span>

                            <ol>
                                <g:each in="${resourceFiles[resource.id]}" var="rf">
                                    <li>${rf.filename} | ${rf.humanSize}</li>
                                </g:each>
                            </ol>

                            <ol>
                                <g:each in="${resource.listRoms()}" var="rom">
                                    <li>${rom.name}</li>
                                </g:each>
                            </ol>

                        </g:else>
                        %{--
                       <a onclick="setDisabledStyle('${resource.uuid}')">not</a> |
                       <a onclick="setUnknownStyle('${resource.uuid}')">waiting</a> |
                       <a onclick="setLockedStyle('${resource.uuid}')">lock</a> |
                       <a onclick="setFinishedStyle('${resource.uuid}')">belongs</a> |
                       <a onclick="setShortStyle('${resource.uuid}')">short</a> |
                       <a onclick="setUnlockedStyle('${resource.uuid}')">queue</a> |
                       <a onclick="setDownloadingStyle('${resource.uuid}')">downloading</a>
--}%
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-right">
                        <p class="text-nowrap">
                            <span class="label label-info level_${resource.level}"
                                  style="color:white;background-color:transparent;font-weight: bold">${resource.limitByLevelString}</span>
                            <g:if test="${resource.credits == 0}">
                                <span class="label label-warning"
                                      style="color:black;background-color:yellow;font-weight: bold">¡0 créditos!</span>
                            </g:if>
                        </p>

                        <p class="text-nowrap"
                           style="font-size:24px;font-weight: 200;color:white">${tools.StringTools.humanReadableBytes(resource.size)} <span
                                style="font-size:10px">${tools.StringTools.humanReadableSize(resource.size)}</span></p>

                        <div>
                            <a id="link_${resource.uuid}" class="btn icon-btn btn-default disabled"
                               href="javascript:void(clickResource('${resource.uuid}'))"><i id="icon_${resource.uuid}"
                                                                                            style="width:30px"
                                                                                            class="btn-glyphicon fa fa-spinner fa-pulse fa-fw"></i> <span
                                    id="text_${resource.uuid}" class="small"><span id="tx_${resource.uuid}"></span>
                            </span></a>
                            <a id="q_${resource.uuid}"
                               style="z-index:99; position: absolute; right:5px;top:64px;display:none"
                               id="question_${resource.uuid}"
                               href="javascript:void(clickQuestion('${resource.uuid}'))"><i
                                    class="fa fa-question-circle" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </li>
        </g:each>
    </ul>
</div>

<g:if test="${total > 0}">
    <div class="text-center">
        <my:paginate controller="list" action="${action}" params="${[id: id]}" max="${max}" offset="${offset}"
                     total="${total}"/>
    </div>
</g:if>
</div>

</body>
</html>
