<%@ page import="mamespin.FileDownload" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1>Descargas usuarios</h1>
        </div>

        <div class="col-lg-12">
            <div class="bs-component">
                <my:paginate params="[state: params.state, order: params.order, sort: params.sort]" max="50"
                             total="${totalCount ?: 0}"/>
                %{--<my:table controllerName="users" collection="${downloadList}" properties="['lastUpdated', 'state',  'user']"/>--}%
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sortable sorted null"><a
                                href="/users/downloads?sort=lastUpdated&amp;order=asc">Last Updated</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=state&amp;order=asc"></a></th>
                        <th class="sortable"><a href="/users/downloads?sort=user&amp;order=asc">State</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=user&amp;order=asc">State</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=user&amp;order=asc">Name</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=user&amp;order=asc">Filename</a></th>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${downloadList}" var="${fd}" status="i">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                            <td>${fd.lastUpdated.format("dd/MM/yyyy")}
                                <span class="small text-primary">${fd.lastUpdated.format("HH:mm:ss")}</span></td>
                            <td>${fd.downloadPercent}%</td>
                            <td>${fd.speedKBs()} KB/s</td>
                            <td>
                                <g:if test="${fd.state == mamespin.FileDownload.State.finished}">
                                    <span class="label label-success">${fd.state}</span>
                                </g:if>
                                <g:elseif test="${fd.state == mamespin.FileDownload.State.download}">
                                    <span class="label label-info">${fd.state}</span>
                                </g:elseif>
                                <g:elseif test="${fd.state == mamespin.FileDownload.State.unlocked}">
                                    <span class="label label-primary">${fd.state}</span>
                                </g:elseif>
                            </td>
                            <td><g:link controller="users" action="downloads" params="[resourceId:fd.resourceId]">${fd.resource.name}</g:link></td>
                            <td><g:link controller="users" action="downloads" params="[resourceFileId:fd.resourceFileId]">${fd.resourceFile.filename}</g:link></td>
                            <td>${fd.user.username} ${fd.user.email}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <my:paginate params="[state: params.state, order: params.order, sort: params.sort]" max="50"
                             total="${totalCount ?: 0}"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>