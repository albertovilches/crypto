<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1>Usuarios</h1>
        </div>
        <div class="col-lg-12">
            <g:hasErrors bean="${theUser}">
            <ul class="errors" role="alert">
                <g:eachError bean="${theUser}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="well bs-component">
                            <g:form action="update" class="form-horizontal">
                                <g:hiddenField name="id" value="${theUser?.id}" />
                                <fieldset class="form">
                                    <legend>Editar usuario</legend>
                                    <f:field bean="theUser" property="username"/>
                                    <f:field bean="theUser" property="email"/>
                                    <f:field bean="theUser" property="state"/>
                                    <f:field bean="theUser" property="level"/>
                                    <f:field bean="theUser" property="slots"/>
                                </fieldset>

                                <hr/>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <input type="submit" class="btn btn-primary" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                                    </div>
                                </div>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
