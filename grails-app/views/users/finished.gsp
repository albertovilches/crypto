<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1>Descargas usuarios acabadas</h1>
        </div>

        <div class="col-lg-12">
            <div class="bs-component">
                <my:paginate params="[state: params.state, order: params.order, sort: params.sort]" max="50"
                             total="${totalCount ?: 0}"/>
                %{--<my:table controllerName="users" collection="${downloadList}" properties="['lastUpdated', 'state',  'user']"/>--}%
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>

                        <th class="sortable sorted null"><a
                                href="/users/downloads?sort=lastUpdated&amp;order=asc">Last Updated</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=state&amp;order=asc">State</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=user&amp;order=asc">User</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=user&amp;order=asc">User</a></th>
                        <th class="sortable"><a href="/users/downloads?sort=user&amp;order=asc">User</a></th>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${downloadList}" var="${fd}" status="i">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <td>${fd.lastUpdated.format("dd/MM/yyyy HH:mm:ss")}</td>
                        <td>${fd.downloadPercent}%</td>
                        <td>${fd.state}</td>
                        <td>${fd.resource.name}</td>
                        <td>${fd.resourceFile.filename}</td>
                        <td>${fd.user.username} ${fd.user.email}</td>
                    </tr>
                    </g:each>
                    </tbody>

                    <my:paginate params="[state: params.state, order: params.order, sort: params.sort]" max="50"
                                 total="${totalCount ?: 0}"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>