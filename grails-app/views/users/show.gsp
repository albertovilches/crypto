<%@ page import="mamespin.Payment; mamespin.UserAction" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1>Usuarios</h1>
        </div>
        <div class="col-lg-12">
            <div class="well bs-component">
                <fieldset class="buttons">
                    <g:link class="btn btn-default" action="edit" controller="users" params="[id: theUser.id]"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <g:link class="btn btn-default" action="impersonate" controller="users" params="[id: theUser.id]">Impersonate</g:link><br/>
                    <g:form action="sendCredit" style="display: inline">
                        <input type="text" style="width:50px" value="500" name="mb"/>Mb
                        <input type="hidden" name="id" value="${theUser.id}"/>
                        <g:submitButton name="submit" class="btn btn-default" value="Gift"/>
                    </g:form><br/>
                    <g:form action="buyCredit" style="display: inline">
                        <input type="hidden" name="id" value="${theUser.id}"/>
                        <input type="text" style="width:50px" value="200" name="gb"/>Gb
                        <input type="text" style="width:50px" value="5" name="amount"/>
                        <g:select class="form-control" name="currency" from="${mamespin.Payment.Currency.values()}"/>
                        <g:select class="form-control" name="type" from="${mamespin.Payment.Type.values()}"/>
                        <g:submitButton name="submit" class="btn btn-default" value="Purchase"/>
                    </g:form>
                    <g:link action="delete" class="btn btn-danger" params="[id: theUser.id]" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">Borrar</g:link>


                </fieldset>
            </div>
            <f:display bean="theUser" />
        </div>

        <div class="col-lg-12">
            <my:table collection="${mamespin.UserAction.findAllByUser(theUser,[sort:'dateCreated', order:'desc'])}" properties="['dateCreated', 'ip', 'type']"/>

        </div>
    </div>
</div>
</body>
</html>
