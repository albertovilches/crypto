<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1>Usuarios</h1>
        </div>
        <div class="col-lg-12">
            <form class="navbar-form navbar-left">
               <div class="form-group">
                   <input type="text" name="email" class="form-control" placeholder="Email" value="${params.email}">
               </div>
               <button type="submit" class="btn btn-info">Buscar</button>
               <g:link class="btn btn-default ali" action="create">Crear</g:link>
            </form>
        </div>
        <div class="col-lg-12">
            <div class="bs-component">
                <my:paginate params="[email: params.email]" max="50" total="${userCount ?: 0}" />
                <my:table controllerName="users" collection="${userList}" properties="['id', 'email', 'username', 'state', 'dateCreated', 'lastLogin', 'credits', 'level', 'slots']"/>
                <my:paginate params="[email: params.email]" max="50" total="${userCount ?: 0}" />
            </div>
        </div>
    </div>
</div>
</body>
</html>