<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Sign In</title>
</head>

<body>
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <g:form name="submitForm" class="sign-box" method="post" action="check">
                <input type="hidden" name="nextUrl" value="${params.nextUrl}"/>

                <div class="sign-avatar">
                    <asset:image src="avatar-sign.png" alt=""/>
                </div>
                <header class="sign-title">Credentials</header>

                <div class="form-group">
                    <input class="form-control" name="n" type="email" id="n"
                           value="${params.n}" required="required"
                           placeholder="E-Mail">
                </div>

                <div class="form-group">
                    <input class="form-control" name="pwd" type="password" id="pwd"
                           placeholder="Password" required="required">
                </div>

                <div class="form-group">
                    <div class="checkbox float-left">
                        <input type="checkbox" id="signed-in" name="rememberMe"/>
                        <label for="signed-in">Remember me</label>
                    </div>

                    <div class="float-right reset">
                        <g:link controller="recover" action="email">Forgot your password?</g:link>
                    </div>
                </div>
                <button class="btn btn-rounded ladda-button" id="submit"  data-style="expand-right" type="submit"><span
                        class="ladda-label">Login</span></button>

                <p class="sign-note">Don't have an account? <g:link controller="register" action="me"
                                                                    params="[from: 'top']">Sign up</g:link>
                %{--<button type="button" class="close">--}%
                %{--<span aria-hidden="true">&times;</span>--}%
                %{--</button>--}%

                <div style="height: 50px">
                    <g:if test="${flash.error}">
                        <div class="alert alert-danger alert-fill alert-close alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            Wrong credentials
                        </div>
                    </g:if>
                </div>
            </g:form>
        </div>
    </div>
</div><!--.page-center-->


<script>

    $(document).ready(function () {
        <g:if test="${params.n}">
        $('#pwd').focus()
        </g:if>
        <g:else>
        $('#n').focus()
        </g:else>
    });
</script>
</body>
</html>