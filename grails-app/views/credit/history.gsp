<%@ page import="mamespin.Operation; tools.StringTools; tools.UAgent" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>

    <script type="application/javascript">
    </script>
</head>

<body>

<div class="col-lg-9 col-md-9 col-sm-8">
    <div class="panel">
        <nav class="navbar" style="margin-bottom:0;margin-top:10px">
            <div class="container-fluid">
                <div class="navbar-header">
                    <span class="navbar-brand info-header"><span class="info-number">${credits}</span></span>
                    <span class="navbar-brand info-header" style="color: #666">Total acumulado: <span style="color: #666;" class="info-number">${totalAccumulated}</span></span>
                </div>

                <g:if test="${ops}">
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            %{--<li><a href=""> Copiar todos</a></li>--}%
                            %{--
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">Acciones <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#"><i class="fa fa-trash fa-fw" aria-hidden="true"></i> Borrar todos</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            --}%
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><g:link controller="info" action="credits"><i class="fa fa-info-circle fa-fw" aria-hidden="true"></i> Ayuda</g:link>
                            </li>
                        </ul>
                    </div>
                </g:if>
            </div>
        </nav>

        <g:if test="${ops}">

            <span class="small text-primary" style="padding-left:9px">${countOps} operaciones</span>
            <table class="table table-striped table-hover panel panel-info"
                   style="margin:0 auto 15px auto; width: 97.5%">
                <thead>
                <tr>
                    <th colspan="2">Fecha</th>
                    <th>Créditos</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${ops}" var="${op}" idx="i">
                    <tr>
                        <td class="text-nowrap">${op.dateCreated.format("dd/MMM/yyyy")} <span
                                class="small text-primary">${op.dateCreated.format("HH:mm")}</span>
                        <td class="text-center"><g:if test="${op.type == mamespin.Operation.Type.download}">
                            <span class="label label-big label-danger">Descarga</span>
                        </g:if>
                            <g:elseif test="${op.type == mamespin.Operation.Type.sent}">
                                <span class="label label-big label-warning">Envias</span>
                            </g:elseif>
                            <g:elseif test="${op.type == mamespin.Operation.Type.buy}">
                                <span class="label label-big label-info">Recarga</span>
                            </g:elseif>
                            <g:elseif test="${op.type == mamespin.Operation.Type.refund}">
                                <span class="label label-big label-success">Devolución</span>
                            </g:elseif>
                            <g:elseif test="${op.type == mamespin.Operation.Type.recv}">
                                <span class="label label-big label-success">Recibes</span>
                            </g:elseif>

                        </td>
                        <td class="text-nowrap text-right">${op.humanCredits}</td>
                        <td width="100%" style="vertical-align: middle" class="${subject[op.id]?"gradient":""}">
                            <g:if test="${op.userResource}">
                                <g:link controller="list" action="resource"
                                        params="[id: op.userResource.resource.uuid]">Ver detalles</g:link>
                            </g:if>
                            <g:if test="${op.userResource?.revocable}">
                                <span class="text-primary small">
                                    Descarga cancelable</span> <g:link controller="downloads" action="revoke"
                                                                       params="[id: op.userResource.id, from: 'credits']"><i
                                        class="fa fa-trash" aria-hidden="true"></i></g:link>
                            </g:if>
                            <g:elseif test="${op.type == mamespin.Operation.Type.sent}">
                                Destinatario: <b>${op.other?.username}</b>
                            </g:elseif>
                            <g:elseif test="${op.type == mamespin.Operation.Type.recv}">
                                Donante: <b>${op?.other?.username?:"desconocido"}</b>
                            </g:elseif>
                            ${raw(subject[op.id])}
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </g:if>
        <g:else>
            <div class="text-center panel panel-default text-primary"
                 style="margin:0 15px 15px;padding:15px;">Sin operaciones.</div>
        </g:else>
    </div>
</div>
</body>
</html>
