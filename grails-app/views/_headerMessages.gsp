<g:set var="myErrors" value="${[flash.errors, errors, flash.error, error].flatten().findAll()}"/>
<g:if test="${myErrors}">
    <div class="container">
        <div class="row">
            <div class="bs-component">
                <div class="alert alert-dismissible alert-danger" style="margin-top:20px">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <g:if test="${myErrors.size() > 1}">
                        <ul><g:each var="e" in="${myErrors}">
                            <li>${raw(e)}</li>
                        </g:each></ul>
                    </g:if>
                    <g:else>
                        ${raw(myErrors[0])}
                    </g:else>
                </div>
            </div>
        </div>
    </div>
</g:if>

<g:set var="myMessages" value="${[flash.messages, messages, flash.message, message].flatten().findAll()}"/>
<g:if test="${myMessages}">
    <div id="mmm" class="alert alert-dismissible alert-success alert-fixed">
        <g:if test="${myMessages.size() > 1}">
            <ul><g:each var="e" in="${myMessages}">
                <li>${raw(e)}</li>
            </g:each></ul>
        </g:if>
        <g:else>
            ${raw(myMessages[0])}
        </g:else>
        <button type="button" class="close" data-dismiss="alert" style="float:none">&times;</button>
    </div>
    <script type="application/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $("#mmm").fadeOut()
            }, 5000)
        })
    </script>
</g:if>
