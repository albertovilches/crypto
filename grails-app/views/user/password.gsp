<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cambiar contraseña</title>
</head>

<body>
<div class="col-lg-5 col-lg-push-2 col-md-5 col-md-push-2 col-sm-6 col-sm-push-1" style="margin-top:50px">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Cambiar contraseña</h4>
        </div>

        <div class="modal-body">
            <g:form class="bs-component" method="post" action="changePwd">
                <div class="form-group">
                    <input class="form-control" name="current" type="password" id="current" value="${flash.current}"
                           placeholder="Introduce tu contraseña actual">

                    <p style="float: right;margin:5px" class="small"><g:link
                            action="recover">No recuerdo mi contraseña</g:link></p>
                </div>

                <div class="form-group">
                    <input class="form-control" name="newPwd" type="password" id="newPwd" value=""
                           placeholder="Nueva contraseña"
                           data-toggle="tooltip" data-placement="bottom" data-original-title="Minimo 6 caracteres">
                </div>

                <div class="form-group">
                    <input class="form-control" name="newPwd2" type="password" id="newPwd2" value=""
                           placeholder="Repite la nueva contraseña">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-info" style="width: 100%">Guardar</button>
                </div>
            </g:form>
        </div>

    </div>
</div>

<script>
    <g:if test="${!flash.current}">
    $('#current').focus()
    </g:if>
    <g:else>
    $('#newPwd').focus()
    </g:else>
    $('[data-toggle="tooltip"]').tooltip()
</script>

</body>
</html>
