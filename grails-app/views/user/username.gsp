<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cambiar usuario</title>
</head>

<body>
<div class="col-lg-5 col-lg-push-2 col-md-5 col-md-push-2 col-sm-6 col-sm-push-1" style="margin-top:50px">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Nombre actual: ${currentUser.username}</h4>
        </div>

        <div class="modal-body">
            <g:form class="bs-component" method="post" action="changeUsername">
                <div class="form-group">
                    <input class="form-control" name="username" type="text" id="n" value="${flash.message}"
                           placeholder="Nuevo nombre usuario">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-info" style="width: 100%">Cambiar nombre</button>
                </div>
            </g:form>
        </div>
    </div>
</div>
<script>
    $('#n').focus();
    $('#n').val($('#n').val());
</script>

</body>
</html>
