<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Bloqueado temporalmente</title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4">

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Atención</h3>
                </div>

                <div class="panel-body">
                    <p>Por motivos de seguridad, tu ip ha sido bloqueda temporalmente durante unos minutos.</p>
                    <p>Tómate un café y vuelve dentro de un rato.</p>

                    <p style="text-align: center">
                        <br/>
                        <span class="label label-danger" style="font-size: 20px" id="x"></span>
                        <br/>
                        <br/>
                        <g:link controller="recover" action="email">No recuerdo mi contraseña</g:link>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>

<script>

    var remaining = ${remaining}
    showTimer()
    function showTimer() {
        if (remaining <= 0) {
            location.href = "${g.createLink(controller: "sign", action:"in")}"
        }
        $("#x").html(formatTime(remaining))
        remaining --
        setTimeout(showTimer, 1000)
    }

    function formatTime (n) {
        var hours = Math.floor(n/60/60),
            minutes = Math.floor((n - (hours * 60 * 60))/60),
            seconds = Math.round(n - (hours * 60 * 60) - (minutes * 60));
        return (hours > 0 ? hours + ':' : '') + ((minutes < 10) ? '0' + minutes : minutes) + ':' + ((seconds < 10) ? '0' + seconds : seconds);
    }


</script>

</body>
</html>
