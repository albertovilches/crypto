<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Error 404 - Page Not Found</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"><asset:stylesheet src="application.css"/>
</head>

<body>
<div onkeydown="return press(event);" onkeyup="return release(event);" onkeypress="return false;">

    <div class="row">
        <div class="col-lg-3">
        </div>

        <div class="col-lg-6">

            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Error 404 - Page Not Found</h3>
                </div>

                <div class="panel-body" style="text-align: center">
                    <p>La página solicitada no ha sido encontrada.</p>
                    <p>Mientras tanto siempre te puedes echar una partidita...</p>

                    <g:link class="btn btn-default"  uri="/">Ir a la home</g:link>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="javascript:void(start())" class="btn btn-primary">Jugar</a>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3">
        </div>

        <div class="col-lg-6">

            <div class="panel panel-warning" style="">
                <div class="panel-body" style="background: black; text-align: center">
                        <asset:javascript src="galaga/ga.js"/>
                        <asset:javascript src="galaga/ga-core.js"/>
                        <g:javascript>
                            $("document").ready(function () {
                                prepare();
                                draw();
                            })
                        </g:javascript>

                    <div class="galaxianScreen" style="width:578px;margin: 22px;  border: 0px solid #1b698e; height: 465px;margin: auto">
                        <canvas id="canvasInfo" width="576" height="25" style="border:0px;float:left;"></canvas>
                        <canvas id="canvas" width="576" height="438" style="border:0px;float:left;"></canvas>
                    </div>
                    </p>
                </div>
                <div class="panel-footer" style="text-align: center">
                    <h3 class="panel-title"><a href="javascript:void(start())" class="btn btn-primary">Jugar</a></h3>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
