<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Sign In</title>
</head>

<body>
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <g:form class="sign-box " action="checkAndSend" style="text-align: center">
                <!--<div class="sign-avatar">
                    <img src="img/avatar-sign.png" alt="">
                </div>-->
                <header class="sign-title">Done!</header>
                An email with password reset instructions was sent to your email address. <a
                    href="${flash.url}"></a><br/>
                <hr/>

                <div style="text-align: center">
                    You can close this window.
                </div>
            </g:form>
        </div>
    </div>
</div><!--.page-center-->
</body>
</html>