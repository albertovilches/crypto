<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Sign In</title>
</head>

<body>
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <g:form name="resetForm" class="sign-box reset-password-box" action="checkAndSend">
                <!--<div class="sign-avatar">
                    <img src="img/avatar-sign.png" alt="">
                </div>-->
                <header class="sign-title">Create new password</header>

                <div class="form-group">
                    <input type="email" class="form-control" name="n" placeholder="E-Mail" required/>
                </div>

                <div style="text-align: center">
                    <button type="submit" class="btn btn-rounded ladda-button" id="submit"
                            data-style="expand-right">Reset password</button>
                    or <g:link controller="sign" action="in">Sign in</g:link>
                </div>
            </g:form>
        </div>
    </div>
</div><!--.page-center-->

</body>
</html>