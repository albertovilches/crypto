<footer>
    <div class="container">

        <div class="row">
            <div class="row">
                <div class="col-lg-12">

                    <ul class="list-unstyled">
                        <li class="pull-right"><a href="#top">Subir arriba del todo</a></li>
                        <li><g:link controller="info" action="download">Cómo descargar</g:link></li>
                        <li><g:link controller="info" action="credits">Añadir créditos</g:link></li>
                        <li><g:link controller="info" action="slots">Slots y velocidad</g:link></li>
                        <li><g:link controller="downloads">Mis descargas</g:link></li>
                        <li><g:link controller="credit" action="history">Historial</g:link></li>
                    </ul>

%{--
                    <p>Code released under the <a
                            href="https://github.com/thomaspark/bootswatch/blob/gh-pages/LICENSE">MIT License</a>.</p>

                    <p>Based on <a href="http://getbootstrap.com" rel="nofollow">Bootstrap</a>. Icons from <a
                            href="http://fortawesome.github.io/Font-Awesome/"
                            rel="nofollow">Font Awesome</a>. Web fonts from <a href="http://www.google.com/webfonts"
                                                                               rel="nofollow">Google</a>.</p>

--}%
                </div>
            </div>

        </div>
    </div>
</footer>
