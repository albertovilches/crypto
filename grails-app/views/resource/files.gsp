<%@ page import="tools.StringTools; mamespin.FileDownload" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'resource.label', default: 'resource')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="list">Home</g:link></li>
                    <li><g:link controller="resource">resource list</g:link></li>
                    <li><g:link action="show" id="${resource.id}">${resource.uuid} (${resource.id})</g:link></li>
                    <li class="active">Add files</li>
                </ul>
            </div>

            <h3>${resource.name}</h3>
            <span class="small">Created: ${resource.dateCreated.format("dd/MM/yyyy HH:mm:ss")} | Last updated: ${resource.lastUpdated.format("dd/MM/yyyy HH:mm:ss")}</span>
        </div>

        <div class="col-lg-12" style="margin-top:5px">
            <g:link class="btn-sm btn btn-default" action="edit" params="[id: resource.id]">Edit</g:link>
            <g:link class="btn-sm btn btn-default" action="show" params="[id: resource.id]">Show</g:link>
            <g:link class="btn-sm btn btn-default" action="updateChildrenInfo"
                    params="[id: resource.id]">Update children info</g:link>
        </div>

        <g:form action="files">
            <g:hiddenField name="id" value="${resource.id}"/>

            <div class="col-lg-6">
                <g:if test="${resource.children}">
                    <g:set var="my" value="${files.findAll { it.file }.collect { it.name.toLowerCase() } as Set}"/>
                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr class="even">
                            <td><input type="checkbox" onclick="$('.rfileCb').prop('checked', this.checked);"/></td>
                            <td colspan="3">Total: ${resource.children.size()}, Children count: ${resource.childrenCount} | ${resource.humanSize}

                            <g:actionSubmit value="Drop selected" action="dropFiles" class="btn btn-sm btn-danger"/>
                            </td>
                        </tr>
                        <g:each in="${resource.children.sort { it.localPath }}" var="file" status="i">
                            <g:set var="downCount" value="${mamespin.FileDownload.countByResourceFile(file)}"/>
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                <td class="text-nowrap"><input class="rfileCb" name="fileId" type="checkbox" value="${file.id}"/> <span class="text-primary small">${file.id}</span></td>
                                <td style="width: 100%">
                                    <g:if test="${new File(file.localPath).name.toLowerCase() in my}"><span class="text-success">${file.localPath}</span></g:if>
                                    <g:else>${file.localPath}</g:else>
                                    <g:if test="${!file.localPath.endsWith(file.filename)}"><br/><span class="text-warning small">${file.filename}</span></g:if>
                                </td>
                                <td class="text-nowrap">${file.humanSize}</td>
                                <td class="text-nowrap"><g:if test="${downCount}"><span
                                        class="label label-info">${downCount}</span></g:if>
                                    <g:else>
                                        <g:link action="deleteFile" id="${file.id}"></g:link>
                                    </g:else></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
            </div>

            <div class="col-lg-6">

                <input type="text" name="folder" value="${folder}" class="form-control input-sm">

                <table class="table table-striped table-hover">
                    <tbody>
                    <tr class="even">
                        <td>
                            <g:if test="${fileCount}">
                                <input type="checkbox" onclick="$('.fileCb').prop('checked', this.checked);"/>
                            </g:if>
                        </td>
                        <td colspan="3" class="text-nowrap"><g:link action="files" id="${resource.id}"
                                                                    params="${[folder: "$folder/.."]}">[..]</g:link>
                        <g:if test="${fileCount}">
                            <g:actionSubmit value="Add selected" action="addFiles" class="btn btn-sm btn-success"/>
                        </g:if>

                        </td>
                    </tr>
                    <g:set var="my" value="${resource.children.collect { it.localPath.toLowerCase() } as Set}"/>
                    <g:each in="${files}" var="file" status="i">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                            <g:if test="${file.directory}">
                                <td></td>
                                <td class="text-nowrap"><g:link action="files" id="${resource.id}"
                                                                params="${[folder: "$folder/$file.name"]}">[${file.name}]</g:link></td>
                                <td style="width:100%"></td>
                            </g:if>
                            <g:else>
                                <g:if test="${file.absolutePath.toLowerCase() in my}">
                                    <td class=""></td>
                                    <td style="width:100%" class="text-nowrap text-success">${file.name}</td>
                                    <td class="text-nowrap"
                                        style="width:100%">${tools.StringTools.humanReadableString(file.length())}</td>
                                </g:if>
                                <g:else>
                                    <td><input class="fileCb" name="filename" type="checkbox"
                                               value="${file.absolutePath}"/></td>
                                    <td style="width:100%" class="text-nowrap">${file.absolutePath}</td>
                                    <td class="text-nowrap">${tools.StringTools.humanReadableString(file.length())}</td>
                                </g:else>
                            </g:else>
                        </tr>
                    </g:each>
                    </tbody>
                </table>

            </div>
        </g:form>
    </div>
</div>
</div>
</body>
</html>