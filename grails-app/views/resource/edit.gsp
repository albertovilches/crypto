<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'resource.label', default: 'resource')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="list">Home</g:link></li>
                    <li><g:link controller="resource">resource list</g:link></li>
                    <li><g:link action="show" id="${resource.id}">${resource.uuid} (${resource.id})</g:link></li>
                    <li class="active">Edit</li>
                </ul>
            </div>
            <h3>${resource.name}</h3>
        </div>


        <div class="col-lg-12">
            <g:hasErrors bean="${resource}">
                <div class="container">
                    <div class="row">
                        <div class="bs-component">
                            <div class="alert alert-dismissible alert-danger" style="margin-top:20px">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <ul class="errors" role="alert">
                                    <g:eachError bean="${resource}" var="error">
                                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                                error="${error}"/></li>
                                    </g:eachError>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </g:hasErrors>

            <div class="row">
                <div class="col-lg-6">
                    <div class="well bs-component">
                        <g:form action="update" class="form-horizontal">
                            <g:hiddenField name="id" value="${resource?.id}"/>
                            <fieldset class="form">
                                <f:field bean="resource" property="category"/>
                                <f:field bean="resource" property="name"/>
                                <f:field bean="resource" property="hidden"/>
                                <f:field bean="resource" property="creditType"/>
                                <f:field bean="resource" property="publishedDate"/>
                                <f:field bean="resource" property="level"/>
                                %{--<f:field bean="resource" property="url"/>--}%
                                %{--<f:field bean="resource" property="image"/>--}%
                                <f:field bean="resource" property="content"/>
                                <f:field bean="resource" property="description"/>
                            </fieldset>
                            <hr/>
                            <div cla    ss="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <input type="submit" class="btn btn-primary"
                                           value="${message(code: 'default.button.update.label', default: 'Create')}"/>
                                </div>
                            </div>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>