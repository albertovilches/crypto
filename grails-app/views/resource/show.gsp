<%@ page import="tools.StringTools; mamespin.FileDownload" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'resource.label', default: 'resource')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="list">Home</g:link></li>
                    <li><g:link controller="resource">resource list</g:link></li>
                    <li class="active">${resource.uuid} (${resource.id})</li>
                </ul>
            </div>

            <h3>${resource.name}</h3>
            <span class="small">Created: ${resource.dateCreated.format("dd/MM/yyyy HH:mm:ss")} | Last updated: ${resource.lastUpdated.format("dd/MM/yyyy HH:mm:ss")}</span>
        </div>

        <div class="col-lg-12" style="margin-top:5px">
            <g:link class="btn-sm btn btn-default" action="edit" params="[id: resource.id]">Edit</g:link>
            <g:link class="btn-sm btn btn-default" action="files" params="[id: resource.id]">Manage files</g:link>
            <g:link action="delete" class="btn-sm btn btn-danger" params="[id: resource.id]"
                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">Borrar</g:link>
        </div>

        <div class="col-lg-12" style="padding-top:20px">
            <div class="row">
                <div class="form-group">
                    <label class="col-lg-1 control-label" style="text-align: right">Category</label>

                    <div class="col-lg-10">
                        <g:link controller="list" action="cat"
                                id="${resource.categoryId}">${resource.category.name}</g:link> |
                        <g:link controller="category" action="show"
                                id="${resource.categoryId}">Editar</g:link> Id: ${resource.categoryId}|
                        <g:if test="${resource.category.hidden}">
                            <span class="label label-primary">Not visible</span>
                        </g:if>
                        <g:elseif test="${(!resource.publishedDate || resource.publishedDate < new Date()) && !resource.hidden}">
                            <span class="label label-success">Published</span>
                        </g:elseif>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-lg-1 control-label" style="text-align: right">Published</label>

                    <div class="col-lg-10">

                        <g:if test="${!resource.publishedDate}">
                            <span class="text-primary">no publish date</span>
                        </g:if>
                        <g:elseif test="${resource.publishedDate > new Date()}">
                            <span class="text-primary">${resource.publishedDate.format("dd/MM/yyyy HH:mm:ss")}</span>
                        </g:elseif>
                        <g:else>
                            <span class="text-success">${resource.publishedDate.format("dd/MM/yyyy HH:mm:ss")}</span>
                        </g:else>


                        <g:if test="${resource.hidden}">
                            <span class="label label-primary">Hidden</span>
                            <g:link class="" action="unhide" params="[id: resource.id]">Visible</g:link>
                        </g:if>
                        <g:else>
                            <span class="label label-success">Not hidden</span>
                            <g:link class="" action="hide" params="[id: resource.id]">Hide</g:link>
                        </g:else>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-lg-1 control-label" style="text-align: right">Type</label>

                    <div class="col-lg-10">
                        ${resource.creditType?.name() ?: "normal"}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-lg-1 control-label" style="text-align: right">Level</label>

                    <div class="col-lg-10">
                        ${resource.level != null ? resource.level : "(null)"}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-lg-1 control-label" style="text-align: right">Size</label>

                    <div class="col-lg-10">
                        ${resource.childrenCount} children: ${resource.humanSize} (${resource.size} créditos)
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-lg-1 control-label" style="text-align: right">Files</label>

                    <div class="col-lg-10">
                        <g:link class="" action="files"
                                params="[id: resource.id, folder: '/srv']">Manage files</g:link>

                        <g:if test="${resource.children.sort { it.localPath }}">
                            <table class="table table-striped table-hover">
                                <tbody>
                                <g:each in="${resource.children}" var="file" status="i">
                                    <g:set var="downCount" value="${mamespin.FileDownload.countByResourceFile(file)}"/>
                                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                        <td>${file.filename}</td>
                                        <td>${file.humanSize}</td>
                                        <td>${file.size}</td>
                                        <td>${file.localPath}</td>
                                        <td><g:if test="${downCount}"><span
                                                class="label label-info">${downCount}</span></g:if>
                                        </td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </g:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>