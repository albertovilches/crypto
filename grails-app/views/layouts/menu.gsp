<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Portfolio tracker</title>

    <link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
    <link href="img/favicon.png" rel="icon" type="image/png">
    <link href="img/favicon.ico" rel="shortcut icon">

    <script type="application/javascript" src="https://code.jquery.com/jquery-2.2.4.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    %{--<asset:stylesheet src="css/separate/vendor/tags_editor.min.css"/>--}%
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <asset:stylesheet src="separate/vendor/bootstrap-select/bootstrap-select.min.css"/>
    <asset:stylesheet src="lib/bootstrap/bootstrap.min.css"/>
    <asset:stylesheet src="main.css"/>
    <asset:stylesheet src="mio.css"/>

    <g:layoutHead/>

</head>

<body class="with-side-menu theme-picton-blue-white-ebony">

<g:render template="/layouts/header"/>
<div class="mobile-menu-left-overlay"></div>

<g:render template="/layouts/side-menu"/>

<div class="page-content">
    <g:set var="errorMessage" value="${errorMessage?:flash.errorMessage?:params.errorMessage}"/>
    <g:if test="${errorMessage}">
        <div class="container-fluid">
            %{--http://themesanytime.com/startui/alerts.html--}%
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-no-border alert-close alert-dismissible fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <i class="font-icon font-icon-inline font-icon-warning"></i>
                        <strong>Attention</strong><br>
                        ${errorMessage}
                        %{--<div class="alert-btns">--}%
                            %{--<button type="button" class="btn btn-rounded">Do this</button>--}%
                            %{--<button type="button" class="btn btn-rounded">Or this</button>--}%
                        %{--</div>--}%
                    </div>

                </div>
            </div>
        </div>
    </g:if>


    <g:set var="infoMessage" value="${infoMessage?:flash.infoMessage?:params.infoMessage}"/>
    <g:if test="${infoMessage}">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info alert-fill alert-close alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        %{--<strong>Some message</strong><br>--}%
                        ${infoMessage}
%{--
                        <div class="alert-btns">
                            <button type="button" class="btn btn-rounded">Do this</button>
                            <button type="button" class="btn btn-rounded">Or this</button>
                        </div>
--}%
                    </div>
                </div>
            </div><!--.row-->
        </div>
    </g:if>



    <g:render template="/transaction"/>



    <g:layoutBody/>
</div>
</body>
</html>
