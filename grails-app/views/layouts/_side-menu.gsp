<nav class="side-menu">
    <ul class="side-menu-list">
        <li class="grey ${controllerName == "dashboard" && actionName == "index"?"opened":""}">
            <link:dashboard>
                <i class="font-icon font-icon-dashboard"></i>
                <span class="lbl">Dashboard</span>
            </link:dashboard>
        </li>
        <li class="yellow ${controllerName == "dashboard" && actionName == "holdings"?"opened":""}">
            <link:holdings>
                <i class="fa fa-bank"></i>
                <span class="lbl">Holdings</span>
            </link:holdings>
        </li>
        <li class="purple ${controllerName == "dashboard" && actionName == "profit"?"opened":""}">
            <link:profit>
                <i class="fa fa-money"></i>
                <span class="lbl">Profit</span>
            </link:profit>
        </li>
        <li class="brown ${controllerName == "trade"?"opened":""}">
            <link:trades>
                <i class="fa fa-handshake-o"></i>
                <span class="lbl">Trades</span>
            </link:trades>
        </li>
    </ul>

    <g:if test="${controllerName != "trade"}">

    <section>
        <header class="side-menu-title">Top 10</header>
        <ul class="side-menu-list">
        <g:each in="${menuHoldings}" var="${holding}">
        <li class="purple  ${controllerName == "dashboard" && actionName == "holding" && params.id == holding.name?"opened":""}">
            <link:holding id="${holding.name}">
                <img src="${holding.ccFullImageUrl()}" width="16" class="tag-color"/>
                <span class="lbl">${holding.name} / <span class="small valueUnit_${holding.name}" style="floatX:right"></span> <span class="small">€</span></span>
            </link:holding>
        </li>
        </g:each>
        <li class="yellow ${controllerName == "dashboard" && actionName == "holdings"?"opened":""}">
            <link:holdings>
                <i class="fa fa-bank"></i>
                <span class="lbl">All</span>
            </link:holdings>
        </li>
        </ul>
    </section>
    </g:if>

    <g:if test="${currentUser.admin}">
    <section>
        <header class="side-menu-title">Admin</header>
        <ul class="side-menu-list">
            <li class="grey">
                <g:link controller="currency" action="">
                    <i class="fa fa-money"></i>
                    <span class="lbl">Currencies</span>
                </g:link>
            </li>
            <li class="grey">
                <g:link controller="currency" action="pairs">
                    <i class="font-icon font-icon-dashboard"></i>
                    <span class="lbl">All pairs</span>
                </g:link>
            </li>
        </ul>
    </section>
    </g:if>

%{--    <section>
        <header class="side-menu-title">Tags</header>
        <ul class="side-menu-list">
            <li>
                <a href="#">
                    <i class="tag-color green"></i>
                    <span class="lbl">Website</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="tag-color grey-blue"></i>
                    <span class="lbl">Bugs/Errors</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="tag-color red"></i>
                    <span class="lbl">General Problem</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="tag-color pink"></i>
                    <span class="lbl">Questions</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="tag-color orange"></i>
                    <span class="lbl">Ideas</span>
                </a>
            </li>
        </ul>
    </section>--}%


</nav><!--.side-menu-->
