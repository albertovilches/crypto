<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1><g:message code="default.create.label" args="[entityName]"/></h1>
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="list">Home</g:link></li>
                    <li><g:link controller="category">category list</g:link></li>
                    <li class="active">Edit category ${category.id}</li>
                </ul>
            </div>
        </div>

        <div class="col-lg-12">
            <g:hasErrors bean="${category}">
                <div class="container">
                    <div class="row">
                        <div class="bs-component">
                            <div class="alert alert-dismissible alert-danger" style="margin-top:20px">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <ul class="errors" role="alert">
                                    <g:eachError bean="${category}" var="error">
                                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                                error="${error}"/></li>
                                    </g:eachError>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </g:hasErrors>

            <div class="row">
                <div class="col-lg-6">
                    <div class="well bs-component">
                        <g:form action="update" class="form-horizontal">
                            <g:hiddenField name="id" value="${category?.id}"/>
                            <fieldset class="form">
                                <legend>Edit category ${category?.id}</legend>
                                <f:all bean="${category}"/>
                            </fieldset>

                            <hr/>

                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <input type="submit" class="btn btn-primary"
                                           value="${message(code: 'default.button.update.label', default: 'Create')}"/>
                                </div>
                            </div>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>