<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="list">Home</g:link></li>
                    <li><g:link controller="category">category list</g:link></li>
                    <li class="active">Show category ${category.id}</li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="well bs-component">
                <fieldset class="buttons">
                    <g:link class="btn btn-default" action="edit" params="[id: category.id]"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <g:link action="delete" class="btn btn-danger" params="[id: category.id]" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">Borrar</g:link>
                </fieldset>
            </div>
            <f:display bean="${category}" />
        </div>
    </div>
</div>
</body>
</html>