<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1>category</h1>
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="list">Home</g:link></li>
                    <li class="active">category list</li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12">
            <form class="navbar-form navbar-left">
               <div class="form-group">
                   <input type="text" name="search" class="form-control" placeholder="Buscar" value="${search}">
               </div>
               <button type="submit" class="btn btn-info">Buscar</button>
                <g:link class="btn btn-default" action="create">Crear</g:link>
            </form>
        </div>
        <div class="col-lg-12">
            <div class="bs-component">
            <my:paginate params="[search: search]" max="50" total="${categoryCount ?: 0}" />
            <my:table controllerName="category" collection="${categoryList}" />
            <my:paginate params="[search: search]" max="50" total="${categoryCount ?: 0}" />
        </div>
    </div>
</div>
</body>
</html>
