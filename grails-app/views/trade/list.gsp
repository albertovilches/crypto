<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Your trades</title>

    <asset:stylesheet src="lib/bootstrap-table/bootstrap-table.min.css"/>

    <asset:stylesheet src="lib/bootstrap-sweetalert/sweetalert.css"/>
    <asset:stylesheet src="separate/vendor/sweet-alert-animations.min.css"/>

    <style type="text/css">
    .amount {
    }

    .currency {
        color: #999;
        /*font-weight: normal !important;*/
    }

    table#tableTradeList td, table#tableTradeList t {
        border:0 !important;
    }
    </style>

</head>

<body>
    %{--<div class="container-fluid">--}%
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3 id="pageTitle">Your trades</h3>
%{--
                        <ol class="breadcrumb breadcrumb-simple">
                            <li><g:link controller="trade" action="list">List</g:link></li>
                            <li class="active"></li>
                        </ol>
--}%
                    </div>
                </div>
            </div>
        </header>


        <section class="box-typical" id="tradeList">
%{--
            <header class="box-typical-header">
                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title" style="padding-bottom:0">
                        <h1 style="font-size:1.2rem">Your trades</h1>
                    </div>
                </div>
            </header>
--}%

            <div id="toolbar">
                <button id="remove" class="btn btn-secondary btn-sm bulk" disabled>
                    <i class="font-icon font-icon-trash"></i> Delete
                </button>
            </div>

            <div class="table-responsive">
                <table id="tableTradeList"
                       class="table table-striped table-hover table-no-ordered table-sm"
                       data-toolbar="#toolbar"
                       data-search="true"
                       data-show-refresh="true"
                       data-toggle="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-show-export="true"
                       data-sort-name="${sortName}"
                       data-sort-order="${sortOrder}"
                data-detail-view="true"
                %{--data-detail-formatter="detailFormatter"--}%
                       data-minimum-count-columns="2"
                       data-pagination="true"
                       data-side-pagination="server"
                       data-id-field="id"
                       data-page-list="[20, 50, 200]"
                       data-page-size="20"
                       data-show-footer="false"
                       data-url="${createLink(action: "json")}">
                </table>
            </div>
        </section><!--.box-typical-->

    %{--</div><!--.container-fluid-->--}%




<asset:javascript src="lib/bootstrap-table/bootstrap-table.js"/>
<asset:javascript src="lib/bootstrap-table/bootstrap-table-export.min.js"/>
<asset:javascript src="lib/bootstrap-table/tableExport.min.js"/>
<asset:javascript src="create-table.js"/>
<asset:javascript src="lib/bootstrap-sweetalert/sweetalert.min.js"/>


<script type="application/javascript">
    function typeFormatter(data, rowData, index) {
        var classBtn = 'label-default';
        if (rowData.inc == true) {
            classBtn = 'label-success';
            data = "<i class=\"fa fa-plus-circle\"></i> "+data;
        } else if (rowData.out == true) {
            classBtn = 'label-danger';
            data = "<i class=\"fa fa-minus-circle\"></i> "+data;
        } else {
            data = "<i class=\"fa fa-handshake-o\"></i> "+data;
        }
        return '<span class="label ' + classBtn + '">' + data + '</span>';
    }

    function commentCellStyle(value, row, index, field) {
        return {
            css: {
                "font-size": "13px"
            }
        };
    }

    $(document).ready(function () {
        createTable($('#tableTradeList'), $('#toolbar .bulk'),
            {
                responseHandler: function (data) {
                    return data;
                },
                columns: [
                    [
                        {field: 'state', checkbox: true, align: 'center', valign: 'middle'},

                        {field: "tradeDate", title: "Date", align: 'left', sortable: 'true', formatter: dateTimeFormatter},
                        {field: "type", title: "Type", align: 'center', formatter: typeFormatter},
                        {
                            field: "inAmount",
                            title: "In",
                            align: 'right',
                            formatter: emptyIfNull,
                            formatter: amountFormatter,
                            cellStyle: amountCellStyle,
                            sortable: 'true'
                        },
                        {
                            field: "outAmount",
                            title: "Out",
                            align: 'right',
                            formatter: emptyIfNull,
                            formatter: amountFormatter,
                            cellStyle: amountCellStyle,
                            sortable: 'true'
                        },
                        {field: "exchange", title: "Exchange", align: 'left', sortable: 'true'},
                        {
                            field: "comment",
                            title: "Comment",
                            align: 'left',
                            sortable: 'true',
                            cellStyle: commentCellStyle,
                            width: "100%"
                        },
                        // {field:"inValueFiat",title:"inValueFiat", align:'right', sortable: 'true', formatter: emptyIfNull},
                    ]
                ]
            },
            function(button, ids) {
                swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover the deleted transactions",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonClass: "btn-default",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: true
                },
                function(){
                    // TODO: timeout?
                    $('#tableTradeList').bootstrapTable("showLoading")
                    $.ajax("${g.createLink(controller: "trade", action: "bulk")}", {
                        data: {ids: ids.join(","), button: button},
                        success: function () {
                            $('#tableTradeList').bootstrapTable('refresh')
                        },
                        error: function (xhr, status, thrown) {
                            alert(status + " / " + thrown)
                            $('#tableTradeList').bootstrapTable("hideLoading")
                        }
                    })
                });
            }
        );
    });
</script>
</body>
</html>