<%@ page import="tools.StringTools; tools.UAgent" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Home</title>

    <script type="application/javascript">
    </script>
</head>

<body>

<div class="col-lg-9 col-md-9 col-sm-8">
    <div class="panel">
        <nav class="navbar" style="margin-bottom:0;margin-top:10px">
            <div class="container-fluid">
                <div class="navbar-header">
                    <span class="navbar-brand info-header"><span class="info-number">${credits}</span></span>
                    <span class="navbar-brand info-header" style="color: #666">Total acumulado: <span
                            style="color: #666;" class="info-number">${totalAccumulated}</span></span>
                </div>

                <g:if test="${ops}">
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            %{--<li><a href=""> Copiar todos</a></li>--}%
                            %{--
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">Acciones <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#"><i class="fa fa-trash fa-fw" aria-hidden="true"></i> Borrar todos</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            --}%
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><g:link controller="info" action="credits"><i class="fa fa-info-circle fa-fw"
                                                                              aria-hidden="true"></i> Ayuda</g:link>
                            </li>
                        </ul>
                    </div>
                </g:if>
            </div>
        </nav>

        <g:if test="${trades}">
            <style type="text/css">
                .amount {
                    padding-right: 3px !important;
                }
                .currency {
                    padding-left: 0px !important;
                }
            </style>

            <span class="small text-primary" style="padding-left:9px">${trades.size()} operaciones</span>
            <table class="table table-striped table-hover panel panel-info"
                   style="margin:0 auto 15px auto; width: 97.5%">
                <thead>
                <tr>
                    <th>Type</th>
                    <th colspan="2">In</th>
                    <th colspan="2">Pay</th>
                    <th colspan="2">Price</th>
                    <th colspan="2">Avg price</th>
                    <th colspan="2">Operation profit</th>
                    <th>Exchange</th>
                    <th>Comment</th>
                    <th colspan="2">Date</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${trades}" var="${trade}" idx="i">
                    <g:if test="${!params.coin || params.coin in [trade.inCurrency?.name, trade.outCurrency?.name]}">
                    <tr>
                        <g:set var="current" value="${trade.tradeDate.format("dd/MMM/yyyy")}"/>

                        <td class="text-center">
                            <g:if test="${trade.type.out}">
                                <span class="label label-big label-danger">${trade.type.name()}</span>
                            </g:if>
                            <g:elseif test="${trade.type.inc}">
                                <span class="label label-big label-success">${trade.type.name()}</span>
                            </g:elseif>
                            <g:else>
                                <span class="label label-big label-primary">${trade.type.name()}</span>
                            </g:else>
                        </td>
                        <cr:price amount="${trade.inAmount}" currency="${trade.inCurrency}"/>
                        <cr:price amount="${-(trade.outAmount?:0)}" currency="${trade.outCurrency}"/>
                        <cr:price amount="${trade.unitPrice}" class="small" currency=""/>
                        <g:if test="${trade.inNewAvgUnitPriceFiat && trade.inOldAvgUnitPriceFiat}">
                            <cr:discount
                                    data-toggle="tooltip" data-placement="top" title="${"New ${trade.inCurrency.name} avg. price: ${g.formatNumber(number:trade.inNewAvgUnitPriceFiat, format:"0.00")} ${currentUser.defaultCurrency}"}"
                                    amount="${trade.inNewAvgUnitPriceFiat - trade.inOldAvgUnitPriceFiat}" class="small" currency="${currentUser.defaultCurrency}"/>
                        </g:if>
                        <g:elseif test="${trade.inNewAvgUnitPriceFiat && !trade.inOldAvgUnitPriceFiat}">
                            <cr:price
                                    data-toggle="tooltip" data-placement="top" title="${"First buy! This is the current average price for ${trade.inCurrency.name}"}"
                                    class="small" amount="${trade.inNewAvgUnitPriceFiat}"  currency="${currentUser.defaultCurrency}"/>
                        </g:elseif>
                        <g:else>
                            <td colspan="2"></td>
                        </g:else>
                        <cr:profit amount="${(trade.inValueFiat?:0) - (trade.outCostFiat?:0)}" class="small" currency="${currentUser.defaultCurrency}"/>
                        <td class="">${trade.exchange}</td>
                        <td class="text-primary small">${trade.comment}</td>
                        <td class="text-nowrap">${current != last ? current : ""}</td>
                        <td class="small text-primary">${trade.tradeDate.format("HH:mm")}</td>
                    </tr>
                    <g:set var="last" value="${current}"/>
                    </g:if>
                </g:each>
                </tbody>
                <tfoot>
                    <td colspan="9"></td>
                    <cr:profit amount="${trades.sum { (it.inValueFiat?:0) - (it.outCostFiat?:0) }}" currency="${currentUser.defaultCurrency}"/>
                </tfoot>
            </table>
        </g:if>
        <g:else>
            <div class="text-center panel panel-default text-primary"
                 style="margin:0 15px 15px;padding:15px;">Sin operaciones.</div>
        </g:else>
    </div>
</div>
</body>
</html>
