<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Sign In</title>

    <asset:stylesheet src="lib/bootstrap-table/bootstrap-table.min.css"/>
    <style type="text/css">

    </style>

</head>

<body>
<div class="container-fluid">
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h3>${pairPrice.sfrom}-${pairPrice.sto} Prices<h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li><g:link controller="currency">Currencies</g:link></li>
                            <li><g:link controller="currency" action="show" id="${pairPrice.sfrom}">${pairPrice.sfrom}</g:link></li>
                            <li class="active">${pairPrice.sfrom}-${pairPrice.sto} Prices</li>
                        </ol>
                </div>
            </div>
        </div>
    </header>

    <section class="card">
        <div class="card-block">

            <div class="row">
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="form-label">Id ${pairPrice.id}</label>
                        <label class="form-label">${pairPrice.status?.name()}</label>
                        <label class="form-label">${pairPrice.message}</label>
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="form-label">Date created: ${pairPrice.dateCreated}</label>
                        <label class="form-label">Last updated: ${pairPrice.lastUpdated}</label>
                        <label class="form-label">Last import: ${pairPrice.lastImport}</label>
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="form-label">First price: ${pairPrice.firstPriceDate})</label>
                        <label class="form-label">Last price: ${pairPrice.lastPriceDate})</label>

                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <div>
%{--
                            <g:if test="${currency.enabled}">
                                <g:link action="bulk" params="[button:'disable', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                    <i class="font-icon font-icon-close-2"></i> Disable
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link action="bulk" params="[button:'enable', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="font-icon font-icon-check-bird"></i>
                                    Enable</g:link>
                            </g:else>
                            <g:link action="bulk" params="[button:'update', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="font-icon font-icon-refresh"></i> Update full
                            </g:link>
                            <g:link action="bulk" params="[button:'updateOutdatedPrices', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="fa fa-money"></i> Update outdated prices
                            </g:link>
                            <g:link action="bulk" params="[button:'updatePrices', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="fa fa-money"></i> Update full prices
                            </g:link>
--}%
                        </div>
                    </fieldset>

                </div>


            </div>
            <header class="box-typical-header" style="border-ottom: 1px solid #d8e2e7">
                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title" style="padding-bottom:0">
                        <h3>Prices</h3>
                    </div>
                </div>
            </header>

            <div id="toolbar">
%{--
                <button id="updateOutdatedPrices" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-refresh"></i> Update outdated prices
                </button>
                <button id="updatePrices" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-refresh"></i> Update full prices
                </button>
--}%
            </div>

            
            <section class="box-typical">
                <div class="table-responsive">
                    <table id="table"
                           class="table table-striped table-hover table-no-ordered table-sm"
                           data-toolbar="#toolbar"
                           data-search="true"
                           data-show-refresh="true"
                           data-toggle="true"
                           data-show-toggle="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-sort-name="${sortName}"
                           data-sort-order="${sortOrder}"
                           data-minimum-count-columns="2"
                           data-detail-view="true"
                           data-pagination="true"
                           data-side-pagination="server"
                           data-id-field="id"
                           data-page-list="[20, 50, 200]"
                           data-page-size="${limit}"
                           data-show-footer="false"
                           data-url="${createLink(action: "jsonPrice")}">
                    </table>
                </div>
            </section>
        </div>
    </section><!--.box-typical-->

</div><!--.container-fluid-->



<asset:javascript src="lib/bootstrap-table/bootstrap-table.js"/>
<asset:javascript src="lib/bootstrap-table/bootstrap-table-export.min.js"/>
<asset:javascript src="lib/bootstrap-table/tableExport.min.js"/>
<asset:javascript src="create-table.js"/>

<script>



    $(document).ready(function () {
        createTable($('#table'), $('#toolbar .bulk'),
            {
                responseHandler: function (data) {
                    return data;
                },
                queryParams: function (params) {
                    params.fromId = '${pairPrice.fromId}'
                    params.toId = '${pairPrice.toId}'
                    return params;
                },
                detailFormatter: createDetailFormatter('message'),
                columns: [
                    [
                        {field: 'state', checkbox: true, align: 'center', valign: 'middle'},
//                        {field: "startDate", title: "startDate", sortable: 'true', formatter: dateTimeFormatter},
                        {field: "id", title: "Id", sortable: 'true'},
                        {field: "date", title: "Date", sortable: 'true', formatter: dateFormatter},
                        {field: "price", title: "price", sortable: 'true'},
                    ]
                ]
            },
            "${g.createLink(controller: "currency", action: "priceBulk")}"
        );
    });
</script>
</body>
</html>