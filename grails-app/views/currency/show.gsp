<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Sign In</title>

    <asset:stylesheet src="lib/bootstrap-table/bootstrap-table.min.css"/>
    <style type="text/css">

    </style>

</head>

<body>
<div class="container-fluid">
    <g:if test="${currency}">
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h3>${currency.name}</h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li><g:link controller="currency">Currencies</g:link></li>
                            <li class="active">${currency.name}</li>
                        </ol>
                </div>
            </div>
        </div>
    </header>
    </g:if>

    <section class="card">
        <div class="card-block">
            <g:if test="${currency}">

            <header class="box-typical-header" style="border-ottom: 1px solid #d8e2e7">
                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title" style="padding-bottom:0">
                        <h3><img src='${currency.ccFullImageUrl()}' width='44'/>&nbsp; ${currency.displayName}</h3>
                    </div>
                </div>
            </header>
            <div class="row">
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="form-label">Id ${currency.id} <span class="color-red"><strong>${currency.enabled?"":"DISABLED"}</strong></span></label>
                        <label class="form-label"><span class="color-red"><strong>${currency.enabled && (currency.pairsFrom+currency.pairsTo) > 0 ?"":"NO VISIBLE"}</strong></span></label>
                        <label class="form-label">Media: ${currency.mediaOk}</label>
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="form-label">Date created: ${currency.dateCreated}</label>
                        <label class="form-label">Last updated: ${currency.lastUpdated}</label>
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="form-label">Pairs: ${currency.pairsFrom+currency.pairsTo} (${currency.pairsFrom}+${currency.pairsTo})</label>
                        <label class="form-label">Pairs wrong: <strong class="color-red">${currency.pairsWrong?:""}</strong></label>

                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label class="form-label">Status: ${currency.status?.name()}</label>
                        <label class="form-label">Pair list status: ${currency.pairsStatus?.name()}</label>
                        <label class="form-label">Message: ${currency.message}</label>
                        <div>
                            %{--<span class="small">Bulk actions</span>--}%
                            <g:if test="${currency.enabled}">
                                <g:link action="bulk" params="[button:'disable', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                    <i class="font-icon font-icon-close-2"></i> Disable
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link action="bulk" params="[button:'enable', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="font-icon font-icon-check-bird"></i>
                                    Enable</g:link>
                            </g:else>
                            <g:link action="bulk" params="[button:'update', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="font-icon font-icon-refresh"></i> Update full
                            </g:link>
                            <g:link action="bulk" params="[button:'updateOutdatedPrices', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="fa fa-money"></i> Update outdated prices
                            </g:link>
                            <g:link action="bulk" params="[button:'updatePrices', ids:currency.id, from:'show']" class="bulk btn btn-secondary btn-sm">
                                <i class="fa fa-money"></i> Update full prices
                            </g:link>
                        </div>
                    </fieldset>

                </div>


            </div>
            </g:if>
            <header class="box-typical-header" style="border-ottom: 1px solid #d8e2e7">
                <p style="float:right">
                    Sin error y visibles que no están actualizados: <b>actualizar ya</b> &raquo; <a href="javascript:false" onclick="setLinkToolbarSearch($(this))">is:ok is:visible !is:updated</a><br/>
                    Con error y visibles: <b>revisar y corregir</b> &raquo; <a href="javascript:false" onclick="setLinkToolbarSearch($(this))">is:visible is:error</a>
                </p>
                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title" style="padding-bottom:0">
                        <h3>Pairs</h3>
                    </div>
                </div>
            </header>

            <div id="toolbar">
%{--
                                <button id="enable" class="bulk btn btn-secondary btn-sm" disabled>
                                <i class="font-icon font-icon-check-bird"></i>
                                    Enable</button>
                                <button id="disable" class="bulk btn btn-secondary btn-sm" disabled>
                                    <i class="font-icon font-icon-close-2"></i> Disable
                                </button>
--}%
                <button id="updateOutdatedPrices" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-refresh"></i> Update outdated prices
                </button>
                <button id="updatePrices" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-refresh"></i> Update full prices
                </button>
                <button id="resetPrices" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-refresh"></i> Reset
                </button>
                <br/>
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:error</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:ok</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:updated</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">!is:updated</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:visible</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">!is:visible</a> |
                <a href="javascript:false" onclick="setToolbarSearch('')">(clear)</a>

            </div>

            
            <section class="box-typical">
                <div class="table-responsive">
                    <table id="table"
                           class="table table-striped table-hover table-no-ordered table-sm"
                           data-toolbar="#toolbar"
                           data-search="true"
                           data-show-refresh="true"
                           data-toggle="true"
                           data-show-toggle="true"
                           data-show-columns="true"
                           data-show-export="true"
                           data-sort-name="${sortName}"
                           data-sort-order="${sortOrder}"
                           data-minimum-count-columns="2"
                           data-detail-view="true"
                           data-pagination="true"
                           data-side-pagination="server"
                           data-id-field="id"
                           data-page-list="[20, 50, 200]"
                           data-page-size="${limit}"
                           data-show-footer="false"
                           data-url="${createLink(action: "jsonPairs")}">
                    </table>
                </div>
            </section>
        </div>
    </section><!--.box-typical-->

</div><!--.container-fluid-->



<asset:javascript src="lib/bootstrap-table/bootstrap-table.js"/>
<asset:javascript src="lib/bootstrap-table/bootstrap-table-export.min.js"/>
<asset:javascript src="lib/bootstrap-table/tableExport.min.js"/>
<asset:javascript src="create-table.js"/>

<script>

    function goToCurrencyFromFormatter(data, row, index) {
        if (data == '${currency?.name}') return data
        return "<a class='' href='${createLink(action: "show")}/"+row.sfrom+"'>"+data+"</a>";
    }

    function goToPair(data, row, index) {
        return "<a class='' href='${createLink(action: "show")}/"+row.sfrom+"-"+row.sto+"'>"+data+"</a>";
    }
    function goToCurrencyToFormatter(data, row, index) {
        if (data == '${currency?.name}') return data
        return "<a class='' href='${createLink(action: "show")}/"+row.sto+"'>"+data+"</a>";
    }

    var $table
    var setToolbarSearch = function(data) {
        $table.bootstrapTable('resetSearch', data);
        //$("button[name=refresh]").click();
    }
    var addLinkToToolbarSearch = function(e) {
        var ele = $(".search").find("input[type=text]");
        setToolbarSearch((ele.val()+" "+e.text()).trim())
    }
    var setLinkToolbarSearch = function(e) {
        setToolbarSearch(e.text())
    }

    $(document).ready(function () {
        $table = createTable($('#table'), $('#toolbar .bulk'),
            {
                responseHandler: function (data) {
                    return data;
                },
                queryParams: function (params) {
                    params.id = '${currency?.id}'
                    return params;
                },
                detailFormatter: createDetailFormatter('message'),
                columns: [
                    [
                        {field: 'state', checkbox: true, align: 'center', valign: 'middle'},
                        {field: 'id', align: 'left', valign: 'middle', formatter: goToPair},
                        {field: "sfrom", title: "From", align: 'left', sortable: 'true', formatter: goToCurrencyFromFormatter},
                        {field: "sto", title: "To", align: 'left', sortable: 'true', formatter: goToCurrencyToFormatter},
//                        {field: "startDate", title: "startDate", sortable: 'true', formatter: dateTimeFormatter},
                        {field: "dateCreated", title: "Date created", sortable: 'true', formatter: dateTimeFormatter},
                        {field: "lastUpdated", title: "Last updated", sortable: 'true', formatter: dateTimeFormatter},
                        {field: "lastImport", title: "Last price import", sortable: 'true', formatter: dateTimeFormatter},
                        {field: "status", title: "status", sortable: 'true'},
                        {field: "message", title: "Message", sortable: 'true', formatter: trimFormatter},
                        {field: "firstPriceDate", title: "First price", sortable: 'true', formatter: dateFormatter},
                        {field: "lastPriceDate", title: "Last price", sortable: 'true', formatter: dateFormatter},
                    ]
                ]
            },
            "${g.createLink(controller: "currency", action: "pairBulk")}"
        );
    });
</script>
</body>
</html>