<!doctype html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>Sign In</title>

    <asset:stylesheet src="lib/bootstrap-table/bootstrap-table.min.css"/>
    <style type="text/css">

    </style>

</head>

<body>
    <div class="container-fluid">
        <section class="box-typical">
            <header class="box-typical-header" style="border-bottom: 1px solid #d8e2e7">
                <div class="tbl-row">
                    <p style="float:right">
                        Cryptos visibles &raquo; <a href="javascript:false" onclick="setLinkToolbarSearch($(this))">is:crypto is:visible</a><br/>
                        Cryptos visibles con error: <b>full uppdate</b> &raquo; <a href="javascript:false" onclick="setLinkToolbarSearch($(this))">is:crypto is:error is:visible</a><br/>
                        Fiat visibles &raquo; <a href="javascript:false" onclick="setLinkToolbarSearch($(this))">is:fiat is:visible</a><br/>
                        Fiat visibles con error: <b>full uppdate</b> &raquo; <a href="javascript:false" onclick="setLinkToolbarSearch($(this))">is:fiat is:error is:visible</a><br/>
                    </p>

                    <div class="tbl-cell tbl-cell-title" style="padding-bottom:0;float:left">
                        <h1>Currencies</h1>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <g:link action="updateFiat" class="action-btn btn-sm">Update fiat</g:link>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <g:link action="updateCrypto" class="action-btn btn-sm" params="[full:false]">Update cryptos</g:link>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <g:link action="updateCrypto" class="action-btn btn-sm" params="[full:true]">Update cryptos (full+pairs)</g:link>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <g:link action="updatePairsOutdated" class="action-btn btn-sm" params="[full:true]">Update all prices (enabled and outdated/error only)</g:link>
                    </div>
                </div>
            </header>
            <div id="toolbar">
                <button id="enable" class="bulk btn btn-secondary btn-sm" disabled>
                <i class="font-icon font-icon-check-bird"></i>
                    Enable</button>
                <button id="disable" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-close-2"></i> Disable
                </button>
                <button id="update" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-refresh"></i> Update full info
                </button>
                <button id="updateOutdatedPrices" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="font-icon font-icon-refresh"></i> Update outdated prices
                </button>
                <button id="updatePrices" class="bulk btn btn-secondary btn-sm" disabled>
                    <i class="fa fa-money"></i> Update full prices
                </button>
                <br/>
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:error</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:ok</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:enabled</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">!is:enabled</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:visible</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">!is:visible</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:media</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">!is:media</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:fiat</a> |
                <a href="javascript:false" onclick="addLinkToToolbarSearch($(this))">is:crypto</a> |
                <a href="javascript:false" onclick="setToolbarSearch('')">(clear)</a>
            </div>

            <div class="table-responsive">
                <table id="table"
                       class="table table-striped table-hover table-no-ordered table-sm"
                       data-toolbar="#toolbar"
                       data-search="true"
                       data-show-refresh="true"
                       data-toggle="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-show-export="true"
                       data-sort-name="${sortName}"
                       data-sort-order="${sortOrder}"
                       data-detail-view="true"
                       data-minimum-count-columns="2"
                       data-pagination="true"
                       data-side-pagination="server"
                       data-id-field="id"
                       data-page-list="[20, 50, 200]"
                       data-page-size="${limit}"
                       data-show-footer="false"
                       data-url="${createLink(action: "json")}">
                </table>
            </div>
        </section><!--.box-typical-->

    </div><!--.container-fluid-->



<asset:javascript src="lib/bootstrap-table/bootstrap-table.js"/>
<asset:javascript src="lib/bootstrap-table/bootstrap-table-export.min.js"/>
<asset:javascript src="lib/bootstrap-table/tableExport.min.js"/>
<asset:javascript src="create-table.js"/>

<script>
    function goToCurrencyFormatter(data, row, index) {
        return "<a class='' href='${createLink(action: "show")}/"+row.name+"'>"+data+"</a>";
    }

    function logoFormatter(data, row, index) {
        return "<img src='"+data+"' width='24'/>"
    }

    function detailFormatter(index, row, $detail) {
        $detail.html('Loading, please wait...');
        $.get('${createLink(action: "pairs")}?id='+row.id, function (res) {
            $detail.html(res);
//            $detail.html(res.replace(/\n/g, '<br>'));
        });
    }

    var $table
    var setToolbarSearch = function(data) {
        $table.bootstrapTable('resetSearch', data);
        //$("button[name=refresh]").click();
    }
    var addLinkToToolbarSearch = function(e) {
        var ele = $(".search").find("input[type=text]");
        setToolbarSearch((ele.val()+" "+e.text()).trim())
    }
    var setLinkToolbarSearch = function(e) {
        setToolbarSearch(e.text())
    }

    
    $(document).ready(function () {
        $table = createTable($('#table'), $('#toolbar .bulk'),
            {
                responseHandler: function (data) {
                    return data;
                },
                detailFormatter: createDetailFormatter('message'),
                columns: [
                    [
                        {field: 'state', checkbox: true, align: 'center', valign: 'middle'},
                        {field: "fullImageUrl", title: "Logo", align: 'center', formatter: logoFormatter},
                        {field: "rank", title: "Rank", align: 'right', sortable: 'true'},
                        {field: "name", title: "Name", align: 'left', sortable: 'true', formatter: goToCurrencyFormatter},
                        {field: "enabled", title: "Enabled", align: 'center', sortable: 'true', formatter: booleanFormatter},
//                        {field: "displayName", title: "Display name", align: 'left', sortable: 'true'},


                        {field: "status", title: "Status", align: 'center', sortable: 'true'},
                        {field: "pairsStatus", title: "Pairs status", align: 'center', sortable: 'true'},
                        {field: "message", title: "Message", sortable: 'true', formatter: trimFormatter},
                        {field: "pairsWrong", title: "Pairs wrong", align: 'right', sortable: 'true', formatter: numberZeroEmptyFormatter},
                        {field: "mediaOk", title: "Media", align: 'center', sortable: 'true', formatter: booleanFormatter},
//                        {field: "startDate", title: "startDate", sortable: 'true', formatter: dateTimeFormatter},
                        {field: "pairsFrom", title: "Pairs from", align: 'right', sortable: 'true', formatter: numberZeroEmptyFormatter},
                        {field: "pairsTo", title: "Pairs to", align: 'right', sortable: 'true', formatter: numberZeroEmptyFormatter},
                        {field: "dateCreated", title: "Date created", sortable: 'true', formatter: dateTimeFormatter},
                        {field: "lastUpdated", title: "Last updated", sortable: 'true', formatter: dateTimeFormatter}

                        // {field:"inAmount",   title:"In",       align:'right', formatter: emptyIfNull, formatter: amountFormatter, cellStyle: amountCellStyle, sortable: 'true'},
                        // {field:"outAmount",  title:"Out",      align:'right', formatter: emptyIfNull, formatter: amountFormatter, cellStyle: amountCellStyle, sortable: 'true'},
                        // {field:"exchange",   title:"Exchange", align:'left',  sortable: 'true'},
                        // {field:"tradeDate",  title:"Date",     align:'left',  sortable: 'true', formatter: dateTimeFormatter},
                        // {field:"comment",    title:"Comment",  align:'left',  sortable: 'true', cellStyle: commentCellStyle},
                    ]
                ]
            },
            "${g.createLink(controller: "currency", action: "bulk")}"
        );
    });
</script>
</body>
</html>