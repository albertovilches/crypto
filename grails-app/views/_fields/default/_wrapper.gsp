<div class="form-group">
    <label class="col-lg-2 control-label">${property}</label>
    <div class="col-lg-10">
        ${raw(widget)}
    </div>
</div>