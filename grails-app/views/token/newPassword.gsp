<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Sign In</title>
</head>

<body>
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <g:form class="sign-box " action="change" style="text-align: center">
                <g:hiddenField name="uuid" value="${uuid}"/>
                <g:hiddenField name="token" value="${token}"/>
                <!--<div class="sign-avatar">
                    <img src="img/avatar-sign.png" alt="">
                </div>-->
                <g:if test="${user.hasPassword()}">
                    <header class="sign-title">Reset Password</header>
                </g:if>
                <g:else>
                    <header class="sign-title">Ending sign up...</header>
                </g:else>
                <p>Create a new password for user ${user.email}</p>

                <div class="form-group">
                    <input type="password" class="form-control" name="newPwd" placeholder="New Password"/>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="newPwd2" placeholder="Confirm New Password"/>
                </div>
                <button type="submit" class="btn btn-rounded btn-block">Create password and enter</button>
                <div style="height: 50px">
                    <g:if test="${flash.error}">
                        <div class="alert alert-danger alert-fill alert-close alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            ${flash.error}
                        </div>
                    </g:if>
                </div>
            </g:form>
        </div>
    </div>
</div><!--.page-center-->
<script>
    $('#newPwd').focus()
    $('[data-toggle="tooltip"]').tooltip()
</script>

</body>
</html>