<!doctype html>
<html>
<head>
    <meta name="layout" content="nomenu"/>
    <title>Error 500 - Server error</title>
</head>

<body>
<div>

    <div class="row">
        <div class="col-lg-3">
        </div>

        <div class="col-lg-6">

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Error 500 - Server Error</h3>
                </div>

                <div class="panel-body" style="text-align: center">
                    <p>Parece que ha ocurrido un error en el servidor. No te preocupes, los administradores han sido reportados y en algún momento se arreglará.</p>
                    <p>Mientras tanto siempre te puedes echar una partidita...</p>

                    <g:link class="btn btn-default" uri="/">Ir a la home</g:link>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3">
        </div>

        <div class="col-lg-6">

        </div>
    </div>

    <g:if env="development">
    <div class="row">
        <div class="col-lg-12" style="background-color: white; color: black">
                <g:if test="${Throwable.isInstance(exception)}">
                    <g:renderException exception="${exception}"/>
                </g:if>
                <g:elseif test="${request.getAttribute('javax.servlet.error.exception')}">
                    <g:renderException exception="${request.getAttribute('javax.servlet.error.exception')}"/>
                </g:elseif>
                <g:else>
                    <ul class="errors">
                        <li>An error has occurred</li>
                        <li>Exception: ${exception}</li>
                        <li>Message: ${message}</li>
                        <li>Path: ${path}</li>
                    </ul>
                </g:else>
        </div>
    </div>
    </g:if>

</div>

</body>
</html>
