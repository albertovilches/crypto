<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <h1><g:message code="default.create.label" args="[entityName]"/></h1>
            <div class="bs-component">
                <ul class="breadcrumb">
                    <li><g:link controller="list">Home</g:link></li>
                    <li><g:link controller="tag">tag list</g:link></li>
                    <li class="active">Create tag</li>
                </ul>
            </div>
        </div>

        <div class="col-lg-12">
            <g:hasErrors bean="${this.tag}">
                <div class="container">
                    <div class="row">
                        <div class="bs-component">
                            <div class="alert alert-dismissible alert-danger" style="margin-top:20px">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <ul class="errors" role="alert">
                                    <g:eachError bean="${this.tag}" var="error">
                                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                                error="${error}"/></li>
                                    </g:eachError>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </g:hasErrors>
            <div class="row">
                <div class="col-lg-6">
                    <div class="well bs-component">
                        <g:form action="save" class="form-horizontal">
                            <fieldset class="form">
                                <f:all bean="tag"/>
                            </fieldset>
                            <fieldset class="buttons">
                                <g:submitButton name="create" class="save"
                                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
