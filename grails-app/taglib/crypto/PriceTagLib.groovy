package crypto

class PriceTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    CurrencyService currencyService

    static namespace = "cr"

    def profit = { attrs, body ->
        attrs.positive = "text-success"
        attrs.negative = "text-danger"
        out << biColor(attrs)
    }
    def discount = { attrs, body ->
        attrs.positive = "text-danger"
        attrs.negative = "text-success"
        out << biColor(attrs)
    }

    private biColor(attrs) {
        def amount = attrs.remove("amount")
        BigDecimal price = amount instanceof Number ? amount as BigDecimal : null
        def currency = attrs.remove("currency")?:""
        String format = attrs.remove("format")
        String styleClass = attrs.remove("class")?:""
        String tdClass = attrs.remove("tdClass")?:""
        String positive = attrs.remove("positive")?:"text-success"
        String negative = attrs.remove("negative")?:"text-danger"
        BigDecimal zero = attrs.remove("zero")?:0

        if (!attrs.containsKey("sorttable_customkey") && price != null) {
            attrs.sorttable_customkey = price
        }

        String extra = ""
        String extraTD = ""
        for (String key in attrs.keySet()) {
            if (key in TD_ONLY) {
                extraTD += " ${key}=\"${attrs[key]?.encodeAsHTML()}\""
            } else {
                extra += " ${key}=\"${attrs[key]?.encodeAsHTML()}\""
            }
        }

        def data = ""
        def rule = ""
        def equalsToZero = price?.compareTo(zero)?:0
        if (equalsToZero != 0) {
            data = currencyService.formatPrice(price, currency, format)
            if (equalsToZero > 0) {
                rule = positive
            } else {
                rule = negative
            }
        }

        return """
<td ${extraTD} class="text-nowrap text-right amount ${rule} ${tdClass}"><span class="${styleClass}" ${extra}>${data}</span>
<span class="currency">${data ? currency:""}</span></td>"""
    }

    Set TD_ONLY = ["sorttable_customkey"] as Set

    def format = { attrs, body ->
        def amount = attrs.remove("amount")
        def currency = attrs.remove("currency")?:""
        String format = attrs.remove("format")
        out << currencyService.formatPrice(amount, currency, format)
    }
    def price = { attrs, body ->

        def amount = attrs.remove("amount")
        BigDecimal price = amount instanceof Number ? amount as BigDecimal : null
        def currency = attrs.remove("currency")?:""
        String format = attrs.remove("format")
        String styleClass = attrs.remove("class")?:""
        String tdClass = attrs.remove("tdClass")?:""

        if (!attrs.containsKey("sorttable_customkey") && price != null) {
            attrs.sorttable_customkey = price
        }

        String extra = ""
        String extraTD = ""
        for (String key in attrs.keySet()) {
            if (key in TD_ONLY) {
                extraTD += " ${key}=\"${attrs[key]?.encodeAsHTML()}\""
            } else {
                extra += " ${key}=\"${attrs[key]?.encodeAsHTML()}\""
            }
        }

        amount = amount instanceof String && amount.startsWith("loaing") ? "<i class='fa fa-spinner fa-pulse' aria-hidden='true'></i>" : amount

        def data = amount instanceof Number ? currencyService.formatPrice(price, currency, format) : amount

        out << """
<td ${extraTD} class="text-nowrap text-right amount ${tdClass}"><span class="${styleClass}" ${extra}>${data}</span>
<span class="currency">${currency}</span></td>"""
    }
}
