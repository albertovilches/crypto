package mamespin

import grails.core.GrailsDomainClass
import grails.plugin.formfields.FormFieldsTagLib
import grails.util.TypeConvertingMap
import grails.web.mapping.UrlMapping
import org.grails.taglib.TagOutput
import org.grails.taglib.encoder.OutputContextLookupHelper
import org.grails.validation.DomainClassPropertyComparator
import org.springframework.web.servlet.support.RequestContextUtils

class MyTagLib {
    static defaultEncodeAs = [taglib: 'raw']

    static namespace = "my"
    CacheService cacheService

    def cache = { attrs, body ->
        out << cacheService.block(attrs.key, body)
    }

    def html = { Map attrs ->
        out << cacheService.read(attrs.file)
    }

    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    /**
     * Creates next/previous links to support pagination for the current controller.<br/>
     *
     * &lt;g:paginate total="${Account.count()}" /&gt;<br/>
     *
     * @emptyTag
     *
     * @attr total REQUIRED The total number of results to paginate
     * @attr action the name of the action to use in the link, if not specified the default action will be linked
     * @attr controller the name of the controller to use in the link, if not specified the current controller will be linked
     * @attr id The id to use in the link
     * @attr params A map containing request parameters
     * @attr prev The text to display for the previous link (defaults to "Previous" as defined by default.paginate.prev property in I18n messages.properties)
     * @attr next The text to display for the next link (defaults to "Next" as defined by default.paginate.next property in I18n messages.properties)
     * @attr omitPrev Whether to not show the previous link (if set to true, the previous link will not be shown)
     * @attr omitNext Whether to not show the next link (if set to true, the next link will not be shown)
     * @attr omitFirst Whether to not show the first link (if set to true, the first link will not be shown)
     * @attr omitLast Whether to not show the last link (if set to true, the last link will not be shown)
     * @attr max The number of records displayed per page (defaults to 10). Used ONLY if params.max is empty
     * @attr maxsteps The number of steps displayed for pagination (defaults to 10). Used ONLY if params.maxsteps is empty
     * @attr offset Used only if params.offset is empty
     * @attr mapping The named URL mapping to use to rewrite the link
     * @attr fragment The link fragment (often called anchor tag) to use
     */
    Closure paginate = { Map attrsMap ->
        TypeConvertingMap attrs = (TypeConvertingMap) attrsMap
        def writer = out
        if (attrs.total == null) {
            throwTagError("Tag [paginate] is missing required attribute [total]")
        }

        def messageSource = grailsAttributes.messageSource
        def locale = RequestContextUtils.getLocale(request)

        def total = attrs.int('total') ?: 0
        def offset = attrs.int('offset') ?: 0
        def max = attrs.int('max')
        def maxsteps = (attrs.int('maxsteps') ?: 10)

        if (!offset) offset = (params.int('offset') ?: 0)
        if (!max) max = (params.int('max') ?: 10)

        Map linkParams = [:]
        if (attrs.params instanceof Map) linkParams.putAll((Map) attrs.params)
        linkParams.offset = offset - max
        if (params.int('max')) {
            linkParams.max = max
        }
        if (attrs.sort) linkParams.sort = attrs.sort
        else if (params.sort) linkParams.sort = params.sort
        if (attrs.order) linkParams.order = attrs.order
        else if (params.order) linkParams.order = params.order

        Map linkTagAttrs = [:]
        def action
        if (attrs.containsKey('mapping')) {
            linkTagAttrs.mapping = attrs.mapping
            action = attrs.action
        } else {
            action = attrs.action ?: params.action
        }
        if (action) {
            linkTagAttrs.action = action
        }
        if (attrs.controller) {
            linkTagAttrs.controller = attrs.controller
        }
        if (attrs.containsKey(UrlMapping.PLUGIN)) {
            linkTagAttrs.put(UrlMapping.PLUGIN, attrs.get(UrlMapping.PLUGIN))
        }
        if (attrs.containsKey(UrlMapping.NAMESPACE)) {
            linkTagAttrs.put(UrlMapping.NAMESPACE, attrs.get(UrlMapping.NAMESPACE))
        }
        if (attrs.id != null) {
            linkTagAttrs.id = attrs.id
        }
        if (attrs.fragment != null) {
            linkTagAttrs.fragment = attrs.fragment
        }
        linkTagAttrs.params = linkParams

        // determine paging variables
        def steps = maxsteps > 0
        int currentstep = ((offset / max) as int) + 1
        int firststep = 1
        int laststep = Math.round(Math.ceil(total / max)) as int

        if (currentstep == 1 && laststep == 1) return

        writer << "<ul class=\"pagination\">"

        // display previous link when not on firststep unless omitPrev is true
        if (currentstep > firststep && !attrs.boolean('omitPrev')) {
            linkTagAttrs.put('class', 'prevLink')
            linkParams.offset = offset - max
            writer << callLink((Map) linkTagAttrs.clone()) {
                (attrs.prev ?: messageSource.getMessage('paginate.prev', null, messageSource.getMessage('default.paginate.prev', null, 'Previous', locale), locale))
            }
        }

        // display steps when steps are enabled and laststep is not firststep
        if (steps && laststep > firststep) {
            linkTagAttrs.put('class', 'step')

            // determine begin and endstep paging variables
            int beginstep = currentstep - (Math.round(maxsteps / 2.0d) as int) + (maxsteps % 2)
            int endstep = currentstep + (Math.round(maxsteps / 2.0d) as int) - 1

            if (beginstep < firststep) {
                beginstep = firststep
                endstep = maxsteps
            }
            if (endstep > laststep) {
                beginstep = laststep - maxsteps + 1
                if (beginstep < firststep) {
                    beginstep = firststep
                }
                endstep = laststep
            }

            // display firststep link when beginstep is not firststep
            if (beginstep > firststep && !attrs.boolean('omitFirst')) {
                linkParams.offset = 0
                writer << callLink((Map) linkTagAttrs.clone()) { firststep.toString() }
            }
            //show a gap if beginstep isn't immediately after firststep, and if were not omitting first or rev
            if (beginstep > firststep + 1 && (!attrs.boolean('omitFirst') || !attrs.boolean('omitPrev'))) {
                writer << '<li class="step gap">..</li>'
            }

            // display paginate steps
            (beginstep..endstep).each { int i ->
                if (currentstep == i) {
                    writer << "<li class=\"active\"><a href=''>${i}</a></li>"
                } else {
                    linkParams.offset = (i - 1) * max
                    writer << callLink((Map) linkTagAttrs.clone()) { i.toString() }
                }
            }

            //show a gap if beginstep isn't immediately before firststep, and if were not omitting first or rev
            if (endstep + 1 < laststep && (!attrs.boolean('omitLast') || !attrs.boolean('omitNext'))) {
                writer << '<li class="step gap">..</li>'
            }
            // display laststep link when endstep is not laststep
            if (endstep < laststep && !attrs.boolean('omitLast')) {
                linkParams.offset = (laststep - 1) * max
                writer << callLink((Map) linkTagAttrs.clone()) { laststep.toString() }
            }
        }

        // display next link when not on laststep unless omitNext is true
        if (currentstep < laststep && !attrs.boolean('omitNext')) {
            linkTagAttrs.put('class', 'nextLink')
            linkParams.offset = offset + max
            writer << callLink((Map) linkTagAttrs.clone()) {
                (attrs.next ? attrs.next : messageSource.getMessage('paginate.next', null, messageSource.getMessage('default.paginate.next', null, 'Next', locale), locale))
            }
        }
        writer << "</ul>"
    }

    private callLink(Map attrs, Object body) {
        "<li>" + TagOutput.captureTagOutput(tagLibraryLookup, 'g', 'link', attrs, body, OutputContextLookupHelper.lookupOutputContext()) + "</li>"
    }

    /**
     * Renders a collection of beans in a table
     *
     * @attr collection REQUIRED The collection of beans to render
     * @attr domainClass The FQN of the domain class of the elements in the collection.
     * Defaults to the class of the first element in the collection.
     * @attr properties The list of properties to be shown (table columns).
     * Defaults to the first 7 (or less) properties of the domain class ordered by the domain class' constraints.
     * @attr displayStyle OPTIONAL Determines the display template used for the bean's properties.
     * Defaults to 'table', meaning that 'display-table' templates will be used when available.
     */
    def table = { attrs, body ->
        def collection = resolveBean(attrs.remove('collection'))
        def domainClass
        if (attrs.containsKey('domainClass')) {
            domainClass = grailsApplication.getDomainClass(attrs.remove('domainClass'))
        } else {
            domainClass = (collection instanceof Collection) && collection ? resolveDomainClass(collection.iterator().next()) : null
        }
        if (domainClass) {
            def properties
            if (attrs.containsKey('properties')) {
                properties = attrs.remove('properties').collect { domainClass.getPropertyByName(it) }
            } else {
                properties = domainClass.persistentProperties.sort(new DomainClassPropertyComparator(domainClass))
                if (properties.size() > 6) {
                    properties = properties[0..6]
                }
            }
            def displayStyle = attrs.remove('displayStyle')
            out << render(template: "/templates/_fields/mytable",
                    model: [controllerName: attrs.controllerName, domainClass: domainClass, domainProperties: properties, collection: collection, displayStyle: displayStyle])
        }
    }
    private resolvePageScopeVariable(attributeName) {
        // Tomcat throws NPE if you query pageScope for null/empty values
        attributeName?.toString() ? pageScope.variables[attributeName] : null
    }

    private FormFieldsTagLib.BeanAndPrefix resolveBeanAndPrefix(beanAttribute, prefixAttribute) {
        def bean = resolvePageScopeVariable(beanAttribute) ?: beanAttribute
        def prefix = resolvePageScopeVariable(prefixAttribute) ?: prefixAttribute
        new FormFieldsTagLib.BeanAndPrefix(bean: bean, prefix: prefix)
    }

	private Object resolveBean(beanAttribute) {
        resolvePageScopeVariable(beanAttribute) ?: beanAttribute ?: beanStack.bean
	}

    private String resolvePrefix(prefixAttribute) {
        def prefix = resolvePageScopeVariable(prefixAttribute) ?: prefixAttribute ?: beanStack.prefix
		if (prefix && !prefix.endsWith('.'))
			prefix = prefix + '.'
		prefix ?: ''
	}

    private GrailsDomainClass resolveDomainClass(bean) {
   		resolveDomainClass(bean.getClass())
   	}

   	private GrailsDomainClass resolveDomainClass(Class beanClass) {
   		grailsApplication.getDomainClass(beanClass.name)
   	}

    static final String STACK_PAGE_SCOPE_VARIABLE = 'f:with:stack'

    FormFieldsTagLib.BeanAndPrefixStack getBeanStack() {
        if (!pageScope.hasVariable(STACK_PAGE_SCOPE_VARIABLE)) {
            pageScope.setVariable(STACK_PAGE_SCOPE_VARIABLE, new FormFieldsTagLib.BeanAndPrefixStack())
        }
        pageScope.variables[STACK_PAGE_SCOPE_VARIABLE]
    }





}

