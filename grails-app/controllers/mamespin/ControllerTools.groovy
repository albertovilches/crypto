package mamespin

import crypto.CurrencyService
import crypto.HoldingService
import crypto.ImportCurrenciesService
import crypto.ImportPricesService
import crypto.PriceService
import crypto.TradeService
import grails.config.Config
import grails.core.GrailsApplication
import groovy.transform.CompileStatic

/*
 * @author Alberto Vilches
 * @date 17/12/2016
 */

trait ControllerTools {

    GrailsApplication grailsApplication
    SessionService sessionService
    TradeService tradeService
    HoldingService holdingService
    PriceService priceService
    ImportPricesService importPricesService
    ImportCurrenciesService importCurrenciesService
    CurrencyService currencyService

    @CompileStatic
    User getCurrentUser() {
        return sessionService.user
    }

    String getRequestIp() {
        return request.ip
    }

    @CompileStatic
    Config getConfig() {
        grailsApplication.config
    }

    void redirectToHome() {
        redirect uri: "/"
    }

    void addError(String m) {
        flash.errors = flash.errors?flash.errors+[m]:[m]
    }

}