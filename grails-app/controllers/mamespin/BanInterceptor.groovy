/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin

class BanInterceptor implements ControllerTools {
    BanInterceptor() {
        match(controller:"sign")
        match(controller:"home")
    }

    boolean before() {
        if (params.removeBan) {
            BanIp.removeBan(params.removeBan == "my"?request.ip:params.removeBan)
        }

        Date until = null; //BanIp.isBannedUntil(request.ip)
        if (until) {
            render view: "/banned", model: [remaining: ((until.time - new Date().time) / 1000) as int]
            return false
        }
        return true
    }


}