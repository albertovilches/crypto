/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin.auth.interceptors

import grails.core.GrailsControllerClass
import grails.util.Holders
import mamespin.ControllerTools

class NotSignedInInterceptor implements ControllerTools {

    NotSignedInInterceptor() {
        Holders.findApplication().getArtefacts("Controller").each { GrailsControllerClass controllerClass ->
            if (controllerClass.namespace == "notSigned") {
                match(controller: controllerClass.name.with { it[0].toLowerCase()+it[1..-1]})
            }
        }
    }

    boolean before() {
        if (currentUser) {
            redirectToHome()
            return false
        }
        return true
    }

}