/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin.auth

import mamespin.ControllerTools
import mamespin.SignService
import mamespin.User

class SignController implements ControllerTools {

    static namespace = "notSigned"
    static defaultAction = "in"

    SignService signService

/*
    def index(String nextUrl) {
        redirect(action: "in", params:[nextUrl: nextUrl])
    }
*/

    def 'in'(String nextUrl) {
        [:]
    }

    def check(String n, String pwd, String nextUrl, boolean rememberMe) {
        User user = signService.signIn(requestIp, n, pwd, rememberMe)
        if (user) {
            session.removeAttribute("loginFails")
            if (nextUrl) {
                redirect uri:nextUrl
            } else {
                redirectToHome()
            }
        } else {
            session.loginFails = (session.loginFails?:0)+1
            if (session.loginFails >= 2) {
                flash.error = "Usuario, correo o contraseña incorrecto.<br/><br/>Si no te acuerdas de tu contraseña, podemos <a href='${g.createLink(controller:"recover", action: "email", params: [n:n])}'>enviarte un correo</a> para que crees una nueva."
            } else {
                flash.error = "Usuario, correo o contraseña incorrecto."

            }
                    
            redirect(action: "in", params: [nextUrl: nextUrl, n: n])
        }
    }

}