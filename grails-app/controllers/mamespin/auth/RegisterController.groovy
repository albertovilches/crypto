/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin.auth

import mamespin.ControllerTools
import mamespin.SignService
import mamespin.User

class RegisterController implements ControllerTools {

    static namespace = "notSigned"
    static defaultAction = "me"

    SignService signService

/*
    def index() {
        redirect(action: "me")
    }

*/
    def me() {
        [:]
    }

    def checkAndSend(String email) {
        User user = signService.register(requestIp, email)
        if (user) {
            flash.url = user?signService.createNewPasswordUrl(user):""
            redirect action: "sent"
        } else {
            flash.error = "Email incorrecto."
            redirect(action: "me", params: [email: email])
        }
    }

    def sent() {
        [:]
    }

}