/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin.auth

import mamespin.ControllerTools
import mamespin.SignService
import mamespin.User

class RecoverController implements ControllerTools {

    static namespace = "notSigned"

    SignService signService

    static defaultAction = "email"

    def email() {
        [:]
    }

    def checkAndSend(String n) {
        User user = signService.recoverPassword(requestIp, n)
        flash.url = user?signService.createNewPasswordUrl(user):""
        redirect action: "sent"
    }

    def sent() {
        [:]
    }


}