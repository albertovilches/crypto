package mamespin.command

import crypto.Trade
import grails.databinding.BindingFormat

/*
 * @author Alberto Vilches
 * @date 07/11/2017
 */

class TradeCommand {
    BigDecimal outAmount
    BigDecimal inAmount
    String inCurrency
    String outCurrency
    @BindingFormat('yyyy-MM-dd HH:mm')
    Date tradeDate
    Trade.Type type

}