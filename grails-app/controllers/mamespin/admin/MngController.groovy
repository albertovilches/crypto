package mamespin.admin

import mamespin.*
import org.grails.buffer.StreamCharBuffer
import org.springframework.cache.Cache
import tools.StringTools

class MngController implements ControllerTools {

    static namespace = "admin"

    CacheService cacheService

    def index() {
        long cacheSize = nodeSize(cacheService.blockCache.values())?:0
        [nav:true, cacheSize: StringTools.humanReadableString(cacheSize), cacheCount: cacheService.blockCache.size(), cacheKeys: cacheService.blockCache.keySet().sort()]
    }

    def cacheKey(String key) {
        def val = cacheService.blockCache.get(key)
        [nav:true, cacheKey: key, cacheValue: val, cacheSize: nodeSize(val)]
    }


    def clearKey(String key) {
        if (cacheService.blockCache.containsKey(key)) {
            cacheService.blockCache.remove(key)
            flash.message = "Key ${key} removed"
        } else {
            flash.message = "Key ${key} not found"
        }
        redirect action: "index"
    }

    def clearCache() {
        cacheService.clear()
        flash.message = "Cache block clear"
        redirect action: "index"
    }

    private Long nodeSize(a) {
        if (a == null) {
            return 4
        } else if (a instanceof StreamCharBuffer) {
            return a.size()
        } else if (a instanceof String) {
            return a.size()
        } else if (a instanceof Number) {
            return 4
        } else if (a instanceof Map) {
            return nodeSize(a.values())
        } else if (a instanceof Iterable) {
            return ((Iterable)a).collect { nodeSize(it) }.sum()
        }
        throw new RuntimeException("${a} with class ${a?.class} size not computable")

    }

}
