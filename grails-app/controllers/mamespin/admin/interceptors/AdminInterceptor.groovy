/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin.admin.interceptors

import grails.core.GrailsControllerClass
import grails.util.Holders
import mamespin.ControllerTools
import mamespin.SignService
import mamespin.User

class AdminInterceptor implements ControllerTools {

    SignService signService
    int order = 5

    AdminInterceptor() {
        Holders.findApplication().getArtefacts("Controller").each { GrailsControllerClass controllerClass ->
            if (controllerClass.namespace == "admin") {
                match(controller: controllerClass.name.with { it[0].toLowerCase()+it[1..-1]})
            }
        }
    }

    boolean before() {
        if (!currentUser || !session.admin || currentUser.state != User.State.ok) {
            render view: "/notFound"
            return false
        }
        return true
    }

    @Override
    boolean after() {
        if (model != null) {
            if (model.nav == null) model.nav = false
        }
        return true
    }

}