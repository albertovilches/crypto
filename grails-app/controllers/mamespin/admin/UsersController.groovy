package mamespin.admin

import mamespin.ControllerTools
import mamespin.User
import mamespin.UserAction

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

class UsersController implements ControllerTools {

    static namespace = "admin"


    @Transactional(readOnly = true)
    def index(Integer max, String email) {
        params.max = Math.min(max ?: 50, 100)
        def list = User.createCriteria().list(params) {
            or {
                if (email) {
                    like("email", "%${email.toLowerCase()}%")
                }
                if (email) {
                    like("usernameLC", "%${email.toLowerCase()}%")
                }
            }
        }
        [userList: list, userCount: list.totalCount]
    }

    @Transactional(readOnly = true)
    def show(User user) {
        if (user == null) {
            notFound()
            return
        }
        [theUser: user]
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    def save(User user) {
        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond user.errors, view: 'create'
            return
        }

        user.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    def edit(User user) {
        [theUser: user]
    }

    def buyCredit(Long id, Integer gb, String type, String currency, int amount) {
        User user = User.get(id)
        if (user) {
            resourceService.buyMBCredit(user, gb * 1000, Payment.Type."${type}", Payment.Currency."${currency}", amount)
        }
        flash.message = "Añadido ${gb} GB al usuario :)"
        redirect action: "show", params: [id: id]
    }

    def sendCredit(Long id, Integer mb) {
        User user = User.get(id)
        if (user) {
            resourceService.sendMbCredit(user, mb, currentUser)

        }
        flash.message = "Enviado ${mb} MB al usuario :)"
        redirect action: "show", params: [id: id]
    }

    def impersonate(Long id) {
        User user = User.get(id)
        if (user) {
            sessionService.signedIn(user, false)
            session.admin = true
        }
        redirect controller: "list"
    }

    @Transactional
    def update(Long id, String email, String username, String state, Integer level, Integer slots) {
        User theUser = User.get(id)
        if (theUser == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (email != theUser.email) {
            theUser.email = email
        }
        if (state != theUser.state) {
            theUser.state = state
        }
        if (username != theUser.username) {
            theUser.username = username
        }
        theUser.level = level
        theUser.slots = slots

        if (!theUser.validate()) {
            transactionStatus.setRollbackOnly()
            respond theUser.errors, view: 'edit'
            return
        }

        theUser.save flush: true

        flash.message = "Usuario ${theUser.email} (${theUser.id}) modificado"
        redirect action: "index"
    }

    @Transactional
    def delete(User user) {

        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        UserAction.findAllByUser(user)*.delete(flush: true)
        user.delete flush:true

        flash.message = "Usuario ${user.email} (${user.id}) eliminado"
        redirect action:"index"
    }

    protected void notFound() {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
        redirect action:"index"
    }
}
