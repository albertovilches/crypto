package mamespin.admin

import mamespin.Tag
import mamespin.User

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TagController {

    static namespace = "admin"

    static allowedMethods = [save: "POST", update: "POST"]

    def index(Integer max, String search) {
        params.max = Math.min(max ?: 50, 100)
        def list =  Tag.createCriteria().list(params) {
            or {
                if (search) {
                    // like("", "%${search.toLowerCase()}%")
                }
            }
        }
        [tagList: list, tag: list.totalCount, search: search]
    }

    def show(Tag tag) {
        if (tag == null) {
            notFound()
            return
        }
        [tag:tag]
    }

    def create() {
        [tag:new Tag(params)]
    }

    @Transactional
    def save(Tag tag) {
        if (tag == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (tag.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond tag.errors, view:'create'
            return
        }

        tag.save flush:true

//        flash.message = message(code: 'default.created.message', args: [message(code: 'tag.label', default: 'Tag'), tag.id])
        redirect action: "show", params: [id:tag.id]
    }

    def edit(Tag tag) {
        [tag:tag]
    }

    @Transactional
    def update(Tag tag) {
        if (tag == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (tag.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond tag.errors, view:'edit'
            return
        }

        tag.save flush:true

//        flash.message = message(code: 'default.updated.message', args: [message(code: 'tag.label', default: 'Tag'), tag.id])
        redirect action: "show", params: [id:tag.id]
    }

    @Transactional
    def delete(Tag tag) {

        if (tag == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        tag.delete flush:true

//        flash.message = message(code: 'default.deleted.message', args: [message(code: 'tag.label', default: 'Tag'), tag.id])
        redirect action: "index"
    }

    protected void notFound() {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'tag.label', default: 'Tag'), params.id])
        redirect action: "index"
    }
}
