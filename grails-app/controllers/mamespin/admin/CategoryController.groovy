package mamespin.admin

import mamespin.Category
import mamespin.User

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CategoryController {

    static namespace = "admin"

    static allowedMethods = [save: "POST", update: "POST"]

    def index(Integer max, String search) {
        params.max = Math.min(max ?: 50, 100)
        def list =  Category.createCriteria().list(params) {
            or {
                if (search) {
                    // like("", "%${search.toLowerCase()}%")
                }
            }
        }
        [categoryList: list, category: list.totalCount, search: search]
    }

    def show(Category category) {
        if (category == null) {
            notFound()
            return
        }
        [category:category]
    }

    def create() {
        [category:new Category(params)]
    }

    @Transactional
    def save(Category category) {
        if (category == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (category.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond category.errors, view:'create'
            return
        }

        category.save flush:true

//        flash.message = message(code: 'default.created.message', args: [message(code: 'category.label', default: 'Category'), category.id])
        redirect action: "show", params: [id:category.id]
    }

    def edit(Category category) {
        [category:category]
    }

    @Transactional
    def update(Category category) {
        if (category == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (category.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond category.errors, view:'edit'
            return
        }

        category.save flush:true

//        flash.message = message(code: 'default.updated.message', args: [message(code: 'category.label', default: 'Category'), category.id])
        redirect action: "show", params: [id:category.id]
    }

    @Transactional
    def delete(Category category) {

        if (category == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        category.delete flush:true

//        flash.message = message(code: 'default.deleted.message', args: [message(code: 'category.label', default: 'Category'), category.id])
        redirect action: "index"
    }

    protected void notFound() {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'category.label', default: 'Category'), params.id])
        redirect action: "index"
    }
}
