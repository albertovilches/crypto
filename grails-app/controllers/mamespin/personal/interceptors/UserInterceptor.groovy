/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin.personal.interceptors

import crypto.HoldingService
import grails.core.GrailsControllerClass
import grails.util.Holders
import mamespin.ControllerTools
import mamespin.SignService
import mamespin.User

class UserInterceptor implements ControllerTools {

    SignService signService
    HoldingService holdingService

    UserInterceptor() {
        Holders.findApplication().getArtefacts("Controller").each { GrailsControllerClass controllerClass ->
            if (controllerClass.namespace == "signed" || controllerClass.namespace == "admin") {
                match(controller: controllerClass.name.with { it[0].toLowerCase()+it[1..-1]})
            }
        }
    }

    boolean before() {
        if (!currentUser) {
            signService.tryCookieLogin(requestIp)
        }
        if (!currentUser) {
            String nextUrl = request.requestURI + (request.queryString?("?"+request.queryString):"")
            redirect(controller: "sign", action: "in", params: [nextUrl: nextUrl])
            return false
        } else if (currentUser.state != User.State.ok) {
            render(view: "/disabled")
            return false
        }
        return true
    }

    @Override
    boolean after() {
        if (model != null) {
            model.currentUser = currentUser
            model.isAdmin = session.admin

            model.menuHoldings = holdingService.listMenuHoldings(currentUser, 10)
            model.typeAheadCoinsJson = currencyService.typeAheadCoinsJsonG()
        }
        return true
    }
}