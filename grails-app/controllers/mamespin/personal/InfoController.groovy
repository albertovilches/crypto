package mamespin.personal

import mamespin.*

class InfoController implements ControllerTools {

    CacheService cacheService

    static namespace = "signed"

    def download(boolean refresh) {
        render view: "/html", model: [activeCat: "info", html: cacheService.read("download.html", refresh)]
    }

    def slots(boolean refresh) {
        render view: "/html", model: [activeCat: "slots", html: cacheService.read("slots.html", refresh)]
    }

    def credits(boolean refresh) {
        render view: "/html", model: [activeCat: "credits", html: cacheService.read("credits.html", refresh)]
    }

}
