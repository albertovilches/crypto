package mamespin.personal

import crypto.*
import grails.converters.JSON
import grails.core.GrailsApplication
import grails.databinding.BindingFormat
import mamespin.ControllerTools
import mamespin.command.TradeCommand

class TradeController implements ControllerTools {


    static namespace = "signed"

    static defaultAction = "dashboard"

    def json(Integer offset, Integer limit, String order, String sort, String search) {

//        TimeZone tz = TimeZone.getTimeZone("UTC");
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
//        df.setTimeZone(tz);

        List<Trade> trades = tradeService.findUserTrades(currentUser, offset, limit, sort, order, search)
        List<Map> jsonTrades = trades.collect { Trade trade ->
            [id         : trade.id,
             type       : trade.type?.name(),
             inAmount   : trade.inAmount != null ? "${currencyService.formatPrice(trade.inAmount, trade.inCurrency)}|${trade.inCurrency}" : "",
             inValueFiat: trade.inValueFiat,
             out        : trade.type.out,
             inc        : trade.type.inc,
             outAmount  : trade.outAmount ? "${currencyService.formatPrice(trade.outAmount, trade.outCurrency)}|${trade.outCurrency}" : "",
             exchange   : trade.exchange,
             comment    : trade.comment,
             tradeDate  : trade.tradeDate?.time
            ]
        }
        int count = trades.getTotalCount()
        def result = [total: count, rows: jsonTrades]
        render result as JSON
    }

    def refresh() {
        tradeService.refresh(currentUser, params.getInt('max') ?: 9999999)
        redirect action: "list"
    }

    def bulk(String button, String ids) {
        if (button == "remove") {
            Map result = [deleted: tradeService.deleteTrades(currentUser, ids?.split(",") as List)]
            tradeService.refresh(currentUser)
            render result as JSON
        }
/*

        Trade.withNewTransaction {
            ids.split(",").each {
                Trade trade = Trade.get(it)
//                if (button == "enable") {
//                    trade.enabled = true
//                } else if (button == "disable") {
//                    trade.enabled = false
//                }
                if (trade.dirty) trade.save(flush: true)
                render "ok"
            }
        }
        render "ok"
*/
    }

    def transaction(TradeCommand tradeCmd) {
        Trade trade = tradeService.addNewTransaction(currentUser, tradeCmd)
        tradeService.refresh(currentUser)
        Map result = [ok: trade != null]
        render result as JSON
    }

    def list() {
        [sortName: "tradeDate", sortOrder: "desc"]
    }


    def csv() {
        tradeService.importCoinTrackingCsv(true, currentUser, new FileReader(new File("/Users/avilches/Downloads/CoinTracking · Trade Table.csv")))
        render "ok"
    }

}
