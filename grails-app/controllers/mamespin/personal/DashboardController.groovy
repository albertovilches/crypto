package mamespin.personal

import crypto.Currency
import crypto.Holding
import mamespin.ControllerTools

class DashboardController implements ControllerTools {

    static namespace = "signed"

    static defaultAction = "index"

    def index() {

        Currency fiat = currentUser.defaultCurrency

        List<Holding> holdings = holdingService.listAllUserHoldings(currentUser)
        List<Map> chartList = holdingService.aggregateHoldingListToChartList(holdings, fiat)
        List lastUserHoldings = holdings.findAll { it.last }
        Map totals = createTotalsModelFromHoldings(lastUserHoldings)

        def var24h = 12.5
        // TODO: revisar estos valores a ver que es util. sobre todo al vender bitcoin por divisas, el profit que sale
/*
        List widgets = [
                createWidget("Portfolio value", totals.all, "unrealizedProfit"),
                [title  : "24h Change",
                 big    : "",
                 small  : "",
                 style  : var24h >= 0 ? "color-blue" : "color-red",
                 percent: var24h],
                createWidget("Crypto value", totals.crypto, "unrealizedProfit"),
                createWidget("Fiat value", totals.fiat, "profit", fiat)
        ]
*/
        // TODO: ordenar por defecto por valor (primero las divisas que mas tenemos)
        Map chart = createModelForHoldingSerialChart(chartList)
        Map holdingModel = createHoldingsModel(lastUserHoldings, fiat, false)
        Map pieModel = createPortfolioValuePieModel(lastUserHoldings)

        int tradeCount = tradeService.countUserTrades(currentUser)

        [holdingSerialChart: chart,
         totals            : totals,
         tradeCount        : tradeCount,
         fiat              : fiat,
         portfolioValuePie : pieModel

//         widgets: widgets,
        ] + holdingModel
    }

    private Map createPortfolioValuePieModel(List<Holding> lastUserHoldings) {
        List data = lastUserHoldings.collect { Holding holding ->
            [currency: holding.currencyName,
             value   : holding.valueFiat.setScale(2, java.math.BigDecimal.ROUND_HALF_UP)]
        }
        [data: data]
    }

    def holdings() {
        // TODO La ordenacion de tablas no funciona en los datos live
        Currency fiat = currentUser.defaultCurrency
        List lastUserHoldings = holdingService.listLastUserHoldings(currentUser)
        Map holdingModel = createHoldingsModel(lastUserHoldings, fiat, true)
        [fiat: fiat] + holdingModel
    }

    def profit() {
        // TODO La ordenacion de tablas no funciona en los datos live
        Currency fiat = currentUser.defaultCurrency
        List lastUserHoldings = holdingService.listLastUserHoldings(currentUser)
        Map holdingModel = createHoldingsModel(lastUserHoldings, fiat, true)
        [fiat: fiat] + holdingModel
    }

    def holding(String id) {
        Currency currency = currencyService.getCurrency(id)
        if (!currency) {
            redirect action: "dashboard"
        }
        Currency fiat = currentUser.defaultCurrency

        List<Holding> holdings = holdingService.listAllUserHoldings(currentUser, currency)
        List<Map> chartList = holdingService.holdingListToChartList(holdings, currency, fiat)
        Map holdingSerialChart = createModelForHoldingSerialChart(chartList)

        int tradeCount = tradeService.countUserTrades(currentUser, currency)

        Holding holding = holdings.find { it.last }

        Map holdingModel = createHoldingsModel([holding], fiat, true)

        [holdingSerialChart: holdingSerialChart,
         holding: holding,
         fiat: fiat,
         tradeCount: tradeCount
        ] + holdingModel
    }


    private createWidget(String title, Holding holding, String profitField, Currency fiat) {
        [title   : title,
         big     : "${currencyService.formatPrice(holding.valueFiat, fiat)} ${fiat}",
         small   : "${currencyService.formatPrice(holding[profitField], fiat)} ${fiat}",
         style   : holding[profitField] >= 0 ? "color-blue" : "color-red",
         percent : Math.abs(holding[profitField] / holding.costFiat) * 100,
         barColor: holding[profitField] >= 0 ? "#00a8ff" : "#fa424a"]
    }

    private Map createModelForHoldingSerialChart(List<Map> chartList, int maxZoomDays = 120) {
        Date other = chartList[0].date
        Date zoomFromTime = new Date().clearTime() - maxZoomDays
        zoomFromTime = zoomFromTime.time >= other.time ? zoomFromTime : other

        Map history = [data    : chartList,
                       zoomFrom: zoomFromTime.format("yyyy-MM-dd")]
        return history
    }

    private Map createTotalsModelFromHoldings(Collection<Holding> lastUserHoldings) {
        Map holdingsByFiat = lastUserHoldings.groupBy { it.currencyFiat }
        Collection<Holding> cryptoHoldings = holdingsByFiat[false]
        Collection<Holding> fiatHoldings = holdingsByFiat[true]

        def createTotal = { Collection<Holding> from ->
            return new Holding(
                    valueFiat: from.sum { it.valueFiat },
                    costFiat: from.sum { it.costFiat },
                    profit: from.sum { it.profit }
            )
        }
        return [fiat  : createTotal(fiatHoldings),
                crypto: createTotal(cryptoHoldings),
                all   : createTotal(lastUserHoldings)]
    }

    private Map createHoldingsModel(List<Holding> holdings, Currency fiat, boolean last30d) {
        Map holdingsByFiat = holdings.groupBy { it.currencyFiat }

        def json = holdings.collect { Holding holding ->
            def prices = last30d ? priceService.listLastPricesWithConversion(holding.fastCurrency, fiat, 30) : null
            [name    : holding.currencyName,
             amount  : holding.amount,
             unitCost: holding.unitPriceFiat,
             last30d : prices]
        }.inject([:]) { map, entry ->
            map[entry.name] = entry
            return map
        }
/*
        // TODO: hacer en sql
        List<Holding> holdingState = holdingsByFiat[false].groupBy { it.currency }.inject((List<Holding>)[]) { List result, Currency key, List<Holding> all ->
            result << all.sort {it.holdDate}.last()
            return result
        }
*/
        [allHoldings   : holdings?.sort { -it.valueFiat },
         jsonHolding   : json,
         fiatHoldings  : holdingsByFiat[true]?.sort { it.fastCurrency.name }?:[:],
         cryptoHoldings: holdingsByFiat[false]?.sort { it.fastCurrency.rank }?:[:]]
    }


}
