package mamespin.personal

import mamespin.ControllerTools
import mamespin.SignService

class UserController implements ControllerTools {

    static namespace = "signed"

    SignService signService

    def password() {
        [:]
    }

    def username() {
        [:]
    }

    def recover() {
        signService.createTokenAndSendEmailForRecoverPwd(requestIp, currentUser)
        flash.message = "Revisa tu buzón de correo, en unos momentos te llegará un correo electrónico con\n" +
                "un enlace con el que podrás crear una nueva contraseña."
        redirectToHome()
    }

    def changeUsername(String username) {
        username = username?.replaceAll("\\#", "")?.trim()
        if (!(username?.trim())) {
            addError("Debe especificar un nombre de usuario.")
            redirect action: "username"
        } else if (!signService.isUsernameFree(currentUser, username)) {
            flash.username = username
            addError("Ese nombre de usuario ya está siendo usado. Elige otro.")
            redirect action: "username"
        } else {
            currentUser.username = username
            currentUser.save(flush: true)
            flash.message = "Nombre de usuario cambiado correctamente."
            redirectToHome()
        }

    }
    def changePwd(String current, String newPwd, String newPwd2) {
        if (!currentUser.checkPwd(current)) {
            addError("La contraseña actual es incorrecta. Si no te acuerdas, podemos <a href='${g.createLink(action:"recover")}'>enviarte un correo</a> para que crees una nueva.")
            redirect action: "password"
        } else if (newPwd?.size() < 6) {
            addError("La contraseña debe tener 6 caraceteres o más.")
            flash.current = current
            redirect action: "password"
        } else if (newPwd != newPwd2) {
            addError("Las dos contraseñas nuevas no coinciden.")
            flash.current = current
            redirect action: "password"
        } else {
            currentUser.setPassword(newPwd)
            currentUser.save(flush: true)
            flash.message = "Nueva contraseña cambiada correctamente."
            redirectToHome()
        }
    }
}
