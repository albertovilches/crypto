package mamespin

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        name dashboard:"/dashboard"  {controller = "dashboard"; action = "index"}
        name holding:"/holding/$id"{controller = "dashboard"; action = "holding"}
        name holdings:"/holdings"  {controller = "dashboard"; action = "holdings"}
        name profit:"/profit"      {controller = "dashboard"; action = "profit"}
        name trades:"/trades"      {controller = "trade"; action = "list"}

        "/"(controller: "dashboard")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
