package mamespin

class LogoutController implements ControllerTools {

    SignService signService

    def invalidate() {
        signService.logout(requestIp, currentUser, false)
        redirectToHome()
    }

    def index() {
        signService.logout(requestIp, currentUser)
        redirectToHome()
    }
}
