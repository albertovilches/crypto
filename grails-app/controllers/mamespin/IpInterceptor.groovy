/*
 * @author Alberto Vilches
 * @date 16/12/2016
 */
package mamespin;

class IpInterceptor {

    int order = HIGHEST_PRECEDENCE
    Random random = new Random()

    IpInterceptor() {
        matchAll()
    }

    boolean before() {
        String ip = request.getRemoteAddr()
        request.ip = ip
        request.random = random.nextDouble()
        return true
    }


}