package mamespin

class TokenController implements ControllerTools {

    SignService signService

    def newPassword(String uuid, String token) {
        User user = signService.findUserByRecoverPwdToken(requestIp, uuid, token)
        if (user) {
            [token:token, user: user, uuid: uuid]
        } else {
            render view: "wrongToken"
        }
    }

    def change(String uuid, String token, String newPwd, String newPwd2) {
        User user = signService.findUserByRecoverPwdToken(requestIp, uuid, token)
        if (user) {
            if (newPwd?.size() < 6) {
                flash.error = "Please, type at least 6 characters"
                redirect action: "newPassword", params: [uuid: uuid, token: token]
            } else if (newPwd != newPwd2) {
                flash.error = "Two passwords doesn't match"
                redirect action: "newPassword", params: [uuid: uuid, token: token]
            } else {
                signService.changePasswordFromToken(requestIp, user, newPwd)
                sessionService.signedIn(user)
                redirectToHome()
            }
        } else {
            render view: "wrongToken"
        }
    }
}
