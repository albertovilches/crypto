package crypto

import grails.converters.JSON
import grails.transaction.Transactional

class CurrencyController {

    static namespace = "admin"

    static allowedMethods = [save: "POST", update: "POST"]
    CurrencyService currencyService
    ImportCurrenciesService importCurrenciesService
    ImportPricesService importPricesService
    PriceService priceService

    def pairs() {
        render view: "show", model: [sortName: "sfrom", sortOrder: "asc", limit: 50]
    }

    def index() {
        // Todo: remember values?
        [sortName: "rank", sortOrder: "asc", limit: 50]
    }

    def checkConversion() {
        Set oh = []
//        Set only = "BET, IFC, LYC, ZCC, TMC, POLY, KRC, CIR, AMIS, AVT, DENT, BQX, TIX, 8BT, CDT, SUR, PQT, PPT, FYN, IML, PRO, COSS, UET, SPN, ROOTS, ICE, MANA, XAS, MRV, MET, WNET, FUCK, IGNIS, EVX, LNK, PBKX, HBT, GOOD, SCL, CCT, CAT".split(", ").collect { Currency.findByName(it)}
//        Set only = "PQT".split(", ").collect { Currency.findByName(it)}
        Set only = currencyService.findAllEnabledCryptoCurrencies()
//        Set only = [Currency.findByName("BET")]
        only.each { Currency currency ->
            def val = priceService.convertToHistoricPrice(1, currency, Currency.findByName("EUR"), new Date()-2)
            def val2 = priceService.convertToHistoricPrice(1, currency, Currency.findByName("USD"), new Date()-2)
            println val
            if (val < 0 && val2 < 0) {
                oh << currency
            }
        }
        oh.each { Currency currency ->
            println "${PairPrice.findAllByFrom(currency)*.sto}\n${currency.name}\n\n"
        }

    }

    def json(Integer offset, Integer limit, String order, String sort, String search) {

        List parts = (search ?: "").split(" ").findAll()*.toLowerCase() // remove empty estring

        boolean updatedToday = false
        Boolean withError = null
        Boolean enabled = null
        Boolean visible = null
        Boolean fiat = null
        Boolean withMedia

        if ("is:media" in parts) {
            withMedia = true
        } else if ("!is:media" in parts) {
            withMedia = false
        }
        parts.removeAll(["is:media", "!is:media"])

        if (":today" in parts) {
            parts.remove(":today")
            updatedToday = true
        }

        if ("is:error" in parts || "!is:ok" in parts) {
            withError = true
        } else if ("is:ok" in parts || "!is:error" in parts) {
            withError = false
        }
        parts.removeAll(["is:error", "!is:error", "is:ok", "!is:ok"])

        if ("is:enabled" in parts || "!is:disabled" in parts) {
            enabled = true
        } else if ("is:disabled" in parts || "!is:enabled" in parts) {
            enabled = false
        }
        parts.removeAll(["is:enabled", "!is:enabled", "is:disabled", "!is:disabled"])

        if ("is:visible" in parts) {
            visible = true
        } else if ("!is:visible" in parts) {
            visible = false
        }
        parts.removeAll(["is:visible", "!is:visible"])

        if ("is:fiat" in parts || "!is:crypto" in parts) {
            fiat = true
        } else if ("is:crypto" in parts || "!is:crypto" in parts) {
            fiat = false
        }
        parts.removeAll(["is:fiat", "!is:fiat", "is:crypto", "!is:crypto"])

        parts.removeAll { it.contains(":") }

        limit = Math.min(limit ?: 50, 200)
        List list = Currency.createCriteria().list(["offset": offset, "max": limit]) {
            delegate.order(sort, order)
            if (updatedToday) {
                ge("lastUpdated", new Date().clearTime())
            }
            if (fiat != null) {
                eq("fiat", fiat)
            }
            if (enabled != null) {
                eq("enabled", enabled)
            }
            if (visible == true) {
                eq("enabled", true)
                or {
                    gt("pairsFrom", 0)
                    gt("pairsTo", 0)
                }
            } else if (visible == false) {
                or {
                    eq("enabled", false)
                    and {
                        eq("pairsFrom", 0)
                        eq("pairsTo", 0)
                    }
                }
            }

            if (withMedia != null) {
                eq("mediaOk", withMedia)
            }

            if (withError == true) {
                or {
                    ne("status", Currency.Status.ok)
                    ne("pairsStatus", Currency.Status.ok)
                    isNotNull("message")
                }
            } else if (withError == false) {
                and {
                    eq("status", Currency.Status.ok)
                    eq("pairsStatus", Currency.Status.ok)
                    isNull("message")
                }
            }
            if (parts) {
                or {
                    parts.each { String part ->
                        if (part.startsWith("!")) {
                            not {
                                ilike("name", part.substring(1))
                            }
                        } else {
                            ilike("name", part)
                        }
                    }
                }
            }
        }
        List<Map> rows = list.collect { Currency currency ->
            [id          : currency.id,
             name        : currency.name,
             displayName : currency.displayName,
             fullImageUrl: currency.ccFullImageUrl(),
             rank        : currency.rank,
             status      : currency.status?.name(),
             pairsStatus : currency.pairsStatus?.name(),
             message     : currency.message,
             mediaOk     : currency.mediaOk,
             dateCreated : currency.dateCreated,
             lastUpdated : currency.lastUpdated,
             enabled     : currency.enabled,
             pairsFrom   : currency.pairsFrom,
             pairsTo     : currency.pairsTo,
             pairsWrong     : currency.pairsWrong
            ]
        }
        int count = list.getTotalCount()
        def result = [total: count, rows: rows]
        render result as JSON
    }

    def showPair(String id) {
        goShowPair(id)
    }
    private goShowPair(String id) {
        if (!id || !id.contains("-")) {
            return redirect(action: "index")
        }
        String[] parts = id.split("-")
        String from = parts[0].trim()
        String to = parts[1].trim()
        Currency currencyFrom = Currency.findByName(from)
        Currency currencyTo = Currency.findByName(to)
        if (!currencyFrom || !currencyTo) return redirect(action: "index")
        PairPrice pairPrice = PairPrice.findWhere(from: currencyFrom, to: currencyTo)
        if (!pairPrice) {
            return redirect(action: "show", id: "${to}-${from}")
        }
        render view:"showPair", model: [sortName: "date", sortOrder: "desc", pairPrice: pairPrice, limit: 200]
    }

    def show(String id) {
        if (id && id.contains("-")) {
            goShowPair(id)
            return
        }
        Currency currency = Currency.findByName(id)
        if (!currency) return redirect(action: "index")
        [sortName: "sfrom", sortOrder: "asc", currency: currency, limit: 50]
    }

    def jsonPrice(Long fromId, Long toId, Integer offset, Integer limit, String order, String sort, String search) {
        limit = Math.min(limit ?: 20, 2000)

        List list = Price.createCriteria().list(["offset": offset, "max": limit]) {
            delegate.order(sort, order)
            eq("from", Currency.get(fromId))
            eq("to", Currency.get(toId))

        }

        def rows = list.collect { Price price ->
            [id            : price.id,
             date          : price.date,
             price         : price.price,
            ]
        }
        int count = list.getTotalCount()
        def result = [total: count, rows: rows]
        render result as JSON

    }
    def jsonPairs(Long id, Integer offset, Integer limit, String order, String sort, String search) {
        Currency currency = Currency.get(id)

        List parts = (search ?: "").split(" ").findAll()*.toLowerCase() // remove empty estring
        String to = parts?.find { String part -> part.startsWith("to:") && part.size() > 3}?.substring(3)
        String notTo = parts?.find { String part -> part.startsWith("!to:") && part.size() > 3}?.substring(4)
        String from = parts?.find { String part -> part.startsWith("from:") && part.size() > 5}?.substring(5)
        String notFrom = parts?.find { String part -> part.startsWith("!from:") && part.size() > 6}?.substring(6)

        Boolean withError
        Boolean outdated
        Boolean visible

        if ("is:error" in parts || "!is:ok" in parts) {
            withError = true
        } else if ("is:ok" in parts || "!is:error" in parts) {
            withError = false
        }
        parts.removeAll(["is:error", "!is:error", "is:ok", "!is:ok"])

        if ("is:outdated" in parts || "!is:updated" in parts) {
            outdated = true
        } else if ("!is:outdated" in parts || "is:updated" in parts) {
            outdated = false
        }
        parts.removeAll(["is:outdated", "!is:outdated", "is:updated", "!is:updated"])

        if ("is:visible" in parts) {
            visible = true
        } else if ("!is:visible" in parts) {
            visible = false
        }
        parts.removeAll(["is:visible", "!is:visible"])


        parts.removeAll { it.contains(":") }

        limit = Math.min(limit ?: 50, 200)
        List list = PairPrice.createCriteria().list(["offset": offset, "max": limit]) {
            delegate.order(sort, order)
            if (currency) {
                or {
                    eq("from", currency)
                    eq("to", currency)
                }
            }

            if (visible == true) {
                and {
                    delegate.from {
                        eq("enabled", true)
                    }
                    delegate.to {
                        eq("enabled", true)
                    }
                }
            } else if (visible == false) {
                or {
                    delegate.from {
                        eq("enabled", false)
                    }
                    delegate.to {
                        eq("enabled", false)
                    }
                }
            }

            if (withError == true) {
                or {
                    ne("status", Currency.Status.ok)
                    isNotNull("message")
                }
            } else if (withError == false) {
                and {
                    eq("status", Currency.Status.ok)
                    isNull("message")
                }
            }

            if (outdated == false) {
                eq("lastPriceDate", new Date().clearTime())
            } else if (outdated == true) {
                or {
                    isNull("lastPriceDate")
                    lt("lastPriceDate", new Date().clearTime())
                }
            }

            if (to) {
                if (to == "fiat") {
                    delegate.to {
                        eq("fiat", true)
                    }
                } else {
                    ilike("sto", to)
                }
            }
            if (notTo) {
                if (notTo == "fiat") {
                    delegate.to {
                        eq("fiat", false)
                    }
                } else {
                    not {
                        ilike("sto", notTo)
                    }
                }
            }

            if (from) {
                if (from == "fiat") {
                    delegate.from {
                        eq("fiat", true)
                    }
                } else {
                    ilike("sfrom", from)
                }
            }

            if (notFrom) {
                if (notFrom == "fiat") {
                    delegate.from {
                        eq("fiat", false)
                    }
                } else {
                    not {
                        ilike("sfrom", notFrom)
                    }
                }
            }

            if (parts) {
                or {
                    parts.each { String part ->
                        if (part.startsWith("!")) {
                            not {
                                and {
                                    ilike("sfrom", part.substring(1))
                                    ilike("sto", part.substring(1))
                                }
                            }
                        } else {
                            or {
                                ilike("sfrom", part)
                                ilike("sto", part)
                            }
                        }
                    }
                }
            }

        }
        def rows = list.collect { PairPrice pairPrice ->
            [id            : pairPrice.id,
             sfrom         : pairPrice.sfrom,
             sto           : pairPrice.sto,
             status        : pairPrice.status?.name(),
             message       : pairPrice.message,
             lastImport    : pairPrice.lastImport,
             firstPriceDate: pairPrice.firstPriceDate,
             lastPriceDate : pairPrice.lastPriceDate,
             dateCreated   : pairPrice.dateCreated,
             lastUpdated   : pairPrice.lastUpdated,
            ]
        }
        int count = list.getTotalCount()
        def result = [total: count, rows: rows]
        render result as JSON
    }

    def bulk(String button, String ids) {
        Collection<Currency> onlyThese = ids.split(",").collect { Currency.get(it) }.findAll()

//        redirect action: "show", params: [id:onlyThese[0].name]
        if (button == "update") {
            importCurrenciesService.businessUpdateCrypto(true, onlyThese)
        } else if (button == "updateOutdatedPrices") {
            importPricesService.checkPricesInCurrencies(0, onlyThese)

        } else if (button == "updatePrices") {
            importPricesService.checkPricesInCurrencies(1, onlyThese)

        } else {
            Currency.withNewTransaction {
                onlyThese.each { Currency currency ->
                    if (button == "enable") {
                        currency.enabled = true
                    } else if (button == "disable") {
                        currency.enabled = false
                    }
                    if (currency.dirty) currency.save(flush: true)
                }
            }
            currencyService.refreshCacheFromDb()
        }
        if (params.from == "show") {
            redirect(action: "show", params: [id:onlyThese[0].name])
        } else {
            render("ok")
        }
    }

    def updateFiat() {
        long start = System.currentTimeMillis()
        importCurrenciesService.updateFiat()
        currencyService.refreshCacheFromDb()
        flash.infoMessage = "Update fiat finished in ${(System.currentTimeMillis() - start) / 1000}s"
        redirect action: "index"
    }

    def updatePairsOutdated() {
        long start = System.currentTimeMillis()
        importPricesService.checkPricesOutdated()
        flash.infoMessage = "Update outdated pairs finished in ${(System.currentTimeMillis() - start) / 1000}s"
        redirect action: "index"
    }

    def updateCrypto(Boolean full) {
        long start = System.currentTimeMillis()
        importCurrenciesService.businessUpdateCrypto(full)
        flash.infoMessage = "update coins finished in ${(System.currentTimeMillis() - start) / 1000}s"
        redirect action: "index"
    }

    def daily() {
        long start = System.currentTimeMillis()
        importCurrenciesService.businessUpdateCrypto(false)
        importPricesService.checkPricesOutdated()
        flash.infoMessage = "update coins finished in ${(System.currentTimeMillis() - start) / 1000}s"
        redirect action: "index"
    }

    def pairBulk(String button, String ids) {
        long start = System.currentTimeMillis()
        Collection<PairPrice> onlyThese = ids.split(",").collect { PairPrice.get(it) }.findAll()
        if (button == "updateOutdatedPrices") {
            onlyThese.each { PairPrice pairPrice ->
                importPricesService.checkPricesInPairPrice(0, pairPrice)
            }
        } else if (button == "updatePrices") {
            onlyThese.each { PairPrice pairPrice ->
                importPricesService.checkPricesInPairPrice(1, pairPrice)
            }
        } else if (button == "resetPrices") {
            onlyThese.each { PairPrice pairPrice ->
                importPricesService.checkPricesInPairPrice(-1, pairPrice)
            }
        }
        flash.infoMessage = "update prices finished in ${(System.currentTimeMillis() - start) / 1000}s"
        redirect action: "index"
    }

    def enabledCurrency(Long id) {
        Currency currency = Currency.get(id)
        currency.enabled = !currency.enabled
        currency.save(flush: true)
        render "ok"
    }

    def create() {
        [currency: new Currency(params)]
    }

    def save(Currency currency) {
        if (currency == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
        if (currency.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render model: [currency: currency], view: 'edit'
            return
        }
        currency.save flush: true

//        flash.message = message(code: 'default.created.message', args: [message(code: 'currency.label', default: 'Currency'), currency.id])
        redirect action: "show", params: [id: currency.id]
    }

    def edit(Currency currency) {
        [currency: currency]
    }

    @Transactional
    def update(Currency currency) {
        if (currency == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (currency.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render model: [currency: currency], view: 'edit'
            return
        }

        currency.save flush: true

//        flash.message = message(code: 'default.updated.message', args: [message(code: 'currency.label', default: 'Currency'), currency.id])
        redirect action: "show", params: [id: currency.id]
    }

    @Transactional
    def delete(Currency currency) {

        if (currency == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        currency.delete flush: true

//        flash.message = message(code: 'default.deleted.message', args: [message(code: 'currency.label', default: 'Currency'), currency.id])
        redirect action: "index"
    }

    protected void notFound() {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'currency.label', default: 'Currency'), params.id])
        redirect action: "index"
    }
}
